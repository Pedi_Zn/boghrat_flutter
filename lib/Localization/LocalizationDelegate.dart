import 'dart:async';

import 'package:boghrat_flutter/Localization/Localization.dart';
import 'package:flutter/material.dart';

class DemoLocalizationsDelegate
    extends LocalizationsDelegate<DemoLocalizations> {
  DemoLocalizationsDelegate();
  @override
  bool isSupported(Locale locale) => ['en', 'fa'].contains(locale.languageCode);

  @override
  Future<DemoLocalizations> load(Locale locale) async {
    DemoLocalizations localizations = new DemoLocalizations(locale);
    await localizations.load();

    print("Load ${locale.languageCode}");
    return localizations;
  }

  @override
  bool shouldReload(DemoLocalizationsDelegate old) => true;
}

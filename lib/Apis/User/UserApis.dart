import 'dart:convert';

import 'package:boghrat_flutter/Areas/User/Models/ForgotPasswordModel.dart';
import 'package:boghrat_flutter/Areas/User/Models/LoginModel.dart';
import 'package:boghrat_flutter/Areas/User/Models/OTPModel.dart';
import 'package:boghrat_flutter/Areas/User/Models/RegisterModel.dart';
import 'package:boghrat_flutter/Areas/User/Models/UserInfoModel.dart';
import 'package:boghrat_flutter/Config/BaseClient.dart';
import 'package:boghrat_flutter/Config/BaseController.dart';
import 'package:http/http.dart' as http;

class UserApis extends GetxController with BaseClient {
  static var client = http.Client();
  static var baseUrl = "https://boghrat.com";
  final BaseClient baseClient = new BaseClient();

  Future<LoginModel> login(body) async {
    dynamic errorRes;
    baseClient.setHeader("Content-Type", "application/x-www-form-urlencoded");
    var response = await baseClient
        .post(baseUrl + "/api/oauth/token", body)
        .catchError((error) {
      handleError(error);
      errorRes = error.message;
    });
    return loginModelFromJson(errorRes == null ? response : errorRes);
  }

  Future<UserInfoModel> getUserInfo(token) async {
    baseClient.setHeader("Authorization", token);
    var response = await baseClient
        .get(baseUrl + "/api/account/GetUserInfo")
        .catchError(handleError);
    return userInfoModelFromJson(response);
  }

  Future<OTPModel> resendVerificationCode(body) async {
    baseClient.setHeader("Content-Type", "application/json");
    var response = await baseClient
        .post(baseUrl + "/api/account/ResendPhoneVerificationCode",
            jsonEncode(body))
        .catchError(handleError);
    return otpModelFromJson(response);
  }

  Future<OTPModel> phoneVerification(body) async {
    baseClient.setHeader("Content-Type", "application/json");
    var response = await baseClient
        .post(baseUrl + "/api/account/PhoneVerification", jsonEncode(body))
        .catchError(handleError);
    return otpModelFromJson(response);
  }

  Future<ForgotPasswordModel> sendVerificationCode(body) async {
    baseClient.setHeader("Content-Type", "application/json");
    var response = await baseClient
        .post(baseUrl + "/api/account/PhoneNumberForgetPassword",
            jsonEncode(body))
        .catchError(handleError);
    return forgotPasswordModelFromJson(response);
  }

  Future<ForgotPasswordModel> forgotPassword(body) async {
    baseClient.setHeader("Content-Type", "application/json");
    var response = await baseClient
        .post(
            baseUrl + "/api/account/PhoneNumberResetPassword", jsonEncode(body))
        .catchError(handleError);
    return forgotPasswordModelFromJson(response);
  }

  Future<RegisterModel> register(body) async {
    baseClient.setHeader("Content-Type", "application/json");
    var response = await baseClient
        .post(baseUrl + "/api/account/RegisterUser", jsonEncode(body))
        .catchError(handleError);
    return registerModelFromJson(response);
  }

  Future<RegisterModel> registerV2(body) async {
    baseClient.setHeader("Content-Type", "application/json");
    var response = await baseClient
        .post(baseUrl + "/api/account/RegisterAsUser", jsonEncode(body))
        .catchError(handleError);
    return registerModelFromJson(response);
  }
}

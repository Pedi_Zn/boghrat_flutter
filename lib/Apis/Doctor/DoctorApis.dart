import 'dart:convert';

import 'package:boghrat_flutter/Areas/Doctors/Models/AllDoctorHasOnlineReceptionOfClinicModel.dart';
import 'package:boghrat_flutter/Areas/Doctors/Models/AllOnlineAppointmentTypeModel.dart';
import 'package:boghrat_flutter/Areas/Doctors/Models/AllSchedulesOfClinicModel.dart';
import 'package:boghrat_flutter/Areas/Doctors/Models/ByPublicDoctorModel.dart';
import 'package:boghrat_flutter/Areas/Doctors/Models/ClinicGalleryModel.dart';
import 'package:boghrat_flutter/Areas/Doctors/Models/DoctorProfileByUserNameModel.dart';
import 'package:boghrat_flutter/Areas/Doctors/Models/DoctorSocialModel.dart';
import 'package:boghrat_flutter/Areas/Doctors/Models/OnlinePaymentPolicyModel.dart';
import 'package:boghrat_flutter/Areas/Doctors/Models/OnlineReceptionFreeTimeModel.dart';
import 'package:boghrat_flutter/Areas/SearchTab/Models/ExpertisesModel.dart';
import 'package:boghrat_flutter/Areas/SearchTab/Models/ProvincesModel.dart';
import 'package:boghrat_flutter/Areas/SearchTab/Models/SearchModel.dart';
import 'package:boghrat_flutter/Config/BaseClient.dart';
import 'package:boghrat_flutter/Config/BaseController.dart';
import 'package:http/http.dart' as http;

class DoctorApis extends GetxController with BaseClient {
  static var client = http.Client();
  static var baseUrl = "https://boghrat.com";
  final BaseClient baseClient = new BaseClient();

  Future<DoctorProfileByUserNameModel> getDoctorProfileByUserName(
      userName) async {
    var response = await baseClient
        .get(baseUrl +
            "/api/userpublicprofile/GetDoctorProfileByUserName?userName=$userName")
        .catchError(handleError);
    return doctorProfileByUserNameModelFromJson(response);
  }

  Future<DoctorSocialModel> getDoctorSocial(doctorId) async {
    var response = await baseClient
        .get(baseUrl + "/api/Doctor/GetDoctorSocial?doctorId=$doctorId")
        .catchError(handleError);
    return doctorSocialModelFromJson(response);
  }

  Future<List<ByPublicDoctorModel>> getByPublicDoctor(doctorId) async {
    var response = await baseClient
        .get(baseUrl + "/api/clinic/GetByPublicDoctor?doctorId=$doctorId")
        .catchError(handleError);
    return byPublicDoctorModelFromJson(response);
  }

  Future<List<AllDoctorHasOnlineReceptionOfClinicModel>>
      getAllDoctorHasOnlineReceptionOfClinic(clinicId) async {
    var response = await baseClient
        .get(baseUrl +
            "/api/DoctorOnlineAppointmentConfig/GetAllDoctorHasOnlineReceptionOfClinic?clinicId$clinicId")
        .catchError(handleError);
    return allDoctorHasOnlineReceptionOfClinicModelFromJson(response);
  }

  Future<List<ClinicGalleryModel>> getClinicGallery(clinicId) async {
    var response = await baseClient
        .get(baseUrl + "/api/ClinicGallery/GetClinicGallery?clinicId=$clinicId")
        .catchError(handleError);
    return clinicGalleryModelFromJson(response);
  }

  Future<List<AllSchedulesOfClinicModel>> getAllSchedulesOfClinic(
      clinicId) async {
    var response = await baseClient
        .get(baseUrl +
            "/api/ClinicSchedule/GetAllSchedulesOfClinic?clinicId=$clinicId")
        .catchError(handleError);
    return allSchedulesOfClinicModelFromJson(response);
  }

  Future<List<AllOnlineAppointmentTypeModel>> getAllOnlineAppointmentType(
      clinicId) async {
    var response = await baseClient
        .get(baseUrl +
            "/api/AppointmentType/GetAllOnlineAppointmentType?clinicId=$clinicId")
        .catchError(handleError);
    return allOnlineAppointmentTypeModelFromJson(response);
  }

  Future<List<OnlineReceptionFreeTimeModel>> getOnlineReceptionFreeTime(
      appointmentTypeId, clinicId, day, doctorId, fromDate) async {
    var response = await baseClient
        .get(baseUrl +
            "/api/DoctorSchedule/GetOnlineReceptionFreeTime?appointmentTypeId=$appointmentTypeId&clinicId=$clinicId&day=$day&doctorId=$doctorId&fromDate=$fromDate")
        .catchError(handleError);
    return onlineReceptionFreeTimeModelFromJson(response);
  }

  Future<OnlinePaymentPolicyModel> getOnlinePaymentPolicy(
      appointmentTypeId) async {
    var response = await baseClient
        .get(baseUrl +
            "/api/onlinePaymentPolicy/Get?appointmentTypeId=$appointmentTypeId")
        .catchError(handleError);
    return onlinePaymentPolicyModelFromJson(response);
  }
}

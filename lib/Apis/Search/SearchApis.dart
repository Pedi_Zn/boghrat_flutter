import 'dart:convert';

import 'package:boghrat_flutter/Areas/SearchTab/Models/ExpertisesModel.dart';
import 'package:boghrat_flutter/Areas/SearchTab/Models/ProvincesModel.dart';
import 'package:boghrat_flutter/Areas/SearchTab/Models/SearchModel.dart';
import 'package:boghrat_flutter/Config/BaseClient.dart';
import 'package:boghrat_flutter/Config/BaseController.dart';
import 'package:http/http.dart' as http;

class SearchApis extends GetxController with BaseClient {
  static var client = http.Client();
  static var baseUrl = "https://boghrat.com";
  final BaseClient baseClient = new BaseClient();

  Future<SearchModel> getDoctors(body) async {
    baseClient.setHeader("Content-Type", "application/json");
    var response = await baseClient
        .post(baseUrl + "/api/SearchDoctor/GetDoctors", jsonEncode(body))
        .catchError(handleError);
    return searchModelFromJson(response);
  }

  Future<List<ProvincesModel>> getProvinces() async {
    var response = await baseClient
        .get(baseUrl + "/api/district/GetProvinces")
        .catchError(handleError);
    return provincesModelFromJson(response);
  }

  Future<List<ExpertisesModel>> getExpertisesWithQuery(query) async {
    var response = await baseClient
        .get(baseUrl +
            "/api/Public/SearchDoctor/GetSearchExpertises?keyword=$query")
        .catchError(handleError);
    return expertisesModelFromJson(response);
  }

  Future<SearchModel> getDoctorsWithQuert(query, body) async {
    var response = await baseClient
        .post(baseUrl + "/api/Public/SearchDoctor/GetDoctors?keyword=$query",
            body)
        .catchError(handleError);
    return searchModelFromJson(response);
  }
}

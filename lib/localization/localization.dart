import 'dart:async';
import 'dart:convert';
import 'package:flash/flash.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:boghrat_flutter/Config/AppConfig.dart' as config;

class DemoLocalizations {
  late Locale locale;
  DemoLocalizations(this.locale);
  static DemoLocalizations? of(BuildContext context) {
    return Localizations.of<DemoLocalizations>(context, DemoLocalizations);
  }

  late Map<String, dynamic> _sentences;

  void showDialog(BuildContext context, String text) {
    showFlash(
      context: context,
      duration: Duration(milliseconds: 2500),
      builder: (context, controller) {
        return Flash(
          controller: controller,
          barrierDismissible: false,
          behavior: FlashBehavior.floating,
          position: FlashPosition.top,
          margin: EdgeInsets.fromLTRB(45.0, 20.0, 45.0, 20.0),
          borderRadius: BorderRadius.circular(10.0),
          boxShadows: kElevationToShadow[4],
          backgroundColor: Theme.of(context).accentColor,
          horizontalDismissDirection: HorizontalDismissDirection.horizontal,
          child: Container(
            height: 45.0,
            child: Center(
              child: Text(
                text,
                textAlign: TextAlign.center,
                style: TextStyle(
                  color: config.Colors().primaryColorLight(1),
                  fontSize: 14.0,
                ),
              ),
            ),
          ),
        );
      },
    );
  }

  Future<bool> load() async {
    String data = await rootBundle
        .loadString('languages/${this.locale.languageCode}.json');
    Map<String, dynamic> _result = json.decode(data);

    this._sentences = new Map();
    _result.forEach((String key, dynamic value) {
      if (!(value is String))
        this._sentences[key] = value;
      else
        this._sentences[key] = value.toString();
    });

    return true;
  }

  String transDate(int d) {
    d = (DateTime.now().millisecondsSinceEpoch -
        d +
        1000); // from now 1 sec before

    double delta = d / 1000;
    String numString = delta.toString().split(".")[0];
    if (delta < (60)) {
      return this.transPriceNumber(numString) +
          " " +
          this._sentences["S"] +
          " " +
          this._sentences["Ago"];
    } else {
      delta = delta / 60;
      numString = delta.toString().split(".")[0];
      if (delta < (60)) {
        return this.transPriceNumber(numString) +
            " " +
            this._sentences["Mi"] +
            " " +
            this._sentences["Ago"];
      } else {
        delta = delta / 60;
        numString = delta.toString().split(".")[0];
        if (delta < (24)) {
          return this.transPriceNumber(numString) +
              " " +
              this._sentences["H"] +
              " " +
              this._sentences["Ago"];
        } else {
          // day
          delta = delta / 24;
          numString = delta.toString().split(".")[0];
          if (delta < 7) {
            numString = delta.toString().split(".")[0];
            return this.transPriceNumber(numString) +
                " " +
                this._sentences["D"] +
                " " +
                this._sentences["Ago"];
          } else {
            delta = delta / 7;
            if (delta < 4.287) {
              numString = delta.toString().split(".")[0];
              return this.transPriceNumber(numString) +
                  " " +
                  this._sentences["W"] +
                  " " +
                  this._sentences["Ago"];
            } else {
              delta = (delta * 7) / 30;
              if (delta < 12) {
                numString = delta.toString().split(".")[0];
                return this.transPriceNumber(numString) +
                    " " +
                    this._sentences["Mo"] +
                    " " +
                    this._sentences["Ago"];
              } else {
                delta = (delta) / 12;
                numString = delta.toString().split(".")[0];
                return this.transPriceNumber(numString) +
                    " " +
                    this._sentences["Y"] +
                    " " +
                    this._sentences["Ago"];
              }
            }
          }
        }
      }
    }
  }

  Color getColorObjectFromHex(hexString) {
    var l = hexString.split("#").join("");
    int colorHex = int.parse(l, radix: 16);
    return (new Color(colorHex));
  }

  dynamic trans(String key) {
    return this._sentences[key];
  }

  String transPriceNumber(String numberString) {
    numberString = numberString.split(".").elementAt(0);
    if (!double.parse(numberString).isNaN) {
      var chars = numberString.split("");
      var numberTranslated = "";
      var z = 0;
      for (var c = chars.length - 1; c > -1; c--) {
        if ((chars[c] == ".") || (chars[c] == ".")) {
          numberTranslated = "." + numberTranslated;
        } else {
          if (this._sentences[chars[c]] != null)
            numberTranslated = this._sentences[chars[c]] + numberTranslated;
        }
        if ((z > 1) && ((z) % 3) == 2) {
          if (c > 0) {
            numberTranslated = this._sentences[","] + numberTranslated;
          }
        }
        z++;
      }
      return numberTranslated;
    } else {
      return "Nan";
    }
  }

  String transPhoneNumber1(String numberString) {
    var s = numberString;

    var chars = s.split("");
    var numberTranslated = "";
    var dashed = "";
    if (chars[0] == "-") {
      dashed = chars[0];
    }
    for (var c = chars.length - 1; c > -1; c--) {
      if ((chars[c] == ".") || (chars[c] == ".")) {
        numberTranslated = "." + numberTranslated;
      } else {
        if (chars[c] != "-") {
          if (this._sentences[chars[c] + "-"] != null)
            numberTranslated =
                this._sentences[chars[c] + "-"] + numberTranslated;
          else
            numberTranslated = numberTranslated + this._sentences[chars[c]];
        }
      }
    }
    return numberTranslated + (dashed == "-" ? dashed : "");
  }

  String transNumber(String numberString) {
    var s = numberString;
    s = numberString.split(".")[0];
    if (numberString.split(".").length > 1) {
      s = s + "." + numberString.split(".")[1].split("")[0];
      if (numberString.split(".")[1].split("").length > 1) {
        s = s + numberString.split(".")[1].split("")[1];
      }
    }
    if (!double.parse(s).isNaN) {
      var chars = s.split("");
      var numberTranslated = "";
      var dashed = "";
      if (chars[0] == "-") {
        dashed = chars[0];
      }
      for (var c = chars.length - 1; c > -1; c--) {
        if ((chars[c] == ".") || (chars[c] == ".")) {
          numberTranslated = "." + numberTranslated;
        } else {
          if (chars[c] != "-")
            numberTranslated = this._sentences[chars[c]] + numberTranslated;
        }
      }
      return numberTranslated + (dashed == "-" ? dashed : "");
    } else {
      return "Nan";
    }
  }

  String transClock(String numberString) {
    String s = this.transNumber(numberString);
    if (s.length == 1) {
      s = this._sentences["0"] + s;
    }
    return s;
  }

  String transPriceNumberToValue(String numberString) {
    // only persian number to int

    var chars = numberString.split("");
    var numberTranslated = "";

    for (var c = chars.length - 1; c > -1; c--) {
      if (this._sentences[chars[c]] != null) {
        //
        try {
          int.parse(chars[c]);
          numberTranslated = chars[c] + numberTranslated;
        } catch (e) {
          numberTranslated = this._sentences[chars[c]] + numberTranslated;
        }
      } else {
        return "NaN";
      }
    }
    return numberTranslated;
  }
}

import 'package:flutter/material.dart';
import 'package:get/get.dart';

class TabBarController extends GetxController
    with SingleGetTickerProviderMixin {
  late TabController tabController;
  var currentIndex = 4.obs;

  @override
  void onInit() {
    tabController = TabController(vsync: this, length: 5, initialIndex: 4);
    super.onInit();
  }
}

import 'package:boghrat_flutter/Areas/SearchTab/Views/SearchScreen.dart';
import 'package:boghrat_flutter/Areas/TabBar/Controllers/TabBarController.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:material_design_icons_flutter/material_design_icons_flutter.dart';
import 'package:boghrat_flutter/Config/AppConfig.dart' as config;

class TabBarScreen extends StatelessWidget {
  final tabBarController = Get.put(TabBarController());

  @override
  Widget build(BuildContext context) {
    return SafeArea(
      child: Scaffold(
        body: TabBarView(
          controller: tabBarController.tabController,
          children: [
            Container(),
            Container(),
            Container(),
            Container(),
            SearchScreen(),
          ],
        ),
        bottomNavigationBar: _buildBottomNavigation(tabBarController),
      ),
    );
  }
}

Widget _buildBottomNavigation(tabBarController) {
  return Container(
    height: 50,
    child: Material(
      elevation: 10,
      color: config.Colors().primaryColorLight(1),
      child: TabBar(
        controller: tabBarController.tabController,
        labelColor: config.Colors().blackColor(1),
        unselectedLabelColor: Color(0xffacacac),
        indicatorColor: Colors.transparent,
        tabs: [
          Tab(icon: Icon(MdiIcons.viewDashboardOutline, size: 30)),
          Tab(icon: Icon(MdiIcons.account, size: 30)),
          Tab(icon: Icon(MdiIcons.calendarToday, size: 30)),
          Tab(icon: Icon(MdiIcons.currencyUsd, size: 30)),
          Tab(icon: Icon(MdiIcons.magnify, size: 30)),
        ],
      ),
    ),
  );
}

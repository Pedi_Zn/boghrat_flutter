import 'dart:convert';

OnlinePaymentPolicyModel onlinePaymentPolicyModelFromJson(String str) =>
    OnlinePaymentPolicyModel.fromJson(json.decode(str));

String onlinePaymentPolicyModelToJson(OnlinePaymentPolicyModel data) =>
    json.encode(data.toJson());

class OnlinePaymentPolicyModel {
  OnlinePaymentPolicyModel({
    required this.description,
    required this.onlinePaymentReturnPolicies,
    required this.id,
  });

  String? description;
  List<OnlinePaymentReturnPolicy>? onlinePaymentReturnPolicies;
  int? id;

  factory OnlinePaymentPolicyModel.fromJson(Map<String, dynamic> json) =>
      OnlinePaymentPolicyModel(
        description: json["description"] == null ? null : json["description"],
        onlinePaymentReturnPolicies: json["onlinePaymentReturnPolicies"] == null
            ? null
            : List<OnlinePaymentReturnPolicy>.from(
                json["onlinePaymentReturnPolicies"]
                    .map((x) => OnlinePaymentReturnPolicy.fromJson(x))),
        id: json["id"] == null ? null : json["id"],
      );

  Map<String, dynamic> toJson() => {
        "description": description == null ? null : description,
        "onlinePaymentReturnPolicies": onlinePaymentReturnPolicies == null
            ? null
            : List<dynamic>.from(
                onlinePaymentReturnPolicies!.map((x) => x.toJson())),
        "id": id == null ? null : id,
      };
}

class OnlinePaymentReturnPolicy {
  OnlinePaymentReturnPolicy({
    required this.fromTime,
    required this.toTime,
    required this.amount,
    required this.description,
    required this.id,
  });

  int? fromTime;
  int? toTime;
  int? amount;
  String? description;
  int? id;

  factory OnlinePaymentReturnPolicy.fromJson(Map<String, dynamic> json) =>
      OnlinePaymentReturnPolicy(
        fromTime: json["fromTime"] == null ? null : json["fromTime"],
        toTime: json["toTime"] == null ? null : json["toTime"],
        amount: json["amount"] == null ? null : json["amount"],
        description: json["description"] == null ? null : json["description"],
        id: json["id"] == null ? null : json["id"],
      );

  Map<String, dynamic> toJson() => {
        "fromTime": fromTime == null ? null : fromTime,
        "toTime": toTime == null ? null : toTime,
        "amount": amount == null ? null : amount,
        "description": description,
        "id": id == null ? null : id,
      };
}

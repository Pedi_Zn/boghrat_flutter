import 'dart:convert';

List<AllOnlineAppointmentTypeModel> allOnlineAppointmentTypeModelFromJson(
        String? str) =>
    List<AllOnlineAppointmentTypeModel>.from(json
        .decode(str!)
        .map((x) => AllOnlineAppointmentTypeModel.fromJson(x)));

String? allOnlineAppointmentTypeModelToJson(
        List<AllOnlineAppointmentTypeModel> data) =>
    json.encode(List<dynamic>.from(data.map((x) => x.toJson())));

class AllOnlineAppointmentTypeModel {
  AllOnlineAppointmentTypeModel({
    required this.standardCodeId,
    required this.standardCodeTitle,
    required this.onlinePaymentPolicy,
    required this.forms,
    required this.appointmentTypeTemplate,
    required this.clinicId,
    required this.title,
    required this.description,
    required this.duration,
    required this.isOnline,
    required this.flag,
    required this.capacity,
    required this.active,
    required this.price,
    required this.currency,
    required this.order,
    required this.visitType,
    required this.ticketingTimeout,
    required this.id,
  });

  int? standardCodeId;
  String? standardCodeTitle;
  int? onlinePaymentPolicy;
  String? forms;
  String? appointmentTypeTemplate;
  int? clinicId;
  String? title;
  String? description;
  int? duration;
  bool? isOnline;
  int? flag;
  int? capacity;
  bool? active;
  int? price;
  int? currency;
  int? order;
  int? visitType;
  int? ticketingTimeout;
  int? id;

  factory AllOnlineAppointmentTypeModel.fromJson(Map<String?, dynamic> json) =>
      AllOnlineAppointmentTypeModel(
        standardCodeId:
            json["standardCodeId"] == null ? null : json["standardCodeId"],
        standardCodeTitle: json["standardCodeTitle"] == null
            ? null
            : json["standardCodeTitle"],
        onlinePaymentPolicy: json["onlinePaymentPolicy"] == null
            ? null
            : json["onlinePaymentPolicy"],
        forms: json["forms"] == null ? null : json["forms"],
        appointmentTypeTemplate: json["appointmentTypeTemplate"] == null
            ? null
            : json["appointmentTypeTemplate"],
        clinicId: json["clinicId"] == null ? null : json["clinicId"],
        title: json["title"] == null ? null : json["title"],
        description: json["description"] == null ? null : json["description"],
        duration: json["duration"] == null ? null : json["duration"],
        isOnline: json["isOnline"] == null ? null : json["isOnline"],
        flag: json["flag"] == null ? null : json["flag"],
        capacity: json["capacity"] == null ? null : json["capacity"],
        active: json["active"] == null ? null : json["active"],
        price: json["price"] == null ? null : json["price"],
        currency: json["currency"] == null ? null : json["currency"],
        order: json["order"] == null ? null : json["order"],
        visitType: json["visitType"] == null ? null : json["visitType"],
        ticketingTimeout:
            json["ticketingTimeout"] == null ? null : json["ticketingTimeout"],
        id: json["id"] == null ? null : json["id"],
      );

  Map<String?, dynamic> toJson() => {
        "standardCodeId": standardCodeId == null ? null : standardCodeId,
        "standardCodeTitle":
            standardCodeTitle == null ? null : standardCodeTitle,
        "onlinePaymentPolicy":
            onlinePaymentPolicy == null ? null : onlinePaymentPolicy,
        "forms": forms == null ? null : forms,
        "appointmentTypeTemplate":
            appointmentTypeTemplate == null ? null : appointmentTypeTemplate,
        "clinicId": clinicId == null ? null : clinicId,
        "title": title == null ? null : title,
        "description": description == null ? null : description,
        "duration": duration == null ? null : duration,
        "isOnline": isOnline == null ? null : isOnline,
        "flag": flag == null ? null : flag,
        "capacity": capacity == null ? null : capacity,
        "active": active == null ? null : active,
        "price": price == null ? null : price,
        "currency": currency == null ? null : currency,
        "order": order == null ? null : order,
        "visitType": visitType == null ? null : visitType,
        "ticketingTimeout": ticketingTimeout == null ? null : ticketingTimeout,
        "id": id == null ? null : id,
      };
}

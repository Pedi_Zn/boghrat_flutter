import 'dart:convert';

List<AllDoctorHasOnlineReceptionOfClinicModel>
    allDoctorHasOnlineReceptionOfClinicModelFromJson(String? str) =>
        List<AllDoctorHasOnlineReceptionOfClinicModel>.from(json
            .decode(str!)
            .map((x) => AllDoctorHasOnlineReceptionOfClinicModel.fromJson(x)));

String? allDoctorHasOnlineReceptionOfClinicModelToJson(
        List<AllDoctorHasOnlineReceptionOfClinicModel> data) =>
    json.encode(List<dynamic>.from(data.map((x) => x.toJson())));

class AllDoctorHasOnlineReceptionOfClinicModel {
  AllDoctorHasOnlineReceptionOfClinicModel({
    required this.doctorSchedules,
    required this.medicalNo,
    required this.medicalOrganization,
    required this.appointmentTypes,
    required this.order,
    required this.expertises,
    required this.genderId,
    required this.maritalStatus,
    required this.nationalCode,
    required this.fatherName,
    required this.birthDate,
    required this.deathDate,
    required this.birthDatePersian,
    required this.deathDatePersian,
    required this.registerDate,
    required this.pRegisterDate,
    required this.phoneNumber,
    required this.tell,
    required this.country,
    required this.id,
    required this.confirmUser,
    required this.profileImageId,
    required this.profileImage,
    required this.userName,
    required this.selfRegister,
    required this.type,
    required this.gender,
    required this.firstName,
    required this.lastName,
    required this.pageAddress,
    required this.legalPageAddresses,
    required this.fullName,
  });

  dynamic doctorSchedules;
  String? medicalNo;
  int? medicalOrganization;
  List<AppointmentType>? appointmentTypes;
  int? order;
  List<Expertise>? expertises;
  int? genderId;
  dynamic maritalStatus;
  String? nationalCode;
  dynamic fatherName;
  dynamic birthDate;
  dynamic deathDate;
  String? birthDatePersian;
  String? deathDatePersian;
  String? registerDate;
  String? pRegisterDate;
  String? phoneNumber;
  String? tell;
  dynamic country;
  String? id;
  bool? confirmUser;
  String? profileImageId;
  dynamic profileImage;
  dynamic userName;
  bool? selfRegister;
  int? type;
  int? gender;
  String? firstName;
  String? lastName;
  String? pageAddress;
  dynamic legalPageAddresses;
  String? fullName;

  factory AllDoctorHasOnlineReceptionOfClinicModel.fromJson(
          Map<String?, dynamic> json) =>
      AllDoctorHasOnlineReceptionOfClinicModel(
        doctorSchedules: json["doctorSchedules"],
        medicalNo: json["medicalNo"] == null ? null : json["medicalNo"],
        medicalOrganization: json["medicalOrganization"] == null
            ? null
            : json["medicalOrganization"],
        appointmentTypes: json["appointmentTypes"] == null
            ? null
            : List<AppointmentType>.from(json["appointmentTypes"]
                .map((x) => AppointmentType.fromJson(x))),
        order: json["order"] == null ? null : json["order"],
        expertises: json["expertises"] == null
            ? null
            : List<Expertise>.from(
                json["expertises"].map((x) => Expertise.fromJson(x))),
        genderId: json["genderId"] == null ? null : json["genderId"],
        maritalStatus: json["maritalStatus"],
        nationalCode:
            json["nationalCode"] == null ? null : json["nationalCode"],
        fatherName: json["fatherName"],
        birthDate: json["birthDate"],
        deathDate: json["deathDate"],
        birthDatePersian:
            json["birthDatePersian"] == null ? null : json["birthDatePersian"],
        deathDatePersian:
            json["deathDatePersian"] == null ? null : json["deathDatePersian"],
        registerDate:
            json["registerDate"] == null ? null : json["registerDate"],
        pRegisterDate:
            json["pRegisterDate"] == null ? null : json["pRegisterDate"],
        phoneNumber: json["phoneNumber"] == null ? null : json["phoneNumber"],
        tell: json["tell"] == null ? null : json["tell"],
        country: json["country"],
        id: json["id"] == null ? null : json["id"],
        confirmUser: json["confirmUser"] == null ? null : json["confirmUser"],
        profileImageId:
            json["profileImageId"] == null ? null : json["profileImageId"],
        profileImage: json["profileImage"],
        userName: json["userName"],
        selfRegister:
            json["selfRegister"] == null ? null : json["selfRegister"],
        type: json["type"] == null ? null : json["type"],
        gender: json["gender"] == null ? null : json["gender"],
        firstName: json["firstName"] == null ? null : json["firstName"],
        lastName: json["lastName"] == null ? null : json["lastName"],
        pageAddress: json["pageAddress"] == null ? null : json["pageAddress"],
        legalPageAddresses: json["legalPageAddresses"],
        fullName: json["fullName"] == null ? null : json["fullName"],
      );

  Map<String?, dynamic> toJson() => {
        "doctorSchedules": doctorSchedules,
        "medicalNo": medicalNo == null ? null : medicalNo,
        "medicalOrganization":
            medicalOrganization == null ? null : medicalOrganization,
        "appointmentTypes": appointmentTypes == null
            ? null
            : List<dynamic>.from(appointmentTypes!.map((x) => x.toJson())),
        "order": order == null ? null : order,
        "expertises": expertises == null
            ? null
            : List<dynamic>.from(expertises!.map((x) => x.toJson())),
        "genderId": genderId == null ? null : genderId,
        "maritalStatus": maritalStatus,
        "nationalCode": nationalCode == null ? null : nationalCode,
        "fatherName": fatherName,
        "birthDate": birthDate,
        "deathDate": deathDate,
        "birthDatePersian": birthDatePersian == null ? null : birthDatePersian,
        "deathDatePersian": deathDatePersian == null ? null : deathDatePersian,
        "registerDate": registerDate == null ? null : registerDate,
        "pRegisterDate": pRegisterDate == null ? null : pRegisterDate,
        "phoneNumber": phoneNumber == null ? null : phoneNumber,
        "tell": tell == null ? null : tell,
        "country": country,
        "id": id == null ? null : id,
        "confirmUser": confirmUser == null ? null : confirmUser,
        "profileImageId": profileImageId == null ? null : profileImageId,
        "profileImage": profileImage,
        "userName": userName,
        "selfRegister": selfRegister == null ? null : selfRegister,
        "type": type == null ? null : type,
        "gender": gender == null ? null : gender,
        "firstName": firstName == null ? null : firstName,
        "lastName": lastName == null ? null : lastName,
        "pageAddress": pageAddress == null ? null : pageAddress,
        "legalPageAddresses": legalPageAddresses,
        "fullName": fullName == null ? null : fullName,
      };
}

class AppointmentType {
  AppointmentType({
    required this.standardCodeId,
    required this.standardCodeTitle,
    required this.onlinePaymentPolicy,
    required this.forms,
    required this.appointmentTypeTemplate,
    required this.clinicId,
    required this.title,
    required this.description,
    required this.duration,
    required this.isOnline,
    required this.flag,
    required this.capacity,
    required this.active,
    required this.price,
    required this.currency,
    required this.order,
    required this.visitType,
    required this.ticketingTimeout,
    required this.id,
  });

  dynamic standardCodeId;
  dynamic standardCodeTitle;
  OnlinePaymentPolicy? onlinePaymentPolicy;
  List<dynamic>? forms;
  dynamic appointmentTypeTemplate;
  int? clinicId;
  String? title;
  String? description;
  int? duration;
  bool? isOnline;
  int? flag;
  int? capacity;
  bool? active;
  int? price;
  int? currency;
  int? order;
  int? visitType;
  int? ticketingTimeout;
  int? id;

  factory AppointmentType.fromJson(Map<String?, dynamic> json) =>
      AppointmentType(
        standardCodeId: json["standardCodeId"],
        standardCodeTitle: json["standardCodeTitle"],
        onlinePaymentPolicy: json["onlinePaymentPolicy"] == null
            ? null
            : OnlinePaymentPolicy.fromJson(json["onlinePaymentPolicy"]),
        forms: json["forms"] == null
            ? null
            : List<dynamic>.from(json["forms"].map((x) => x)),
        appointmentTypeTemplate: json["appointmentTypeTemplate"],
        clinicId: json["clinicId"] == null ? null : json["clinicId"],
        title: json["title"] == null ? null : json["title"],
        description: json["description"] == null ? null : json["description"],
        duration: json["duration"] == null ? null : json["duration"],
        isOnline: json["isOnline"] == null ? null : json["isOnline"],
        flag: json["flag"] == null ? null : json["flag"],
        capacity: json["capacity"] == null ? null : json["capacity"],
        active: json["active"] == null ? null : json["active"],
        price: json["price"] == null ? null : json["price"],
        currency: json["currency"] == null ? null : json["currency"],
        order: json["order"] == null ? null : json["order"],
        visitType: json["visitType"] == null ? null : json["visitType"],
        ticketingTimeout:
            json["ticketingTimeout"] == null ? null : json["ticketingTimeout"],
        id: json["id"] == null ? null : json["id"],
      );

  Map<String?, dynamic> toJson() => {
        "standardCodeId": standardCodeId,
        "standardCodeTitle": standardCodeTitle,
        "onlinePaymentPolicy":
            onlinePaymentPolicy == null ? null : onlinePaymentPolicy!.toJson(),
        "forms":
            forms == null ? null : List<dynamic>.from(forms!.map((x) => x)),
        "appointmentTypeTemplate": appointmentTypeTemplate,
        "clinicId": clinicId == null ? null : clinicId,
        "title": title == null ? null : title,
        "description": description == null ? null : description,
        "duration": duration == null ? null : duration,
        "isOnline": isOnline == null ? null : isOnline,
        "flag": flag == null ? null : flag,
        "capacity": capacity == null ? null : capacity,
        "active": active == null ? null : active,
        "price": price == null ? null : price,
        "currency": currency == null ? null : currency,
        "order": order == null ? null : order,
        "visitType": visitType == null ? null : visitType,
        "ticketingTimeout": ticketingTimeout == null ? null : ticketingTimeout,
        "id": id == null ? null : id,
      };
}

class OnlinePaymentPolicy {
  OnlinePaymentPolicy({
    required this.description,
    required this.onlinePaymentReturnPolicies,
    required this.id,
  });

  String? description;
  List<OnlinePaymentReturnPolicy>? onlinePaymentReturnPolicies;
  int? id;

  factory OnlinePaymentPolicy.fromJson(Map<String?, dynamic> json) =>
      OnlinePaymentPolicy(
        description: json["description"] == null ? null : json["description"],
        onlinePaymentReturnPolicies: json["onlinePaymentReturnPolicies"] == null
            ? null
            : List<OnlinePaymentReturnPolicy>.from(
                json["onlinePaymentReturnPolicies"]
                    .map((x) => OnlinePaymentReturnPolicy.fromJson(x))),
        id: json["id"] == null ? null : json["id"],
      );

  Map<String?, dynamic> toJson() => {
        "description": description == null ? null : description,
        "onlinePaymentReturnPolicies": onlinePaymentReturnPolicies == null
            ? null
            : List<dynamic>.from(
                onlinePaymentReturnPolicies!.map((x) => x.toJson())),
        "id": id == null ? null : id,
      };
}

class OnlinePaymentReturnPolicy {
  OnlinePaymentReturnPolicy({
    required this.fromTime,
    required this.toTime,
    required this.amount,
    required this.description,
    required this.id,
  });

  int? fromTime;
  int? toTime;
  int? amount;
  dynamic description;
  int? id;

  factory OnlinePaymentReturnPolicy.fromJson(Map<String?, dynamic> json) =>
      OnlinePaymentReturnPolicy(
        fromTime: json["fromTime"] == null ? null : json["fromTime"],
        toTime: json["toTime"] == null ? null : json["toTime"],
        amount: json["amount"] == null ? null : json["amount"],
        description: json["description"],
        id: json["id"] == null ? null : json["id"],
      );

  Map<String?, dynamic> toJson() => {
        "fromTime": fromTime == null ? null : fromTime,
        "toTime": toTime == null ? null : toTime,
        "amount": amount == null ? null : amount,
        "description": description,
        "id": id == null ? null : id,
      };
}

class Expertise {
  Expertise({
    required this.key,
    required this.value,
  });

  String? key;
  String? value;

  factory Expertise.fromJson(Map<String?, dynamic> json) => Expertise(
        key: json["key"] == null ? null : json["key"],
        value: json["value"] == null ? null : json["value"],
      );

  Map<String?, dynamic> toJson() => {
        "key": key == null ? null : key,
        "value": value == null ? null : value,
      };
}

import 'dart:convert';

DoctorSocialModel doctorSocialModelFromJson(String str) =>
    DoctorSocialModel.fromJson(json.decode(str));

String doctorSocialModelToJson(DoctorSocialModel data) =>
    json.encode(data.toJson());

class DoctorSocialModel {
  DoctorSocialModel({
    required this.doctor,
    required this.website,
    required this.telegram,
    required this.instagram,
    required this.aparat,
    required this.facebook,
    required this.twitter,
    required this.linkedIn,
    required this.gPlus,
  });

  String? doctor;
  String? website;
  String? telegram;
  String? instagram;
  String? aparat;
  String? facebook;
  String? twitter;
  String? linkedIn;
  String? gPlus;

  factory DoctorSocialModel.fromJson(Map<String?, dynamic> json) =>
      DoctorSocialModel(
        doctor: json["doctor"] == null ? null : json["doctor"],
        website: json["website"] == null ? null : json["website"],
        telegram: json["telegram"] == null ? null : json["telegram"],
        instagram: json["instagram"] == null ? null : json["instagram"],
        aparat: json["aparat"] == null ? null : json["aparat"],
        facebook: json["facebook"] == null ? null : json["facebook"],
        twitter: json["twitter"] == null ? null : json["twitter"],
        linkedIn: json["linkedIn"] == null ? null : json["linkedIn"],
        gPlus: json["gPlus"] == null ? null : json["gPlus"],
      );

  Map<String, dynamic> toJson() => {
        "doctor": doctor == null ? null : doctor,
        "website": website == null ? null : website,
        "telegram": telegram == null ? null : telegram,
        "instagram": instagram == null ? null : instagram,
        "aparat": aparat == null ? null : aparat,
        "facebook": facebook == null ? null : facebook,
        "twitter": twitter == null ? null : twitter,
        "linkedIn": linkedIn == null ? null : linkedIn,
        "gPlus": gPlus == null ? null : gPlus,
      };
}

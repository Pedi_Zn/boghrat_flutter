import 'dart:convert';

List<AllSchedulesOfClinicModel> allSchedulesOfClinicModelFromJson(String str) =>
    List<AllSchedulesOfClinicModel>.from(
        json.decode(str).map((x) => AllSchedulesOfClinicModel.fromJson(x)));

String allSchedulesOfClinicModelToJson(List<AllSchedulesOfClinicModel> data) =>
    json.encode(List<dynamic>.from(data.map((x) => x.toJson())));

class AllSchedulesOfClinicModel {
  AllSchedulesOfClinicModel({
    required this.clinicId,
    required this.daysOfWeek,
    required this.startTime,
    required this.endTime,
    required this.active,
    required this.id,
  });

  int? clinicId;
  int? daysOfWeek;
  String? startTime;
  String? endTime;
  bool? active;
  int? id;

  factory AllSchedulesOfClinicModel.fromJson(Map<String, dynamic> json) =>
      AllSchedulesOfClinicModel(
        clinicId: json["clinicId"] == null ? null : json["clinicId"],
        daysOfWeek: json["daysOfWeek"] == null ? null : json["daysOfWeek"],
        startTime: json["startTime"] == null ? null : json["startTime"],
        endTime: json["endTime"] == null ? null : json["endTime"],
        active: json["active"] == null ? null : json["active"],
        id: json["id"] == null ? null : json["id"],
      );

  Map<String, dynamic> toJson() => {
        "clinicId": clinicId == null ? null : clinicId,
        "daysOfWeek": daysOfWeek == null ? null : daysOfWeek,
        "startTime": startTime == null ? null : startTime,
        "endTime": endTime == null ? null : endTime,
        "active": active == null ? null : active,
        "id": id == null ? null : id,
      };
}

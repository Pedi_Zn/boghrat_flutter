import 'dart:convert';

List<OnlineReceptionFreeTimeModel> onlineReceptionFreeTimeModelFromJson(
        String? str) =>
    List<OnlineReceptionFreeTimeModel>.from(
        json.decode(str!).map((x) => OnlineReceptionFreeTimeModel.fromJson(x)));

String? onlineReceptionFreeTimeModelToJson(
        List<OnlineReceptionFreeTimeModel> data) =>
    json.encode(List<dynamic>.from(data.map((x) => x.toJson())));

class OnlineReceptionFreeTimeModel {
  OnlineReceptionFreeTimeModel({
    required this.scheduleId,
    required this.date,
    required this.dayOfWeek,
    required this.pDate,
    required this.startTime,
    required this.endTime,
    required this.doctorId,
    required this.clinicId,
    required this.patientId,
    required this.description,
    required this.sourceId,
    required this.appointmentType,
  });

  int? scheduleId;
  String? date;
  int? dayOfWeek;
  String? pDate;
  String? startTime;
  String? endTime;
  String? doctorId;
  int? clinicId;
  int? patientId;
  String? description;
  int? sourceId;
  String? appointmentType;

  factory OnlineReceptionFreeTimeModel.fromJson(Map<String?, dynamic> json) =>
      OnlineReceptionFreeTimeModel(
        scheduleId: json["scheduleId"] == null ? null : json["scheduleId"],
        date: json["date"] == null ? null : json["date"],
        dayOfWeek: json["dayOfWeek"] == null ? null : json["dayOfWeek"],
        pDate: json["pDate"] == null ? null : json["pDate"],
        startTime: json["startTime"] == null ? null : json["startTime"],
        endTime: json["endTime"] == null ? null : json["endTime"],
        doctorId: json["doctorId"] == null ? null : json["doctorId"],
        clinicId: json["clinicId"] == null ? null : json["clinicId"],
        patientId: json["patientId"],
        description: json["description"],
        sourceId: json["sourceId"],
        appointmentType: json["appointmentType"],
      );

  Map<String?, dynamic> toJson() => {
        "scheduleId": scheduleId == null ? null : scheduleId,
        "date": date == null ? null : date,
        "dayOfWeek": dayOfWeek == null ? null : dayOfWeek,
        "pDate": pDate == null ? null : pDate,
        "startTime": startTime == null ? null : startTime,
        "endTime": endTime == null ? null : endTime,
        "doctorId": doctorId == null ? null : doctorId,
        "clinicId": clinicId == null ? null : clinicId,
        "patientId": patientId,
        "description": description,
        "sourceId": sourceId,
        "appointmentType": appointmentType,
      };
}

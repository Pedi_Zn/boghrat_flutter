import 'dart:convert';

List<ClinicGalleryModel> clinicGalleryModelFromJson(String? str) =>
    List<ClinicGalleryModel>.from(
        json.decode(str!).map((x) => ClinicGalleryModel.fromJson(x)));

String clinicGalleryModelToJson(List<ClinicGalleryModel> data) =>
    json.encode(List<dynamic>.from(data.map((x) => x.toJson())));

class ClinicGalleryModel {
  ClinicGalleryModel({
    required this.id,
    required this.description,
  });

  String? id;
  String? description;

  factory ClinicGalleryModel.fromJson(Map<String, dynamic> json) =>
      ClinicGalleryModel(
        id: json["id"] == null ? null : json["id"],
        description: json["description"] == null ? null : json["description"],
      );

  Map<String, dynamic> toJson() => {
        "id": id == null ? null : id,
        "description": description == null ? null : description,
      };
}

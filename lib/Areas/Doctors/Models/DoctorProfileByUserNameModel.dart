import 'dart:convert';

DoctorProfileByUserNameModel doctorProfileByUserNameModelFromJson(
        String? str) =>
    DoctorProfileByUserNameModel.fromJson(json.decode(str!));

String? doctorProfileByUserNameModelToJson(DoctorProfileByUserNameModel data) =>
    json.encode(data.toJson());

class DoctorProfileByUserNameModel {
  DoctorProfileByUserNameModel({
    required this.medicalNo,
    required this.pageAddress,
    required this.medicalNoVerified,
    required this.pageAddressVerified,
    required this.expertises,
    required this.email,
    required this.profileImageId,
    required this.bio,
    required this.bioImageIds,
    required this.genderId,
    required this.maritalStatus,
    required this.nationalCode,
    required this.fatherName,
    required this.birthDate,
    required this.deathDate,
    required this.birthDatePersian,
    required this.deathDatePersian,
    required this.registerDate,
    required this.pRegisterDate,
    required this.phoneNumber,
    required this.tell,
    required this.country,
    required this.id,
    required this.confirmUser,
    required this.profileImage,
    required this.userName,
    required this.selfRegister,
    required this.type,
    required this.gender,
    required this.firstName,
    required this.lastName,
    required this.legalPageAddresses,
    required this.fullName,
  });

  String? medicalNo;
  String? pageAddress;
  bool? medicalNoVerified;
  bool? pageAddressVerified;
  List<Expertise>? expertises;
  String? email;
  String? profileImageId;
  String? bio;
  List<String?>? bioImageIds;
  int? genderId;
  dynamic maritalStatus;
  String? nationalCode;
  dynamic fatherName;
  dynamic birthDate;
  dynamic deathDate;
  String? birthDatePersian;
  String? deathDatePersian;
  String? registerDate;
  String? pRegisterDate;
  String? phoneNumber;
  String? tell;
  dynamic country;
  String? id;
  bool? confirmUser;
  dynamic profileImage;
  dynamic userName;
  bool? selfRegister;
  int? type;
  int? gender;
  String? firstName;
  String? lastName;
  dynamic legalPageAddresses;
  String? fullName;

  factory DoctorProfileByUserNameModel.fromJson(Map<String?, dynamic> json) =>
      DoctorProfileByUserNameModel(
        medicalNo: json["medicalNo"] == null ? null : json["medicalNo"],
        pageAddress: json["pageAddress"] == null ? null : json["pageAddress"],
        medicalNoVerified: json["medicalNoVerified"] == null
            ? null
            : json["medicalNoVerified"],
        pageAddressVerified: json["pageAddressVerified"] == null
            ? null
            : json["pageAddressVerified"],
        expertises: json["expertises"] == null
            ? null
            : List<Expertise>.from(
                json["expertises"].map((x) => Expertise.fromJson(x))),
        email: json["email"] == null ? null : json["email"],
        profileImageId:
            json["profileImageId"] == null ? null : json["profileImageId"],
        bio: json["bio"] == null ? null : json["bio"],
        bioImageIds: json["bioImageIds"] == null
            ? null
            : List<String?>.from(json["bioImageIds"].map((x) => x)),
        genderId: json["genderId"] == null ? null : json["genderId"],
        maritalStatus: json["maritalStatus"],
        nationalCode:
            json["nationalCode"] == null ? null : json["nationalCode"],
        fatherName: json["fatherName"],
        birthDate: json["birthDate"],
        deathDate: json["deathDate"],
        birthDatePersian:
            json["birthDatePersian"] == null ? null : json["birthDatePersian"],
        deathDatePersian:
            json["deathDatePersian"] == null ? null : json["deathDatePersian"],
        registerDate:
            json["registerDate"] == null ? null : json["registerDate"],
        pRegisterDate:
            json["pRegisterDate"] == null ? null : json["pRegisterDate"],
        phoneNumber: json["phoneNumber"] == null ? null : json["phoneNumber"],
        tell: json["tell"] == null ? null : json["tell"],
        country: json["country"],
        id: json["id"] == null ? null : json["id"],
        confirmUser: json["confirmUser"] == null ? null : json["confirmUser"],
        profileImage: json["profileImage"],
        userName: json["userName"],
        selfRegister:
            json["selfRegister"] == null ? null : json["selfRegister"],
        type: json["type"] == null ? null : json["type"],
        gender: json["gender"] == null ? null : json["gender"],
        firstName: json["firstName"] == null ? null : json["firstName"],
        lastName: json["lastName"] == null ? null : json["lastName"],
        legalPageAddresses: json["legalPageAddresses"],
        fullName: json["fullName"] == null ? null : json["fullName"],
      );

  Map<String?, dynamic> toJson() => {
        "medicalNo": medicalNo == null ? null : medicalNo,
        "pageAddress": pageAddress == null ? null : pageAddress,
        "medicalNoVerified":
            medicalNoVerified == null ? null : medicalNoVerified,
        "pageAddressVerified":
            pageAddressVerified == null ? null : pageAddressVerified,
        "expertises": expertises == null
            ? null
            : List<dynamic>.from(expertises!.map((x) => x.toJson())),
        "email": email == null ? null : email,
        "profileImageId": profileImageId == null ? null : profileImageId,
        "bio": bio == null ? null : bio,
        "bioImageIds": bioImageIds == null
            ? null
            : List<dynamic>.from(bioImageIds!.map((x) => x)),
        "genderId": genderId == null ? null : genderId,
        "maritalStatus": maritalStatus,
        "nationalCode": nationalCode == null ? null : nationalCode,
        "fatherName": fatherName,
        "birthDate": birthDate,
        "deathDate": deathDate,
        "birthDatePersian": birthDatePersian == null ? null : birthDatePersian,
        "deathDatePersian": deathDatePersian == null ? null : deathDatePersian,
        "registerDate": registerDate == null ? null : registerDate,
        "pRegisterDate": pRegisterDate == null ? null : pRegisterDate,
        "phoneNumber": phoneNumber == null ? null : phoneNumber,
        "tell": tell == null ? null : tell,
        "country": country,
        "id": id == null ? null : id,
        "confirmUser": confirmUser == null ? null : confirmUser,
        "profileImage": profileImage,
        "userName": userName,
        "selfRegister": selfRegister == null ? null : selfRegister,
        "type": type == null ? null : type,
        "gender": gender == null ? null : gender,
        "firstName": firstName == null ? null : firstName,
        "lastName": lastName == null ? null : lastName,
        "legalPageAddresses": legalPageAddresses,
        "fullName": fullName == null ? null : fullName,
      };
}

class Expertise {
  Expertise({
    required this.expertiseId,
    required this.expertiseDegreeId,
    required this.title,
    required this.degreeTitle,
  });

  int? expertiseId;
  int? expertiseDegreeId;
  String? title;
  String? degreeTitle;

  factory Expertise.fromJson(Map<String?, dynamic> json) => Expertise(
        expertiseId: json["expertiseId"] == null ? null : json["expertiseId"],
        expertiseDegreeId: json["expertiseDegreeId"] == null
            ? null
            : json["expertiseDegreeId"],
        title: json["title"] == null ? null : json["title"],
        degreeTitle: json["degreeTitle"] == null ? null : json["degreeTitle"],
      );

  Map<String?, dynamic> toJson() => {
        "expertiseId": expertiseId == null ? null : expertiseId,
        "expertiseDegreeId":
            expertiseDegreeId == null ? null : expertiseDegreeId,
        "title": title == null ? null : title,
        "degreeTitle": degreeTitle == null ? null : degreeTitle,
      };
}

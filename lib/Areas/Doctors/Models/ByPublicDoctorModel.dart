import 'dart:convert';

List<ByPublicDoctorModel> byPublicDoctorModelFromJson(String? str) =>
    List<ByPublicDoctorModel>.from(
        json.decode(str!).map((x) => ByPublicDoctorModel.fromJson(x)));

String? byPublicDoctorModelToJson(List<ByPublicDoctorModel> data) =>
    json.encode(List<dynamic>.from(data.map((x) => x.toJson())));

class ByPublicDoctorModel {
  ByPublicDoctorModel({
    required this.inPlaceAppointment,
    required this.telemedTicketing,
    required this.telemedTell,
    required this.telemedVideo,
    required this.expirePlan,
    required this.legalPageAddress,
    required this.doctorPageAddress,
    required this.insurances,
    required this.averageVisitTime,
    required this.fromDateLaterOnline,
    required this.toDateLaterOnline,
    required this.city,
    required this.province,
    required this.region,
    required this.district,
    required this.lat,
    required this.lng,
    required this.rooms,
    required this.clinicConfig,
    required this.iban,
    required this.loginRequired,
    required this.clinicTells,
    required this.address,
    required this.doctorId,
    required this.hasHis,
    required this.isActive,
    required this.title,
    required this.type,
    required this.regulartoryId,
    required this.id,
  });

  bool? inPlaceAppointment;
  bool? telemedTicketing;
  bool? telemedTell;
  bool? telemedVideo;
  bool? expirePlan;
  String? legalPageAddress;
  String? doctorPageAddress;
  List<dynamic>? insurances;
  int? averageVisitTime;
  int? fromDateLaterOnline;
  int? toDateLaterOnline;
  City? city;
  Province? province;
  dynamic region;
  dynamic district;
  double? lat;
  double? lng;
  List<Room>? rooms;
  dynamic clinicConfig;
  dynamic iban;
  bool? loginRequired;
  List<ClinicTell>? clinicTells;
  String? address;
  String? doctorId;
  bool? hasHis;
  bool? isActive;
  String? title;
  Type? type;
  dynamic regulartoryId;
  int? id;

  factory ByPublicDoctorModel.fromJson(Map<String?, dynamic> json) =>
      ByPublicDoctorModel(
        inPlaceAppointment: json["inPlaceAppointment"] == null
            ? null
            : json["inPlaceAppointment"],
        telemedTicketing:
            json["telemedTicketing"] == null ? null : json["telemedTicketing"],
        telemedTell: json["telemedTell"] == null ? null : json["telemedTell"],
        telemedVideo:
            json["telemedVideo"] == null ? null : json["telemedVideo"],
        expirePlan: json["expirePlan"] == null ? null : json["expirePlan"],
        legalPageAddress:
            json["legalPageAddress"] == null ? null : json["legalPageAddress"],
        doctorPageAddress: json["doctorPageAddress"] == null
            ? null
            : json["doctorPageAddress"],
        insurances: json["insurances"] == null
            ? null
            : List<dynamic>.from(json["insurances"].map((x) => x)),
        averageVisitTime:
            json["averageVisitTime"] == null ? null : json["averageVisitTime"],
        fromDateLaterOnline: json["fromDateLaterOnline"] == null
            ? null
            : json["fromDateLaterOnline"],
        toDateLaterOnline: json["toDateLaterOnline"] == null
            ? null
            : json["toDateLaterOnline"],
        city: json["city"] == null ? null : City.fromJson(json["city"]),
        province: json["province"] == null
            ? null
            : Province.fromJson(json["province"]),
        region: json["region"],
        district: json["district"],
        lat: json["lat"] == null ? null : json["lat"].toDouble(),
        lng: json["lng"] == null ? null : json["lng"].toDouble(),
        rooms: json["rooms"] == null
            ? null
            : List<Room>.from(json["rooms"].map((x) => Room.fromJson(x))),
        clinicConfig: json["clinicConfig"],
        iban: json["iban"],
        loginRequired:
            json["loginRequired"] == null ? null : json["loginRequired"],
        clinicTells: json["clinicTells"] == null
            ? null
            : List<ClinicTell>.from(
                json["clinicTells"].map((x) => ClinicTell.fromJson(x))),
        address: json["address"] == null ? null : json["address"],
        doctorId: json["doctorId"] == null ? null : json["doctorId"],
        hasHis: json["hasHIS"] == null ? null : json["hasHIS"],
        isActive: json["isActive"] == null ? null : json["isActive"],
        title: json["title"] == null ? null : json["title"],
        type: json["type"] == null ? null : Type.fromJson(json["type"]),
        regulartoryId: json["regulartoryId"],
        id: json["id"] == null ? null : json["id"],
      );

  Map<String?, dynamic> toJson() => {
        "inPlaceAppointment":
            inPlaceAppointment == null ? null : inPlaceAppointment,
        "telemedTicketing": telemedTicketing == null ? null : telemedTicketing,
        "telemedTell": telemedTell == null ? null : telemedTell,
        "telemedVideo": telemedVideo == null ? null : telemedVideo,
        "expirePlan": expirePlan == null ? null : expirePlan,
        "legalPageAddress": legalPageAddress == null ? null : legalPageAddress,
        "doctorPageAddress":
            doctorPageAddress == null ? null : doctorPageAddress,
        "insurances": insurances == null
            ? null
            : List<dynamic>.from(insurances!.map((x) => x)),
        "averageVisitTime": averageVisitTime == null ? null : averageVisitTime,
        "fromDateLaterOnline":
            fromDateLaterOnline == null ? null : fromDateLaterOnline,
        "toDateLaterOnline":
            toDateLaterOnline == null ? null : toDateLaterOnline,
        "city": city == null ? null : city!.toJson(),
        "province": province == null ? null : province!.toJson(),
        "region": region,
        "district": district,
        "lat": lat == null ? null : lat,
        "lng": lng == null ? null : lng,
        "rooms": rooms == null
            ? null
            : List<dynamic>.from(rooms!.map((x) => x.toJson())),
        "clinicConfig": clinicConfig,
        "iban": iban,
        "loginRequired": loginRequired == null ? null : loginRequired,
        "clinicTells": clinicTells == null
            ? null
            : List<dynamic>.from(clinicTells!.map((x) => x.toJson())),
        "address": address == null ? null : address,
        "doctorId": doctorId == null ? null : doctorId,
        "hasHIS": hasHis == null ? null : hasHis,
        "isActive": isActive == null ? null : isActive,
        "title": title == null ? null : title,
        "type": type == null ? null : type!.toJson(),
        "regulartoryId": regulartoryId,
        "id": id == null ? null : id,
      };
}

class City {
  City({
    required this.provinceId,
    required this.title,
    required this.latinTitle,
    required this.lat,
    required this.lng,
    required this.tellCode,
    required this.latinDashedTitle,
    required this.id,
  });

  int? provinceId;
  String? title;
  String? latinTitle;
  double lat;
  double lng;
  String? tellCode;
  String? latinDashedTitle;
  int? id;

  factory City.fromJson(Map<String?, dynamic> json) => City(
        provinceId: json["provinceId"] == null ? null : json["provinceId"],
        title: json["title"] == null ? null : json["title"],
        latinTitle: json["latinTitle"] == null ? null : json["latinTitle"],
        lat: json["lat"] == null ? null : json["lat"].toDouble(),
        lng: json["lng"] == null ? null : json["lng"].toDouble(),
        tellCode: json["tellCode"] == null ? null : json["tellCode"],
        latinDashedTitle:
            json["latinDashedTitle"] == null ? null : json["latinDashedTitle"],
        id: json["id"] == null ? null : json["id"],
      );

  Map<String?, dynamic> toJson() => {
        "provinceId": provinceId == null ? null : provinceId,
        "title": title == null ? null : title,
        "latinTitle": latinTitle == null ? null : latinTitle,
        "lat": lat == null ? null : lat,
        "lng": lng == null ? null : lng,
        "tellCode": tellCode == null ? null : tellCode,
        "latinDashedTitle": latinDashedTitle == null ? null : latinDashedTitle,
        "id": id == null ? null : id,
      };
}

class ClinicTell {
  ClinicTell({
    required this.title,
    required this.tell,
    required this.boghratTell,
  });

  dynamic title;
  String? tell;
  bool? boghratTell;

  factory ClinicTell.fromJson(Map<String?, dynamic> json) => ClinicTell(
        title: json["title"],
        tell: json["tell"] == null ? null : json["tell"],
        boghratTell: json["boghratTell"] == null ? null : json["boghratTell"],
      );

  Map<String?, dynamic> toJson() => {
        "title": title,
        "tell": tell == null ? null : tell,
        "boghratTell": boghratTell == null ? null : boghratTell,
      };
}

class Province {
  Province({
    required this.provinceId,
    required this.title,
    required this.latinTitle,
    required this.lat,
    required this.lng,
    required this.tellCode,
    required this.latinDashedTitle,
    required this.id,
  });

  dynamic provinceId;
  String? title;
  String? latinTitle;
  double lat;
  double lng;
  String? tellCode;
  String? latinDashedTitle;
  int? id;

  factory Province.fromJson(Map<String?, dynamic> json) => Province(
        provinceId: json["provinceId"],
        title: json["title"] == null ? null : json["title"],
        latinTitle: json["latinTitle"] == null ? null : json["latinTitle"],
        lat: json["lat"] == null ? null : json["lat"].toDouble(),
        lng: json["lng"] == null ? null : json["lng"].toDouble(),
        tellCode: json["tellCode"] == null ? null : json["tellCode"],
        latinDashedTitle:
            json["latinDashedTitle"] == null ? null : json["latinDashedTitle"],
        id: json["id"] == null ? null : json["id"],
      );

  Map<String?, dynamic> toJson() => {
        "provinceId": provinceId,
        "title": title == null ? null : title,
        "latinTitle": latinTitle == null ? null : latinTitle,
        "lat": lat == null ? null : lat,
        "lng": lng == null ? null : lng,
        "tellCode": tellCode == null ? null : tellCode,
        "latinDashedTitle": latinDashedTitle == null ? null : latinDashedTitle,
        "id": id == null ? null : id,
      };
}

class Room {
  Room({
    required this.clinic,
    required this.title,
    required this.id,
  });

  dynamic clinic;
  String? title;
  int? id;

  factory Room.fromJson(Map<String?, dynamic> json) => Room(
        clinic: json["clinic"],
        title: json["title"] == null ? null : json["title"],
        id: json["id"] == null ? null : json["id"],
      );

  Map<String?, dynamic> toJson() => {
        "clinic": clinic,
        "title": title == null ? null : title,
        "id": id == null ? null : id,
      };
}

class Type {
  Type({
    required this.clinicFeatures,
    required this.title,
    required this.code,
    required this.id,
  });

  List<dynamic>? clinicFeatures;
  String? title;
  String? code;
  int? id;

  factory Type.fromJson(Map<String?, dynamic> json) => Type(
        clinicFeatures: json["clinicFeatures"] == null
            ? null
            : List<dynamic>.from(json["clinicFeatures"].map((x) => x)),
        title: json["title"] == null ? null : json["title"],
        code: json["code"] == null ? null : json["code"],
        id: json["id"] == null ? null : json["id"],
      );

  Map<String?, dynamic> toJson() => {
        "clinicFeatures": clinicFeatures == null
            ? null
            : List<dynamic>.from(clinicFeatures!.map((x) => x)),
        "title": title == null ? null : title,
        "code": code == null ? null : code,
        "id": id == null ? null : id,
      };
}

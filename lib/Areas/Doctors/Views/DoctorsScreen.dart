import 'dart:ui';

import 'package:boghrat_flutter/Areas/Doctors/Views/BookingBottomSheet.dart';
import 'package:boghrat_flutter/Localization/Localization.dart';
import 'package:boghrat_flutter/Widgets/CustomBox.dart';
import 'package:boghrat_flutter/Widgets/CustomLittleButton.dart';
import 'package:boghrat_flutter/Widgets/CustomLongTitleRow.dart';
import 'package:boghrat_flutter/Widgets/CustomTitleRow.dart';
import 'package:boghrat_flutter/Widgets/CustomVisitTypeRow.dart';
import 'package:boghrat_flutter/test_widget.dart';
import 'package:dropdown_plus/dropdown_plus.dart';
import 'package:flutter/material.dart';
import 'package:flutter_advanced_avatar/flutter_advanced_avatar.dart';
import 'package:boghrat_flutter/Config/AppConfig.dart' as config;
import 'package:font_awesome_flutter/font_awesome_flutter.dart';
import 'package:get/get.dart';
import 'package:material_design_icons_flutter/material_design_icons_flutter.dart';
import 'package:persian_number_utility/persian_number_utility.dart';

class DoctorsScreen extends StatelessWidget {
  const DoctorsScreen({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return SafeArea(
      child: Scaffold(
        backgroundColor: config.Colors().primaryColorLight(1),
        body: SingleChildScrollView(
          child: Stack(
            children: <Widget>[
              // Align(
              //   alignment: Alignment.topCenter,
              //   child: Container(
              //     height: 400,
              //     decoration: BoxDecoration(
              //       image: DecorationImage(
              //         image: AssetImage("assets/images/profile_male_back.png"),
              //         fit: BoxFit.fill,
              //       ),
              //     ),
              //   ),
              // ),
              // CustomPaint(
              //   painter: BorderPainter(),
              //   child: Container(
              //     height: 400.0,
              //   ),
              // ),
              // Container(
              //   decoration: BoxDecoration(
              //     image: DecorationImage(
              //       repeat: ImageRepeat.repeat,
              //       colorFilter: ColorFilter.mode(
              //           Colors.black.withOpacity(0.08), BlendMode.dstATop),
              //       image: AssetImage("assets/images/doddle.png"),
              //     ),
              //   ),
              // ),
              Positioned(
                top: 20,
                right: 20,
                left: 20,
                child: Row(
                  mainAxisAlignment: MainAxisAlignment.spaceBetween,
                  children: [
                    Container(
                      decoration: BoxDecoration(
                        border: Border.all(
                          color: config.Colors().blackColor(0.5),
                        ),
                        borderRadius: BorderRadius.circular(10.0),
                        boxShadow: [
                          BoxShadow(
                            color: Colors.transparent,
                            blurRadius: 3,
                            offset: Offset(1, 1),
                          ),
                        ],
                      ),
                      child: IconButton(
                        onPressed: () {
                          Get.back();
                        },
                        icon: Icon(
                          Icons.arrow_back,
                          size: 20,
                          color: config.Colors().blackColor(0.5),
                        ),
                      ),
                    ),
                    Container(
                      decoration: BoxDecoration(
                        border:
                            Border.all(color: config.Colors().blackColor(0.5)),
                        borderRadius: BorderRadius.circular(10.0),
                      ),
                      child: IconButton(
                        onPressed: () {
                          Get.back();
                        },
                        icon: Icon(
                          Icons.share_outlined,
                          size: 23,
                          color: config.Colors().blackColor(0.5),
                        ),
                      ),
                    ),
                  ],
                ),
              ),
              Column(
                children: [
                  Container(
                    height: 415,
                    child: Stack(
                      children: [
                        Positioned(
                          top: 130,
                          left: 20,
                          right: 20,
                          height: 265,
                          child: ClipRect(
                            child: BackdropFilter(
                              filter: ImageFilter.blur(sigmaX: 10, sigmaY: 10),
                              child: Container(
                                decoration: BoxDecoration(
                                  color: config.Colors().primaryColor(0.08),
                                  border: Border.all(
                                    color:
                                        config.Colors().primaryColorDark(0.7),
                                  ),
                                  borderRadius: BorderRadius.circular(10.0),
                                  boxShadow: [
                                    BoxShadow(
                                      color: Colors.transparent,
                                      blurRadius: 3,
                                      offset: Offset(1, 1),
                                    ),
                                  ],
                                ),
                                child: Container(
                                  margin: EdgeInsets.only(
                                      top: 70, right: 15, left: 10),
                                  child: Column(
                                    crossAxisAlignment:
                                        CrossAxisAlignment.start,
                                    children: [
                                      SizedBox(height: 7),
                                      Text(
                                        "دکتر محمد رضا صفری نژاد اقدم",
                                        style: TextStyle(
                                          fontSize: 16.0,
                                          color: config.Colors()
                                              .primaryColorDark(1),
                                          fontWeight: FontWeight.bold,
                                        ),
                                      ),
                                      SizedBox(height: 7),
                                      Row(
                                        children: [
                                          Text(
                                            "نظام پزشکی: ",
                                            style: TextStyle(
                                              fontSize: 14.0,
                                              color:
                                                  config.Colors().brownColor(1),
                                              fontWeight: FontWeight.bold,
                                            ),
                                          ),
                                          Text(
                                            "26739".toPersianDigit(),
                                            style: TextStyle(
                                              fontSize: 14.0,
                                              color: config.Colors()
                                                  .brownColor(0.7),
                                              fontWeight: FontWeight.bold,
                                            ),
                                          ),
                                        ],
                                      ),
                                      SizedBox(height: 7),
                                      Row(
                                        children: [
                                          Text(
                                            "سایت شخصی: ",
                                            style: TextStyle(
                                              fontSize: 14.0,
                                              color:
                                                  config.Colors().brownColor(1),
                                              fontWeight: FontWeight.bold,
                                            ),
                                          ),
                                          Text(
                                            "ebnesinahospital.com",
                                            style: TextStyle(
                                              fontSize: 14.0,
                                              color: config.Colors()
                                                  .brownColor(0.7),
                                              fontWeight: FontWeight.bold,
                                            ),
                                          ),
                                        ],
                                      ),
                                      SizedBox(height: 7),
                                      Row(
                                        children: [
                                          Text(
                                            "تخصص ها: ",
                                            style: TextStyle(
                                              fontSize: 14.0,
                                              color:
                                                  config.Colors().brownColor(1),
                                              fontWeight: FontWeight.bold,
                                            ),
                                          ),
                                          Text(
                                            "متخصص جراحی استخوان و مفاصل",
                                            style: TextStyle(
                                              fontSize: 13.0,
                                              color:
                                                  config.Colors().blueColor(1),
                                              fontWeight: FontWeight.bold,
                                            ),
                                          ),
                                        ],
                                      ),
                                      SizedBox(height: 20),
                                      Row(
                                        mainAxisAlignment:
                                            MainAxisAlignment.center,
                                        children: [
                                          Container(
                                            height: 40,
                                            width: 40,
                                            padding: EdgeInsets.only(right: 2),
                                            decoration: BoxDecoration(
                                              border: Border.all(
                                                color: config.Colors()
                                                    .blackColor(0.2),
                                              ),
                                              shape: BoxShape.circle,
                                            ),
                                            child: Icon(
                                              FontAwesomeIcons.telegramPlane,
                                              color: Color.fromRGBO(
                                                  58, 183, 229, 1),
                                            ),
                                          ),
                                          SizedBox(width: 10),
                                          Container(
                                            height: 40,
                                            width: 40,
                                            decoration: BoxDecoration(
                                              border: Border.all(
                                                color: config.Colors()
                                                    .blackColor(0.2),
                                              ),
                                              shape: BoxShape.circle,
                                            ),
                                            child: Icon(
                                              FontAwesomeIcons.instagram,
                                              color:
                                                  Color.fromRGBO(195, 83, 0, 1),
                                            ),
                                          ),
                                          SizedBox(width: 10),
                                          Container(
                                            height: 40,
                                            width: 40,
                                            decoration: BoxDecoration(
                                              border: Border.all(
                                                color: config.Colors()
                                                    .blackColor(0.2),
                                              ),
                                              shape: BoxShape.circle,
                                            ),
                                            child: Icon(
                                              FontAwesomeIcons.facebookF,
                                              color: Color.fromRGBO(
                                                  75, 109, 170, 1),
                                            ),
                                          ),
                                        ],
                                      )
                                    ],
                                  ),
                                ),
                              ),
                            ),
                          ),
                        ),
                        Container(
                          margin: EdgeInsets.only(top: 127),
                          child: Row(
                            mainAxisAlignment: MainAxisAlignment.center,
                            children: [
                              Container(
                                height: 8,
                                width: 8,
                                decoration: BoxDecoration(
                                  color: config.Colors().primaryColorDark(1),
                                  shape: BoxShape.circle,
                                ),
                              ),
                              Container(
                                color: Colors.white,
                                height: 2,
                                width: 180,
                              ),
                              Container(
                                height: 8,
                                width: 8,
                                decoration: BoxDecoration(
                                  color: config.Colors().primaryColorDark(1),
                                  shape: BoxShape.circle,
                                ),
                              ),
                            ],
                          ),
                        ),
                        Align(
                          alignment: Alignment.topCenter,
                          child: AdvancedAvatar(
                            margin: EdgeInsets.only(top: 70),
                            statusSize: 0,
                            size: 120.0,
                            image: NetworkImage(
                                "https://boghrat.com/api/document/Thumbnail/LoadThumb/6fff549d-a58d-41ba-b7c5-64fb42707e96.png"),
                            decoration: BoxDecoration(shape: BoxShape.circle),
                          ),
                        ),
                      ],
                    ),
                  ),
                  Container(
                    width: MediaQuery.of(context).size.width,
                    margin: EdgeInsets.symmetric(horizontal: 20),
                    padding: EdgeInsets.symmetric(vertical: 20),
                    decoration: BoxDecoration(
                      color: config.Colors().primaryColor(0.08),
                      border: Border.all(
                        color: config.Colors().primaryColorDark(0.7),
                      ),
                      borderRadius: BorderRadius.circular(10.0),
                      boxShadow: [
                        BoxShadow(
                          color: Colors.transparent,
                          blurRadius: 3,
                          offset: Offset(1, 1),
                        ),
                      ],
                    ),
                    child: Column(
                      crossAxisAlignment: CrossAxisAlignment.start,
                      mainAxisAlignment: MainAxisAlignment.start,
                      mainAxisSize: MainAxisSize.min,
                      children: [
                        _buildinfoColumn(context),
                        SizedBox(height: 10),
                        _buildDaysColumn(),
                        SizedBox(height: 10),
                        CarouselWithIndicator(),
                      ],
                    ),
                  ),
                  // Container(
                  //   margin: EdgeInsets.only(left: 20, right: 20),
                  //   child: Row(
                  //     children: [
                  //       Flexible(
                  //         flex: 2,
                  //         child: Container(
                  //           height: 45,
                  //           child: TextButton(
                  //             child: Row(
                  //               mainAxisAlignment: MainAxisAlignment.center,
                  //               children: [
                  //                 Icon(
                  //                   FontAwesomeIcons.infoCircle,
                  //                   color: config.Colors().lightBlueColor(1),
                  //                   size: 20,
                  //                 ),
                  //                 SizedBox(width: 5),
                  //                 Text(
                  //                   "درباره ما",
                  //                   style: TextStyle(
                  //                     fontSize: 14,
                  //                     fontWeight: FontWeight.w300,
                  //                     color: config.Colors().lightBlueColor(1),
                  //                   ),
                  //                 ),
                  //               ],
                  //             ),
                  //             style: ButtonStyle(
                  //               padding: MaterialStateProperty.all<EdgeInsets>(
                  //                 EdgeInsets.all(20),
                  //               ),
                  //               foregroundColor:
                  //                   MaterialStateProperty.all<Color>(Colors.red),
                  //               shape: MaterialStateProperty.all<
                  //                   RoundedRectangleBorder>(
                  //                 RoundedRectangleBorder(
                  //                   borderRadius: BorderRadius.circular(10.0),
                  //                   side: BorderSide(
                  //                     color: config.Colors().lightBlueColor(1),
                  //                   ),
                  //                 ),
                  //               ),
                  //             ),
                  //             onPressed: () {},
                  //           ),
                  //         ),
                  //       ),
                  //       SizedBox(width: 10),
                  //       Flexible(
                  //         flex: 3,
                  //         child: Container(
                  //           height: 45,
                  //           child: TextButton(
                  //             child: Row(
                  //               mainAxisAlignment: MainAxisAlignment.center,
                  //               children: [
                  //                 Icon(
                  //                   FontAwesomeIcons.phone,
                  //                   color: config.Colors().redColor(1),
                  //                   size: 20,
                  //                 ),
                  //                 SizedBox(width: 5),
                  //                 Text(
                  //                   "اطلاعات تماس",
                  //                   style: TextStyle(
                  //                     fontSize: 14,
                  //                     fontWeight: FontWeight.w300,
                  //                     color: config.Colors().redColor(1),
                  //                   ),
                  //                 ),
                  //               ],
                  //             ),
                  //             style: ButtonStyle(
                  //               padding: MaterialStateProperty.all<EdgeInsets>(
                  //                 EdgeInsets.all(20),
                  //               ),
                  //               foregroundColor:
                  //                   MaterialStateProperty.all<Color>(Colors.red),
                  //               shape: MaterialStateProperty.all<
                  //                   RoundedRectangleBorder>(
                  //                 RoundedRectangleBorder(
                  //                   borderRadius: BorderRadius.circular(10.0),
                  //                   side: BorderSide(
                  //                     color: config.Colors().redColor(1),
                  //                   ),
                  //                 ),
                  //               ),
                  //             ),
                  //             onPressed: () {},
                  //           ),
                  //         ),
                  //       ),
                  //     ],
                  //   ),
                  // ),
                  // Container(
                  //   height: 250,
                  //   child: CarouselWithIndicatorDemo(),
                  // ),
                  // Spacer(),
                  // Container(
                  //   margin: EdgeInsets.all(20),
                  //   height: 45,
                  //   child: CustomLittleButton(
                  //     icon: MdiIcons.calendarToday,
                  //     color: config.Colors().greenColor(1),
                  //     text: "دریافت نوبت",
                  //     onPressed: () {},
                  //   ),
                  // )
                  SizedBox(height: 25),
                ],
              ),
            ],
          ),
        ),
        bottomNavigationBar: Container(
          height: 65,
          child: Material(
            color: Colors.white,
            elevation: 10,
            child: Container(
              padding: EdgeInsets.fromLTRB(20, 10, 20, 10),
              child: CustomLittleButton(
                icon: MdiIcons.calendarToday,
                color: config.Colors().greenColor(1),
                text: "دریافت نوبت",
                onPressed: () {
                  BookingBottomSheet().build(context);
                },
              ),
            ),
          ),
        ),
      ),
    );
  }
}

Widget _buildinfoColumn(context) {
  return Container(
    margin: EdgeInsets.symmetric(horizontal: 20),
    child: Column(
      crossAxisAlignment: CrossAxisAlignment.start,
      mainAxisAlignment: MainAxisAlignment.start,
      children: [
        CustomLongTitleRow(
          icon: FontAwesomeIcons.phone,
          text: " تلفن‌ها:",
          secondText: "1518815",
          space: 5,
          textStyle: Theme.of(context).textTheme.headline2!,
        ),
        SizedBox(height: 5),
        CustomLongTitleRow(
          icon: FontAwesomeIcons.mapMarkerAlt,
          text: " آدرس:",
          secondText: "تهران، بیمارستان ابن سینا",
          space: 5,
          textStyle: Theme.of(context).textTheme.headline2!,
        ),
        SizedBox(height: 5),
        CustomLongTitleRow(
          icon: FontAwesomeIcons.solidPlusSquare,
          text: " بیمه‌ها:",
          secondText: "بدون بیمه",
          space: 5,
          textStyle: Theme.of(context).textTheme.headline2!,
        ),
        SizedBox(height: 5),
        CustomLongTitleRow(
          icon: FontAwesomeIcons.solidClock,
          text: " ساعات کاری:",
          secondText: "",
          space: 5,
          textStyle: Theme.of(context).textTheme.headline2!,
        ),
      ],
    ),
  );
}

Widget _buildDaysColumn() {
  return Column(
    children: [
      Row(
        mainAxisAlignment: MainAxisAlignment.spaceBetween,
        children: [
          _buildDaysCard("شنبه"),
          _buildDaysCard("یکشنبه"),
          _buildDaysCard("دوشنبه"),
        ],
      ),
      Row(
        mainAxisAlignment: MainAxisAlignment.spaceBetween,
        children: [
          _buildDaysCard("سه‌شنبه"),
          _buildDaysCard("چهارشنبه"),
          _buildDaysCard("پنجشنبه"),
        ],
      ),
      Row(
        mainAxisAlignment: MainAxisAlignment.center,
        children: [
          _buildDaysCard("جمعه"),
        ],
      ),
    ],
  );
}

Widget _buildDaysCard(day) {
  return Expanded(
    child: Container(
      margin: EdgeInsets.symmetric(horizontal: 5),
      height: 90,
      child: Column(
        mainAxisAlignment: MainAxisAlignment.center,
        children: [
          Text(
            day,
            style: TextStyle(
              fontWeight: FontWeight.bold,
              color: Color.fromRGBO(195, 83, 0, 1),
              fontSize: 18,
            ),
          ),
          SizedBox(height: 10),
          Text(
            "20:00".toPersianDigit() + " - " + "08:00".toPersianDigit(),
            style: TextStyle(
              fontWeight: FontWeight.bold,
              color: Color.fromRGBO(195, 83, 0, 1),
              fontSize: 14,
            ),
          ),
        ],
      ),
    ),
  );
}

class BorderPainter extends CustomPainter {
  @override
  void paint(Canvas canvas, Size size) {
    Paint paint = Paint()
      ..style = PaintingStyle.fill
      ..color = config.Colors().primaryColorLight(0.3);
    Path path = Path();
    path.lineTo(0, 150);
    path.lineTo(size.width, size.height - 100);
    path.lineTo(size.width, 0);
    path.lineTo(0, 0);
    path.close();
    canvas.drawPath(path, paint);
  }

  @override
  bool shouldRepaint(CustomPainter oldDelegate) => true;
}

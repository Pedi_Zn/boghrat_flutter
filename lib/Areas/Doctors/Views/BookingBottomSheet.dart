import 'package:boghrat_flutter/Areas/SearchTab/Controllers/SearchFilterController.dart';
import 'package:boghrat_flutter/Localization/Localization.dart';
import 'package:boghrat_flutter/Widgets/CustomBox.dart';
import 'package:boghrat_flutter/Widgets/CustomVisitTypeRow.dart';
import 'package:dropdown_plus/dropdown_plus.dart';
import 'package:flutter/material.dart';
import 'package:font_awesome_flutter/font_awesome_flutter.dart';
import 'package:get/get.dart';
import 'package:boghrat_flutter/Config/AppConfig.dart' as config;
import 'package:radio_grouped_buttons/custom_buttons/custom_radio_buttons_group.dart';

import 'package:flutter/cupertino.dart';
import 'package:flutter/rendering.dart';
import 'package:persian_datetime_picker/persian_datetime_picker.dart';

class BookingBottomSheet extends StatelessWidget {
  BookingBottomSheet({Key? key}) : super(key: key);

  final searchFilterController = Get.put(SearchFilterController());

  @override
  Widget build(BuildContext context) {
    return _buildBottomSheet(context);
  }
}

_buildBottomSheet(context) {
  return showModalBottomSheet(
    context: context,
    isScrollControlled: true,
    backgroundColor: Colors.transparent,
    shape: RoundedRectangleBorder(
      borderRadius: BorderRadius.all(Radius.circular(10.0)),
    ),
    builder: (context) {
      return Padding(
        padding: MediaQuery.of(context).viewInsets,
        child: StatefulBuilder(
          builder: (context, setState) {
            return Column(
              mainAxisSize: MainAxisSize.min,
              children: [
                Container(
                  margin: EdgeInsets.all(15),
                  decoration: BoxDecoration(
                    color: config.Colors().primaryColorLight(1),
                    borderRadius: BorderRadius.all(Radius.circular(10.0)),
                  ),
                  child: Column(
                    children: <Widget>[
                      _buildBottomSheetHeader(context),
                      Divider(),
                      // SizedBox(height: 40.0),
                      CustomBox(
                        text: "انتخاب مرکز",
                        height: 60,
                        widget: _buildMedicalCenterField(context),
                      ),
                      CustomBox(
                        text: "انتخاب پزشک",
                        height: 60,
                        widget: _buildMedicalCenterField(context),
                      ),
                      CustomBox(
                        text: "انتخاب تخصص",
                        height: 60,
                        widget: _buildMedicalCenterField(context),
                      ),
                      CustomBox(
                        text: "انتخاب نوع ویزیت",
                        height: 125,
                        widget: Center(
                          child: Container(
                            padding: EdgeInsets.all(10),
                            width: MediaQuery.of(context).size.width,
                            height: 100,
                            child: CustomRadioButton(
                              buttonLables: [
                                "تلفنی",
                                "متنی",
                                "تصویری",
                                "حضوری",
                              ],
                              buttonValues: [
                                "Sunday",
                                "Monday",
                                "Tuesday",
                                "Wednesday",
                              ],
                              fontSize: 14,
                              radioButtonValue: (value, index) {
                                print("Button value " + value.toString());
                                print("Integer value " + index.toString());
                              },
                              horizontal: true,
                              enableShape: true,
                              buttonSpace: 5,
                              elevation: 0,
                              initialSelection: 2,
                              textColor: Colors.grey.shade600,
                              buttonColor: Colors.white,
                              unselectedButtonBorderColor: Colors.grey.shade300,
                              selectedColor:
                                  config.Colors().primaryColorDark(1),
                              buttonWidth: 137,
                            ),
                          ),
                        ),
                      ),
                      CustomBox(
                        text: "انتخاب زمان",
                        height: 60,
                        widget: InkWell(
                          child: Center(
                            child: Text(
                              "زمان مدنظر خود را انتخاب کنید",
                              style: TextStyle(
                                  fontSize: 13, color: Colors.grey.shade700),
                            ),
                          ),
                          onTap: () async {
                            Jalali? picked = await showPersianDatePicker(
                              context: context,
                              initialDate: Jalali.now(),
                              firstDate: Jalali(1385, 8),
                              lastDate: Jalali(1450, 9),
                            );
                          },
                        ),
                      ),
                      SizedBox(height: 20.0),
                    ],
                  ),
                ),
                _buildSubmitButton(context),
              ],
            );
          },
        ),
      );
    },
  );
}

_buildBottomSheetHeader(context) {
  return Container(
    padding: EdgeInsets.fromLTRB(20, 10, 20, 5),
    child: Row(
      mainAxisAlignment: MainAxisAlignment.spaceBetween,
      children: [
        Text(
          "دریافت نوبت اینترنتی",
          style: Theme.of(context).textTheme.headline5,
        ),
        IconButton(
          onPressed: () {
            Navigator.pop(context);
          },
          icon: Icon(
            FontAwesomeIcons.times,
            size: 20,
            color: Colors.black26,
          ),
        ),
      ],
    ),
  );
}

_buildMedicalCenterField(context) {
  return DropdownFormField<Map<String, dynamic>>(
    autoFocus: false,
    onEmptyActionPressed: () async {},
    decoration: InputDecoration(
      border: null,
      suffixIcon: Icon(Icons.arrow_drop_down),
      hintText: "مطب محمدعلي سازگاري",
      hintStyle: TextStyle(fontSize: 12),
      disabledBorder: const OutlineInputBorder(
        borderSide: const BorderSide(
          color: Colors.transparent,
        ),
      ),
      enabledBorder: const OutlineInputBorder(
        borderSide: const BorderSide(
          color: Colors.transparent,
        ),
      ),
      focusedBorder: const OutlineInputBorder(
        borderSide: const BorderSide(
          color: Colors.transparent,
        ),
      ),
    ),
    onSaved: (dynamic str) {},
    onChanged: (dynamic str) {},
    validator: (dynamic str) {},
    displayItemFn: (dynamic item) {
      print("2");
      return Text(
        "",
        style: TextStyle(fontSize: 13, color: config.Colors().blackColor(1)),
      );
    },
    findFn: (dynamic str) async {
      return [];
    },
    selectedFn: (dynamic item1, dynamic item2) {
      print("3");
      if (item1 != null && item2 != null) {
        return item1['name'] == item2['name'];
      }
      return false;
    },
    // filterFn: (dynamic item, str) {
    // },
    dropdownItemFn: (dynamic item, int position, bool focused, bool selected,
        Function() onTap) {
      print("5");
      return ListTile(
        title: Text(
          item['name'],
          style: TextStyle(color: Colors.black, fontSize: 14),
        ),
        tileColor: focused ? Color.fromARGB(20, 0, 0, 0) : Colors.transparent,
        onTap: onTap,
      );
    },
  );
}

_buildSubmitButton(context) {
  return Container(
    margin: EdgeInsets.only(left: 15, right: 15, bottom: 15),
    child: MaterialButton(
      onPressed: () {
        // searchFilterController.advanceSearch();
      },
      disabledColor: config.Colors().accentColor(1).withOpacity(0.5),
      color: config.Colors().primaryColorLight(1),
      minWidth: MediaQuery.of(context).size.width,
      shape: RoundedRectangleBorder(
        borderRadius: BorderRadius.circular(10.0),
      ),
      child:
          // (searchFilterController.isLoading.value)
          //     ? CircularProgressIndicator(
          //         strokeWidth: 2,
          //         color: config.Colors().primaryColorDark(1),
          //       )
          //     :
          Text(DemoLocalizations.of(context)!.trans("Submit"),
              style: Theme.of(context).textTheme.headline3),
      height: 55.0,
    ),
  );
}

import 'package:boghrat_flutter/Areas/SearchTab/Controllers/SearchController.dart';
import 'package:boghrat_flutter/Widgets/CustomLittleButton.dart';
import 'package:boghrat_flutter/Widgets/CustomSearch.dart';
import 'package:flutter/material.dart';
import 'package:flutter_advanced_avatar/flutter_advanced_avatar.dart';
import 'package:flutter_staggered_animations/flutter_staggered_animations.dart';
import 'package:get/get.dart';
import 'package:material_design_icons_flutter/material_design_icons_flutter.dart';
import 'package:boghrat_flutter/Config/AppConfig.dart' as config;

class SearchScreen extends StatelessWidget {
  SearchScreen({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    final searchController = Get.put(SearchController());

    return Scaffold(
      extendBody: true,
      backgroundColor: config.Colors().primaryColorLight(1),
      resizeToAvoidBottomInset: false,
      body: Obx(() {
        return Stack(
          fit: StackFit.expand,
          children: [
            NotificationListener<ScrollNotification>(
              onNotification: (ScrollNotification scrollInfo) {
                if (scrollInfo.metrics.pixels >= 100) {
                  searchController.hideHeader(true);
                } else {
                  searchController.hideHeader(false);
                }
                return false;
              },
              child: Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                children: [
                  _buildTitle(searchController),
                  _buildSearchList(searchController),
                  _buildPaginateLoader(searchController),
                ],
              ),
            ),
            CustomSearch(),
          ],
        );
      }),
    );
  }
}

Widget _buildPaginateLoader(searchController) {
  return Container(
    child: searchController.paginateLoading.value
        ? Container(
            margin: EdgeInsets.only(bottom: 10, top: 7),
            child: Center(
              child: CircularProgressIndicator(),
            ),
          )
        : Container(),
  );
}

Widget _buildTitle(searchController) {
  return AnimatedContainer(
    height: !searchController.hideHeader.value ? 140.0 : 65.0,
    duration: Duration(milliseconds: 200),
    child: Container(
      padding: EdgeInsets.fromLTRB(20, 90, 20, 20),
      child: Text(
        "جستجوی بهترین پزشکان",
        style: TextStyle(fontWeight: FontWeight.bold),
      ),
    ),
  );
}

Widget _buildSearchList(searchController) {
  return Obx(() {
    return Expanded(
      child: NotificationListener<ScrollNotification>(
        child: AnimationLimiter(
          child: !searchController.isLoading.value
              ? ListView.builder(
                  addAutomaticKeepAlives: false,
                  itemCount: searchController.doctorsList.length,
                  padding: EdgeInsets.fromLTRB(15, 0, 15, 50),
                  itemBuilder: (BuildContext context, int index) {
                    return AnimationConfiguration.staggeredList(
                      position: index,
                      duration: const Duration(milliseconds: 375),
                      child: SlideAnimation(
                        child: FadeInAnimation(
                          child: _buildSearchItems(
                            context,
                            searchController,
                            searchController.doctorsList[index],
                          ),
                        ),
                      ),
                    );
                  },
                )
              : _buildLoader(),
        ),
        onNotification: (ScrollNotification scrollInfo) {
          if (!searchController.isLoading.value &&
              scrollInfo.metrics.pixels == scrollInfo.metrics.maxScrollExtent &&
              searchController.oneTime.value) {
            searchController.paginateRequest();
          }
          return false;
        },
      ),
    );
  });
}

Widget _buildLoader() {
  return Center(
    child: CircularProgressIndicator(),
  );
}

Widget _buildSearchItems(context, searchController, doctorInfos) {
  return Container(
    child: Stack(
      children: [
        _buildCardBackground(searchController, doctorInfos),
        Row(
          children: [
            _buildCardAvatar(searchController, doctorInfos),
            SizedBox(width: 10),
            _buildCardTitle(searchController, doctorInfos),
          ],
        ),
        _buildCardDetails(context, searchController, doctorInfos),
      ],
    ),
  );
}

Widget _buildCardBackground(searchController, doctorInfos) {
  return Container(
    margin: EdgeInsets.only(bottom: 10, top: 5),
    height: searchController.checkCardSize(doctorInfos),
    decoration: BoxDecoration(
      borderRadius: BorderRadius.circular(12.0),
      gradient: LinearGradient(
        begin: Alignment.centerLeft,
        end: Alignment.centerRight,
        colors: [
          config.Colors().primaryColorLight(1),
          config.Colors().primaryColorLight(1)
        ],
      ),
      boxShadow: [
        BoxShadow(
          color: config.Colors().primaryColor(1),
          blurRadius: 0,
          offset: Offset(1, 1),
        ),
      ],
    ),
  );
}

Widget _buildCardAvatar(searchController, doctorInfos) {
  return AdvancedAvatar(
    statusSize: 0,
    size: 90.0,
    image: NetworkImage(searchController.checkDoctorsImage(doctorInfos)),
    decoration: BoxDecoration(shape: BoxShape.circle),
  );
}

Widget _buildCardTitle(searchController, doctorInfos) {
  return Column(
    crossAxisAlignment: CrossAxisAlignment.start,
    children: [
      Text(
        "دکتر " + doctorInfos.name,
        style: TextStyle(fontSize: 14, fontWeight: FontWeight.bold),
      ),
      SizedBox(height: 5),
      Row(
        children: [
          Text(
            "شماره نظام پزشکی: ",
            style: TextStyle(
              fontSize: 13,
              color: config.Colors().brownColor(1),
            ),
          ),
          Text(
            searchController.doctorCodeStyle(doctorInfos),
            style: TextStyle(
              fontSize: 13,
              color: config.Colors().brownColor(0.7),
            ),
          ),
        ],
      ),
      SizedBox(height: 5),
    ],
  );
}

Widget _buildCardDetails(context, searchController, doctorInfos) {
  return Positioned(
    bottom: 20,
    left: 5,
    right: 5,
    child: Column(
      crossAxisAlignment: CrossAxisAlignment.start,
      children: [
        _buildCardVisitType(context, searchController, doctorInfos),
        SizedBox(height: 10),
        _buildCardSpecialities(searchController, doctorInfos),
        SizedBox(height: 10),
        _buildCardButtons(searchController, doctorInfos),
      ],
    ),
  );
}

Widget _buildCardVisitType(context, searchController, doctorInfos) {
  return Wrap(
    spacing: 5,
    children: searchController.checkVisitType(context, doctorInfos),
  );
}

Widget _buildCardSpecialities(searchController, doctorInfos) {
  return RichText(
    text: new TextSpan(
      style: new TextStyle(fontFamily: 'IranSans'),
      children: <TextSpan>[
        new TextSpan(
          text: "تخصص ها: ",
          style: TextStyle(
            fontSize: 13,
            fontWeight: FontWeight.bold,
            color: config.Colors().brownColor(1),
          ),
        ),
        new TextSpan(
          text: searchController.checkSpecialtiesText(doctorInfos),
          style: TextStyle(
            color: config.Colors().blueColor(1),
            fontSize: 12,
            fontWeight: FontWeight.bold,
          ),
        ),
      ],
    ),
  );
}

Widget _buildCardButtons(searchController, doctorInfos) {
  final searchController = Get.put(SearchController());
  return Row(
    children: [
      Expanded(
        flex: 1,
        child: CustomLittleButton(
          icon: MdiIcons.video,
          color: config.Colors().cyanColor(1),
          text: "مشاوره آنلاین",
          onPressed: () {},
          disabled: searchController.checkButton(doctorInfos),
        ),
      ),
      SizedBox(width: 5),
      Expanded(
        flex: 1,
        child: CustomLittleButton(
          icon: MdiIcons.calendarToday,
          color: config.Colors().greenColor(1),
          text: "اولین نوبت",
          onPressed: () {},
        ),
      ),
    ],
  );
}

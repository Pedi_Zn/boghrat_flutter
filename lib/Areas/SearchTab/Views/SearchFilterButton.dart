import 'package:boghrat_flutter/Areas/SearchTab/Controllers/SearchFilterController.dart';
import 'package:boghrat_flutter/Localization/Localization.dart';
import 'package:boghrat_flutter/Widgets/CustomBox.dart';
import 'package:boghrat_flutter/Widgets/CustomVisitTypeRow.dart';
import 'package:dropdown_plus/dropdown_plus.dart';
import 'package:flutter/material.dart';
import 'package:font_awesome_flutter/font_awesome_flutter.dart';
import 'package:get/get.dart';
import 'package:material_floating_search_bar/material_floating_search_bar.dart';
import 'package:boghrat_flutter/Config/AppConfig.dart' as config;

class SearchFilterButton extends StatelessWidget {
  SearchFilterButton({Key? key}) : super(key: key);

  final searchFilterController = Get.put(SearchFilterController());

  @override
  Widget build(BuildContext context) {
    return FloatingSearchBarAction(
      showIfOpened: true,
      child: CircularButton(
        icon: const Icon(FontAwesomeIcons.slidersH),
        onPressed: () {
          _buildBottomSheet(context, searchFilterController);
        },
      ),
    );
  }
}

_buildBottomSheet(context, searchFilterController) {
  return showModalBottomSheet(
    context: context,
    isScrollControlled: true,
    backgroundColor: Colors.transparent,
    shape: RoundedRectangleBorder(
      borderRadius: BorderRadius.all(Radius.circular(10.0)),
    ),
    builder: (context) {
      return Padding(
        padding: MediaQuery.of(context).viewInsets,
        child: StatefulBuilder(
          builder: (context, setState) {
            return Column(
              mainAxisSize: MainAxisSize.min,
              children: [
                Container(
                  margin: EdgeInsets.all(15),
                  decoration: BoxDecoration(
                    color: config.Colors().primaryColorLight(1),
                    borderRadius: BorderRadius.all(Radius.circular(10.0)),
                  ),
                  child: Column(
                    children: <Widget>[
                      _buildBottomSheetHeader(context),
                      Divider(),
                      CustomBox(
                        text: "انتخاب شهر",
                        height: 60,
                        widget: _buildCityTextField(
                            context, searchFilterController),
                      ),
                      CustomBox(
                        text: "نحوه ویزیت",
                        height: 100,
                        widget:
                            _buildVisitType(context, searchFilterController),
                      ),
                      SizedBox(
                        height: 40.0,
                      )
                    ],
                  ),
                ),
                _buildSubmitButton(context, searchFilterController),
              ],
            );
          },
        ),
      );
    },
  );
}

_buildBottomSheetHeader(context) {
  return Container(
    padding: EdgeInsets.fromLTRB(20, 10, 20, 5),
    child: Row(
      mainAxisAlignment: MainAxisAlignment.spaceBetween,
      children: [
        Text(
          "فیلتر پیشرفته",
          style: Theme.of(context).textTheme.headline5,
        ),
        IconButton(
          onPressed: () {
            Navigator.pop(context);
          },
          icon: Icon(
            FontAwesomeIcons.times,
            size: 20,
            color: Colors.black26,
          ),
        ),
      ],
    ),
  );
}

_buildVisitType(context, controller) {
  return Column(
    mainAxisAlignment: MainAxisAlignment.center,
    children: [
      Wrap(
        direction: Axis.horizontal,
        children: [
          CustomVisitTypeRow(text: "مشاوره تلفنی", value: controller.tell),
          CustomVisitTypeRow(text: "نوبت حضوری", value: controller.inplace),
          CustomVisitTypeRow(text: "گفتگوی متنی", value: controller.ticketing),
          CustomVisitTypeRow(text: "مشاوره تصویری", value: controller.video)
        ],
      ),
    ],
  );
}

_buildCityTextField(context, searchFilterController) {
  return Obx(() {
    return DropdownFormField<Map<String, dynamic>>(
      autoFocus: false,
      onEmptyActionPressed: () async {},
      decoration: InputDecoration(
        border: null,
        suffixIcon: Icon(Icons.arrow_drop_down),
        hintText: !searchFilterController.hideHint.value &&
                searchFilterController.city.value == ""
            ? "شهر مدنظر خود را انتخاب کنید"
            : "",
        hintStyle: TextStyle(fontSize: 12),
        disabledBorder: const OutlineInputBorder(
          borderSide: const BorderSide(
            color: Colors.transparent,
          ),
        ),
        enabledBorder: const OutlineInputBorder(
          borderSide: const BorderSide(
            color: Colors.transparent,
          ),
        ),
        focusedBorder: const OutlineInputBorder(
          borderSide: const BorderSide(
            color: Colors.transparent,
          ),
        ),
      ),
      onSaved: (dynamic str) {},
      onChanged: (dynamic str) {
        searchFilterController.city(str["name"]);
      },
      validator: (dynamic str) {},
      displayItemFn: (dynamic item) {
        print("2");
        return Text(
          searchFilterController.city.value != ""
              ? searchFilterController.city.value
              : "",
          // (item ?? {})['name'] ?? '',
          style: TextStyle(fontSize: 13, color: config.Colors().blackColor(1)),
        );
      },
      findFn: (dynamic str) async {
        return searchFilterController.citiesList;
      },
      selectedFn: (dynamic item1, dynamic item2) {
        print("3");
        if (item1 != null && item2 != null) {
          return item1['name'] == item2['name'];
        }
        return false;
      },
      filterFn: (dynamic item, str) {
        print("4");
        if (str == "" || str.length < 1) {
          searchFilterController.hideHint(false);
        } else {
          searchFilterController.hideHint(true);
        }
        return item['name'].toLowerCase().indexOf(str.toLowerCase()) >= 0;
      },
      dropdownItemFn: (dynamic item, int position, bool focused, bool selected,
          Function() onTap) {
        print("5");
        return ListTile(
          title: Text(
            item['name'],
            style: TextStyle(color: Colors.black, fontSize: 14),
          ),
          tileColor: focused ? Color.fromARGB(20, 0, 0, 0) : Colors.transparent,
          onTap: onTap,
        );
      },
    );
  });
}

_buildSubmitButton(context, searchFilterController) {
  return Obx(() {
    return Container(
      margin: EdgeInsets.only(left: 15, right: 15, bottom: 15),
      child: MaterialButton(
        onPressed: () {
          searchFilterController.advanceSearch();
        },
        disabledColor: config.Colors().accentColor(1).withOpacity(0.5),
        color: config.Colors().primaryColorLight(1),
        minWidth: MediaQuery.of(context).size.width,
        shape: RoundedRectangleBorder(
          borderRadius: BorderRadius.circular(10.0),
        ),
        child: (searchFilterController.isLoading.value)
            ? CircularProgressIndicator(
                strokeWidth: 2,
                color: config.Colors().primaryColorDark(1),
              )
            : Text(DemoLocalizations.of(context)!.trans("Submit"),
                style: Theme.of(context).textTheme.headline3),
        height: 55.0,
      ),
    );
  });
}

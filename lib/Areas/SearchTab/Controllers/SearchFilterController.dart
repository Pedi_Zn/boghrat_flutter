import 'package:boghrat_flutter/Areas/SearchTab/Controllers/SearchController.dart';
import 'package:boghrat_flutter/Areas/SearchTab/Models/SearchModel.dart';
import 'package:boghrat_flutter/Config/BaseController.dart';
import 'package:boghrat_flutter/Services/SearchTab/SearchFilterService.dart';
import 'package:material_floating_search_bar/material_floating_search_bar.dart';

class SearchFilterController extends BaseController {
  SearchFilterService searchFilterService = new SearchFilterService();

  late SearchDS searchDS = new SearchDS();
  final searchController = Get.put(SearchController());

  List<dynamic> cities = [];
  List<Map<String, String>> citiesList = [];

  var inplace = false.obs;
  var ticketing = false.obs;
  var tell = false.obs;
  var video = false.obs;

  var isLoadingg = false.obs;
  var searchQuery = FloatingSearchBarController().obs;
  var searchResult = [].obs;
  var doctorsList = [].obs;
  var expertisesList = [].obs;

  var list = [].obs;

  var hideHint = false.obs;
  var city = "".obs;

  @override
  void onInit() {
    searchQuery.value.query = "";
    getProvinces();
    super.onInit();
  }

  @override
  void onClose() {
    city.value = "";
    super.onClose();
  }

  void getModel() {
    searchDS.setPageNumber = "1";
    searchDS.setVisitTypes = checkVisitType();
    searchDS.setCity = city.value;
    searchDS.setQuery = searchQuery.value.query;
  }

  List<int> checkVisitType() {
    List<int> visitType = [];
    if (inplace.value == true) {
      visitType.add(1);
    }
    if (ticketing.value == true) {
      visitType.add(2);
    }
    if (tell.value == true) {
      visitType.add(3);
    }
    if (video.value == true) {
      visitType.add(4);
    }
    return visitType;
  }

  Future getProvinces() async {
    getModel();
    isLoading(true);
    try {
      var response = await searchFilterService.getProvinces();
      if (response.flag) {
        cities.addAll(response.result);
        for (var i = 0; i < response.result.length; i++) {
          citiesList.add({"name": cities[i].title});
        }
        // print(citiesList);
      } else {
        //
      }
    } finally {
      isLoading(false);
    }
  }

  void fillProvincesList(provincesList) {
    for (var i = 0; i < provincesList.length; i++) {
      citiesList.add(cities[i].title);
    }
    print(citiesList);
  }

  void fillSearchResult() {
    list.value = [
      {
        "تخصص‌ها :": [],
        "اسم‌ها :": [],
      }
    ];
    expertisesList.clear();
    doctorsList.clear();

    getExpertisesWithQuery();
    getDoctorsWithQuert();
  }

  Future advanceSearch() async {
    getModel();
    isLoading(true);
    searchController.isLoading(true);
    try {
      Get.back();
      var response = await searchFilterService.advanceSearch(searchDS);
      if (response.flag) {
        searchController.doctorsList.clear();
        searchController.doctorsList.addAll(response.result.items);
      } else {}
    } finally {
      searchController.isLoading(false);
      isLoading(false);
    }
  }

  Future getExpertisesWithQuery() async {
    // isLoading(true);
    try {
      var response = await searchFilterService
          .getExpertisesWithQuery(searchQuery.value.query);
      if (response.result.length > 0) {
        print(response.flag.toString());
        for (var i = 0;
            i < (response.result.length > 3 ? 4 : response.result.length);
            i++) {
          expertisesList.add(response.result[i].title);
        }
        list[0]["تخصص‌ها :"].addAll(expertisesList.toList());
      } else {
        list[0]["تخصص‌ها :"].addAll(["تخصصی وجود ندارد"]);
      }
    } finally {
      // isLoading(false);
      print(isLoading.value.toString() + "یشسیشیشس");
    }
  }

  Future getDoctorsWithQuert() async {
    isLoading(true);
    try {
      var response = await searchFilterService
          .getDoctorsWithQuert(searchQuery.value.query);
      if (response.flag) {
        for (var i = 0; i < 4; i++) {
          doctorsList.add(response.result.items[i].name);
        }
        print(doctorsList.toString());
        list[0]["اسم‌ها :"].addAll(doctorsList.toList());
      } else {
        list[0]["اسم‌ها :"].addAll("اسمی وجود ندارد");
      }
    } finally {
      isLoading(false);
    }
  }
}

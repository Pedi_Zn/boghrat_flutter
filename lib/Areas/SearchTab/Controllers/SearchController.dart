import 'package:boghrat_flutter/Areas/SearchTab/Models/SearchModel.dart';
import 'package:boghrat_flutter/Config/BaseController.dart';
import 'package:boghrat_flutter/Services/SearchTab/SearchService.dart';
import 'package:boghrat_flutter/Widgets/CustomTitleRow.dart';
import 'package:flutter/material.dart';

class SearchController extends BaseController {
  SearchService searchService = new SearchService();
  List<Doctor> doctorsList = [];

  late SearchDS searchDS = new SearchDS();

  var paginateLoading = false.obs;
  var oneTime = true.obs;
  var hideHeader = false.obs;
  int pageIndex = 1;

  @override
  void onInit() {
    getDoctors();
    super.onInit();
  }

  @override
  void onClose() {
    super.onClose();
  }

  void getModel() {
    searchDS.setPageNumber = pageIndex.toString();
  }

  void checkDoctorsImage(doctorInfos) {
    if (searchService.checkDoctorsImage(doctorInfos).flag) {
      return searchService.checkDoctorsImage(doctorInfos).result;
    } else {
      // Log
      print(searchService.checkDoctorsImage(doctorInfos).errors.toString());
    }
  }

  String checkCityText(doctorInfos) {
    if (searchService.checkCityText(doctorInfos).flag) {
      return searchService.checkCityText(doctorInfos).result;
    } else {
      // Log
      print(searchService.checkCityText(doctorInfos).errors.toString());
      return "-";
    }
  }

  String checkSpecialtiesText(doctorInfos) {
    if (searchService.checkSpecialtiesText(doctorInfos).flag) {
      return searchService.checkSpecialtiesText(doctorInfos).result;
    } else {
      // Log
      print(searchService.checkSpecialtiesText(doctorInfos).errors.toString());
      return "تخصصی ثبت نشده";
    }
  }

  bool checkButton(doctorInfos) {
    if (searchService.checkButton(doctorInfos).flag) {
      return searchService.checkButton(doctorInfos).result;
    } else {
      // Log
      print(searchService.checkButton(doctorInfos).errors.toString());
      return false;
    }
  }

  double checkCardSize(doctorInfos) {
    double cardHeight = 205.0;

    if (searchService.checkCity(doctorInfos).flag) {
      final TextPainter visitTypesPainter = TextPainter(
        text: TextSpan(text: checkCityText(doctorInfos)),
        maxLines: 1,
        textDirection: TextDirection.ltr,
      )..layout(minWidth: 0, maxWidth: double.infinity);

      if (doctorInfos.visitTypes.length == 4 &&
          visitTypesPainter.size.width > 51.0) {
        cardHeight += (visitTypesPainter.size.width / 51.0).floor() * 20.0;
      }
    } else {
      // Log
      print(searchService.checkCity(doctorInfos).errors.toString());
    }

    if (searchService.checkSpecialties(doctorInfos).flag) {
      final TextPainter specialitiesTextPainter = TextPainter(
        text: TextSpan(text: checkSpecialtiesText(doctorInfos)),
        maxLines: 1,
        textDirection: TextDirection.ltr,
      )..layout(minWidth: 0, maxWidth: double.infinity);

      if (specialitiesTextPainter.size.width > 233.0) {
        cardHeight +=
            (specialitiesTextPainter.size.width / 233.0).floor() * 20.0;
      }
    } else {
      // Log
      print(searchService.checkSpecialties(doctorInfos).errors.toString());
    }

    return cardHeight;
  }

  List<Widget> checkVisitType(context, doctorInfos) {
    List<Widget>? visitTypeWidgets = [];

    if (searchService.checkVisitType(doctorInfos).flag) {
      if (doctorInfos.visitTypes.contains(1)) {
        visitTypeWidgets.add(
          CustomTitleRow(
            icon: Icons.location_on,
            text: "حضوری در ${checkCityText(doctorInfos)}",
            space: 3,
            textStyle: Theme.of(context).textTheme.bodyText1!,
          ),
        );
      }
      if (doctorInfos.visitTypes.contains(2)) {
        visitTypeWidgets.add(
          CustomTitleRow(
            icon: Icons.chat,
            text: "متنی",
            space: 3,
            textStyle: Theme.of(context).textTheme.bodyText1!,
          ),
        );
      }
      if (doctorInfos.visitTypes.contains(3)) {
        visitTypeWidgets.add(
          CustomTitleRow(
            icon: Icons.perm_phone_msg,
            text: "تلفنی",
            space: 3,
            textStyle: Theme.of(context).textTheme.bodyText1!,
          ),
        );
      }
      if (doctorInfos.visitTypes.contains(4)) {
        visitTypeWidgets.add(
          CustomTitleRow(
            icon: Icons.voice_chat,
            text: "تصویری",
            space: 3,
            textStyle: Theme.of(context).textTheme.bodyText1!,
          ),
        );
      }
    } else {
      // Log
      print(searchService.checkVisitType(doctorInfos).errors.toString());
    }

    return visitTypeWidgets;
  }

  String doctorCodeStyle(doctorInfos) {
    if (searchService.doctorCodeStyle(doctorInfos).flag) {
      return searchService.doctorCodeStyle(doctorInfos).result;
    } else {
      // Log
      print(searchService.doctorCodeStyle(doctorInfos).errors.toString());
      return "-";
    }
  }

  Future getDoctors() async {
    getModel();
    isLoading(true);
    try {
      var response = await searchService.getDoctors(searchDS);
      if (response.flag) {
        doctorsList.addAll(response.result.items);
      } else {
        // Tools().showDialog(context, response.errors);
      }
    } finally {
      isLoading(false);
    }
  }

  Future paginateRequest() async {
    pageIndex++;
    getModel();
    paginateLoading(true);
    oneTime(false);
    try {
      var response = await searchService.getDoctors(searchDS);
      if (response.flag) {
        doctorsList.addAll(response.result.items);
      } else {
        print("objecttttttttt");
      }
    } finally {
      oneTime(true);
      paginateLoading(false);
    }
  }
}

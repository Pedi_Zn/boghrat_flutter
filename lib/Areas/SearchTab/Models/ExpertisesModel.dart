import 'dart:convert';

List<ExpertisesModel> expertisesModelFromJson(String str) =>
    List<ExpertisesModel>.from(
        json.decode(str).map((x) => ExpertisesModel.fromJson(x)));

String expertisesModelToJson(List<ExpertisesModel> data) =>
    json.encode(List<dynamic>.from(data.map((x) => x.toJson())));

class ExpertisesModel {
  ExpertisesModel({
    required this.title,
    required this.isExpertise,
    required this.id,
  });

  String? title;
  bool? isExpertise;
  int? id;

  factory ExpertisesModel.fromJson(Map<String, dynamic> json) =>
      ExpertisesModel(
        title: json["title"] == null ? null : json["title"],
        isExpertise: json["isExpertise"] == null ? null : json["isExpertise"],
        id: json["id"] == null ? null : json["id"],
      );

  Map<String, dynamic> toJson() => {
        "title": title == null ? null : title,
        "isExpertise": isExpertise == null ? null : isExpertise,
        "id": id == null ? null : id,
      };
}

import 'package:meta/meta.dart';
import 'dart:convert';

SearchModel searchModelFromJson(String? str) {
  if (str == null) {
    return SearchModel.fromJson(json.decode("{}"));
  }
  return SearchModel.fromJson(json.decode(str));
}

String searchModelToJson(SearchModel data) => json.encode(data.toJson());

class SearchModel {
  SearchModel({
    required this.total,
    required this.items,
  });

  int? total;
  List<Doctor>? items;

  factory SearchModel.fromJson(Map<String, dynamic> json) => SearchModel(
        total: json["total"] == null ? null : json["total"],
        items: json["items"] == null
            ? null
            : List<Doctor>.from(json["items"].map((x) => Doctor.fromJson(x))),
      );

  Map<String, dynamic> toJson() => {
        "total": total == null ? null : total,
        "items": items == null
            ? null
            : List<dynamic>.from(items!.map((x) => x.toJson())),
      };
}

class Doctor {
  Doctor({
    required this.id,
    required this.name,
    required this.doctorCode,
    required this.clinicLegalId,
    required this.isBoghrat,
    required this.pageAddress,
    required this.img,
    required this.gender,
    required this.order,
    required this.doctorId,
    required this.visitTypes,
    required this.cities,
    required this.expertises,
    required this.specialties,
  });

  int? id;
  String? name;
  String? doctorCode;
  dynamic clinicLegalId;
  bool? isBoghrat;
  String? pageAddress;
  String? img;
  int? gender;
  int? order;
  String? doctorId;
  List<int>? visitTypes;
  List<City>? cities;
  List<Expertise>? expertises;
  List<Expertise>? specialties;

  factory Doctor.fromJson(Map<String, dynamic> json) => Doctor(
        id: json["id"] == null ? null : json["id"],
        name: json["name"] == null ? null : json["name"],
        doctorCode: json["doctorCode"] == null ? null : json["doctorCode"],
        clinicLegalId: json["clinicLegalId"],
        isBoghrat: json["isBoghrat"] == null ? null : json["isBoghrat"],
        pageAddress: json["pageAddress"] == null ? null : json["pageAddress"],
        img: json["img"] == null ? null : json["img"],
        gender: json["gender"] == null ? null : json["gender"],
        order: json["order"] == null ? null : json["order"],
        doctorId: json["doctorId"] == null ? null : json["doctorId"],
        visitTypes: json["visitTypes"] == null
            ? null
            : List<int>.from(json["visitTypes"].map((x) => x)),
        cities: json["cities"] == null
            ? null
            : List<City>.from(json["cities"].map((x) => City.fromJson(x))),
        expertises: json["expertises"] == null
            ? null
            : List<Expertise>.from(
                json["expertises"].map((x) => Expertise.fromJson(x))),
        specialties: json["specialties"] == null
            ? null
            : List<Expertise>.from(
                json["specialties"].map((x) => Expertise.fromJson(x))),
      );

  Map<String, dynamic> toJson() => {
        "id": id == null ? null : id,
        "name": name == null ? null : name,
        "doctorCode": doctorCode == null ? null : doctorCode,
        "clinicLegalId": clinicLegalId,
        "isBoghrat": isBoghrat == null ? null : isBoghrat,
        "pageAddress": pageAddress == null ? null : pageAddress,
        "img": img == null ? null : img,
        "gender": gender == null ? null : gender,
        "order": order == null ? null : order,
        "doctorId": doctorId == null ? null : doctorId,
        "visitTypes": visitTypes == null
            ? null
            : List<dynamic>.from(visitTypes!.map((x) => x)),
        "cities": cities == null
            ? null
            : List<dynamic>.from(cities!.map((x) => x.toJson())),
        "expertises": expertises == null
            ? null
            : List<dynamic>.from(expertises!.map((x) => x.toJson())),
        "specialties": specialties == null
            ? null
            : List<dynamic>.from(specialties!.map((x) => x.toJson())),
      };
}

class City {
  City({
    required this.provinceId,
    required this.title,
    required this.latinTitle,
    required this.lat,
    required this.lng,
    required this.tellCode,
    required this.latinDashedTitle,
    required this.id,
  });

  int? provinceId;
  String? title;
  dynamic latinTitle;
  double? lat;
  double? lng;
  dynamic tellCode;
  dynamic latinDashedTitle;
  int? id;

  factory City.fromJson(Map<String, dynamic> json) => City(
        provinceId: json["provinceId"] == null ? null : json["provinceId"],
        title: json["title"] == null ? null : json["title"],
        latinTitle: json["latinTitle"],
        lat: json["lat"] == null ? null : json["lat"],
        lng: json["lng"] == null ? null : json["lng"],
        tellCode: json["tellCode"],
        latinDashedTitle: json["latinDashedTitle"],
        id: json["id"] == null ? null : json["id"],
      );

  Map<String, dynamic> toJson() => {
        "provinceId": provinceId == null ? null : provinceId,
        "title": title == null ? null : title,
        "latinTitle": latinTitle,
        "lat": lat == null ? null : lat,
        "lng": lng == null ? null : lng,
        "tellCode": tellCode,
        "latinDashedTitle": latinDashedTitle,
        "id": id == null ? null : id,
      };
}

class Expertise {
  Expertise({
    required this.key,
    required this.value,
  });

  String key;
  String value;

  factory Expertise.fromJson(Map<String, dynamic> json) => Expertise(
        key: json["key"] == null ? null : json["key"],
        value: json["value"] == null ? null : json["value"],
      );

  Map<String, dynamic> toJson() => {
        "key": key == null ? null : key,
        "value": value == null ? null : value,
      };
}

class SearchDS {
  SearchDS({pageNumber, visitTypes, city});

  String? pageNumber;
  List<int>? visitTypes;
  String? city;
  String? query;

  set setPageNumber(String number) {
    pageNumber = number;
  }

  String? get getPageNumber {
    return pageNumber;
  }

  set setVisitTypes(List<int> type) {
    visitTypes = type;
  }

  List<int>? get getVisitTypes {
    return visitTypes;
  }

  set setCity(String name) {
    city = name;
  }

  String? get getCity {
    return city;
  }

  set setQuery(String text) {
    query = text;
  }

  String? get getQuery {
    return query;
  }

  Map<String, dynamic> toJson() => {
        "pageNumber": pageNumber == null ? null : pageNumber,
        "visitTypes": visitTypes == null ? [] : visitTypes,
        "city": city == null ? "" : city,
        "name": query == null ? "" : query,
      };
}

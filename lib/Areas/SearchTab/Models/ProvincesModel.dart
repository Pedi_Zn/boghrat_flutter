import 'dart:convert';

List<ProvincesModel> provincesModelFromJson(String str) =>
    List<ProvincesModel>.from(
        json.decode(str).map((x) => ProvincesModel.fromJson(x)));

String provincesModelToJson(List<ProvincesModel> data) =>
    json.encode(List<dynamic>.from(data.map((x) => x.toJson())));

class ProvincesModel {
  ProvincesModel({
    required this.provinceId,
    required this.title,
    required this.latinTitle,
    required this.lat,
    required this.lng,
    required this.tellCode,
    required this.latinDashedTitle,
    required this.id,
  });

  dynamic provinceId;
  String? title;
  String? latinTitle;
  double? lat;
  double? lng;
  String? tellCode;
  String? latinDashedTitle;
  int? id;

  factory ProvincesModel.fromJson(Map<String, dynamic> json) => ProvincesModel(
        provinceId: json["provinceId"],
        title: json["title"] == null ? null : json["title"],
        latinTitle: json["latinTitle"] == null ? null : json["latinTitle"],
        lat: json["lat"] == null ? null : json["lat"].toDouble(),
        lng: json["lng"] == null ? null : json["lng"].toDouble(),
        tellCode: json["tellCode"] == null ? null : json["tellCode"],
        latinDashedTitle:
            json["latinDashedTitle"] == null ? null : json["latinDashedTitle"],
        id: json["id"] == null ? null : json["id"],
      );

  Map<String, dynamic> toJson() => {
        "provinceId": provinceId,
        "title": title == null ? null : title,
        "latinTitle": latinTitle == null ? null : latinTitle,
        "lat": lat == null ? null : lat,
        "lng": lng == null ? null : lng,
        "tellCode": tellCode == null ? null : tellCode,
        "latinDashedTitle": latinDashedTitle == null ? null : latinDashedTitle,
        "id": id == null ? null : id,
      };
}

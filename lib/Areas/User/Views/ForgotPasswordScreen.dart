import 'package:boghrat_flutter/Areas/User/Controllers/ForgotPasswordController.dart';
import 'package:boghrat_flutter/Widgets/BoghratBackground.dart';
import 'package:boghrat_flutter/Widgets/BoghtatLogo.dart';
import 'package:boghrat_flutter/Widgets/CustomOtpCodeForm.dart';
import 'package:boghrat_flutter/Widgets/CustomFormField.dart';
import 'package:boghrat_flutter/Localization/Localization.dart';
import 'package:boghrat_flutter/Widgets/CustomBackButton.dart';
import 'package:boghrat_flutter/Widgets/CustomTitle.dart';
import 'package:flutter/material.dart';
import 'package:font_awesome_flutter/font_awesome_flutter.dart';
import 'package:get/get.dart';
import 'package:boghrat_flutter/Config/AppConfig.dart' as config;

class ForgotPasswordScreen extends StatelessWidget {
  final String phoneNumber;
  ForgotPasswordScreen({Key? key, required this.phoneNumber}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    final forgotPasswordController =
        Get.put(ForgotPasswordController(phoneNumber));

    return Scaffold(
      backgroundColor: config.Colors().primaryColor(1),
      body: Stack(
        children: [
          BoghratBackground(
            children: [
              BoghratLogo(),
              _buildTitle(context),
              Spacer(),
              _buildCodeForm(context, forgotPasswordController),
              SizedBox(height: 20),
              _buildPasswordForm(context, forgotPasswordController),
              SizedBox(height: 20),
              _buildConfirmPasswordForm(context, forgotPasswordController),
              Spacer(),
              _buildForgotPasswordButton(context, forgotPasswordController),
              SizedBox(height: 30),
            ],
          ),
          CustomBackButton(),
        ],
      ),
    );
  }

  _buildTitle(context) {
    return CustomTitle(
        titleText: DemoLocalizations.of(context)!.trans("ForgotPasswordText"));
  }

  _buildCodeForm(context, forgotPasswordController) {
    return CustomOtpCodeForm(
      icon: FontAwesomeIcons.sms,
      text: DemoLocalizations.of(context)!.trans("OtpCode"),
      controller: forgotPasswordController.codeController.value,
      screenType: "Forgot",
    );
  }

  _buildPasswordForm(context, forgotPasswordController) {
    return CustomPasswordForm(
      icon: FontAwesomeIcons.key,
      text: DemoLocalizations.of(context)!.trans("Password"),
      controller: forgotPasswordController.passwordController.value,
      screenType: "Forgot",
    );
  }

  _buildConfirmPasswordForm(context, forgotPasswordController) {
    return CustomPasswordForm(
      icon: FontAwesomeIcons.lock,
      text: DemoLocalizations.of(context)!.trans("ConfirmPassword"),
      controller: forgotPasswordController.confirmPasswordController.value,
      screenType: "Forgot",
    );
  }

  _buildForgotPasswordButton(context, forgotPasswordController) {
    return Obx(() {
      return MaterialButton(
        onPressed: forgotPasswordController.isValid.value
            ? () {
                forgotPasswordController.forgotPassword(context);
              }
            : null,
        disabledColor: config.Colors().accentColor(1).withOpacity(0.5),
        color: config.Colors().accentColor(1),
        minWidth: MediaQuery.of(context).size.width,
        shape: RoundedRectangleBorder(
          borderRadius: BorderRadius.circular(10.0),
        ),
        child: (forgotPasswordController.isLoading.value)
            ? CircularProgressIndicator(
                strokeWidth: 2,
                color: config.Colors().primaryColorLight(1),
              )
            : Text(DemoLocalizations.of(context)!.trans("Submit"),
                style: Theme.of(context).textTheme.subtitle1),
        height: 55.0,
      );
    });
  }
}

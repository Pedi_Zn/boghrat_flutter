import 'package:boghrat_flutter/Areas/User/Controllers/RegisterController.dart';
import 'package:boghrat_flutter/Areas/User/Views/LoginScreen.dart';
import 'package:boghrat_flutter/Widgets/BoghratBackground.dart';
import 'package:boghrat_flutter/Widgets/BoghtatLogo.dart';
import 'package:boghrat_flutter/Localization/Localization.dart';
import 'package:boghrat_flutter/Widgets/CustomNationalCodeForm.dart';
import 'package:boghrat_flutter/Widgets/CustomPhoneForm.dart';
import 'package:boghrat_flutter/Widgets/CustomTitle.dart';
import 'package:flutter/material.dart';
import 'package:font_awesome_flutter/font_awesome_flutter.dart';
import 'package:get/get.dart';
import 'package:boghrat_flutter/Config/AppConfig.dart' as config;

class RegisterScreen extends StatelessWidget {
  final registerController = Get.put(RegisterController());
  RegisterScreen({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return BoghratBackground(
      children: [
        BoghratLogo(),
        _buildTitle(context),
        Spacer(),
        _buildPhoneForm(context),
        SizedBox(height: 10),
        _buildNationalCodeForm(context),
        Spacer(),
        _buildRegisterButton(context),
        SizedBox(height: 10),
        _buildHaveAccountButton(context),
        SizedBox(height: 30),
      ],
    );
  }

  _buildTitle(context) {
    return CustomTitle(
        titleText: DemoLocalizations.of(context)!.trans("RegisterText"));
  }

  _buildPhoneForm(context) {
    return CustomPhoneForm(
      icon: FontAwesomeIcons.phone,
      text: DemoLocalizations.of(context)!.trans("Phone"),
      controller: registerController.phoneController.value,
      screenType: "Register",
    );
  }

  _buildNationalCodeForm(context) {
    return CustomNationalCodeForm(
      icon: FontAwesomeIcons.idCard,
      text: DemoLocalizations.of(context)!.trans("NationalCode"),
      controller: registerController.nationalCodeController.value,
      screenType: "Register",
    );
  }

  _buildRegisterButton(context) {
    return Obx(() {
      return MaterialButton(
        onPressed: registerController.isValid.value
            ? () {
                registerController.register(context);
              }
            : null,
        disabledColor: config.Colors().accentColor(1).withOpacity(0.5),
        color: config.Colors().accentColor(1),
        minWidth: MediaQuery.of(context).size.width,
        shape: RoundedRectangleBorder(
          borderRadius: BorderRadius.circular(10.0),
        ),
        child: (registerController.isLoading.value)
            ? CircularProgressIndicator(
                strokeWidth: 2,
                color: config.Colors().primaryColorLight(1),
              )
            : Text(DemoLocalizations.of(context)!.trans("Register"),
                style: Theme.of(context).textTheme.subtitle1),
        height: 55.0,
      );
    });
  }

  _buildHaveAccountButton(context) {
    return MaterialButton(
      onPressed: () {
        Get.to(() => LoginScreen(), transition: Transition.upToDown);
      },
      color: config.Colors().primaryColor(1),
      minWidth: MediaQuery.of(context).size.width,
      shape: RoundedRectangleBorder(
        borderRadius: BorderRadius.circular(10.0),
      ),
      child: Text(DemoLocalizations.of(context)!.trans("HaveAccount"),
          style: Theme.of(context).textTheme.subtitle2),
      height: 55.0,
    );
  }
}

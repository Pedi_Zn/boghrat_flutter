import 'package:boghrat_flutter/Areas/User/Controllers/RegisterV2Controller.dart';
import 'package:boghrat_flutter/Areas/User/Models/RegisterModel.dart';
import 'package:boghrat_flutter/Widgets/BoghratBackground.dart';
import 'package:boghrat_flutter/Widgets/BoghtatLogo.dart';
import 'package:boghrat_flutter/Localization/Localization.dart';
import 'package:boghrat_flutter/Widgets/CustomBackButton.dart';
import 'package:boghrat_flutter/Widgets/CustomNameForm.dart';
import 'package:boghrat_flutter/Widgets/CustomTitle.dart';
import 'package:boghrat_flutter/Widgets/CustomTitleRow.dart';
import 'package:boghrat_flutter/Widgets/GenderField.dart';
import 'package:flutter/material.dart';
import 'package:font_awesome_flutter/font_awesome_flutter.dart';
import 'package:get/get.dart';
import 'package:boghrat_flutter/Config/AppConfig.dart' as config;

class RegisterV2Screen extends StatelessWidget {
  final RegisterDS registerDS;
  RegisterV2Screen({Key? key, required this.registerDS}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    final registerV2Controller = Get.put(RegisterV2Controller(registerDS));
    return Scaffold(
      body: Stack(
        children: [
          BoghratBackground(
            children: [
              BoghratLogo(),
              _buildTitle(context),
              Spacer(),
              _buildGenderForm(context, registerV2Controller),
              SizedBox(height: 10),
              _buildFirstNameForm(context, registerV2Controller),
              SizedBox(height: 15),
              _buildLastNameForm(context, registerV2Controller),
              Spacer(),
              _buildRegisterButton(context, registerV2Controller),
              SizedBox(height: 30),
            ],
          ),
          CustomBackButton(),
        ],
      ),
    );
  }

  _buildTitle(context) {
    return CustomTitle(
        titleText: DemoLocalizations.of(context)!.trans("RegisterText"));
  }

  _buildGenderForm(context, registerV2Controller) {
    return Column(
      crossAxisAlignment: CrossAxisAlignment.start,
      children: [
        CustomTitleRow(
          icon: FontAwesomeIcons.venusMars,
          text: DemoLocalizations.of(context)!.trans("Gender"),
          space: 10,
          textStyle: Theme.of(context).textTheme.subtitle2!,
        ),
        GenderField(['مرد', 'زن'], registerV2Controller.genderController.value),
        SizedBox(height: 3),
        Divider(
          height: 2.0,
          color: Colors.black45,
        )
      ],
    );
  }

  _buildFirstNameForm(context, registerV2Controller) {
    return CustomNameForm(
      icon: FontAwesomeIcons.solidUser,
      text: DemoLocalizations.of(context)!.trans("FirstName"),
      controller: registerV2Controller.firstNameController.value,
      screenType: "Register",
    );
  }

  _buildLastNameForm(context, registerV2Controller) {
    return CustomNameForm(
      icon: FontAwesomeIcons.solidIdBadge,
      text: DemoLocalizations.of(context)!.trans("LastName"),
      controller: registerV2Controller.lastNameController.value,
      screenType: "Register",
    );
  }

  _buildRegisterButton(context, registerV2Controller) {
    return Obx(() {
      return MaterialButton(
        onPressed: registerV2Controller.isValid.value
            ? () {
                registerV2Controller.registerV2(context);
              }
            : null,
        disabledColor: config.Colors().accentColor(1).withOpacity(0.5),
        color: config.Colors().accentColor(1),
        minWidth: MediaQuery.of(context).size.width,
        shape: RoundedRectangleBorder(
          borderRadius: BorderRadius.circular(10.0),
        ),
        child: (registerV2Controller.isLoading.value)
            ? CircularProgressIndicator(
                strokeWidth: 2,
                color: config.Colors().primaryColorLight(1),
              )
            : Text(DemoLocalizations.of(context)!.trans("Register"),
                style: Theme.of(context).textTheme.subtitle1),
        height: 55.0,
      );
    });
  }
}

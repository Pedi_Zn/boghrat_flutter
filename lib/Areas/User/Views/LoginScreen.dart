import 'package:boghrat_flutter/Areas/User/Controllers/LoginController.dart';
import 'package:boghrat_flutter/Widgets/BoghratBackground.dart';
import 'package:boghrat_flutter/Widgets/BoghtatLogo.dart';
import 'package:boghrat_flutter/Widgets/CustomFormField.dart';
import 'package:boghrat_flutter/Localization/Localization.dart';
import 'package:boghrat_flutter/Widgets/CustomPhoneForm.dart';
import 'package:boghrat_flutter/Widgets/CustomTitle.dart';
import 'package:flutter/material.dart';
import 'package:font_awesome_flutter/font_awesome_flutter.dart';
import 'package:get/get.dart';
import 'package:boghrat_flutter/Config/AppConfig.dart' as config;

class LoginScreen extends StatelessWidget {
  final loginController = Get.put(LoginController());
  LoginScreen({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return BoghratBackground(
      children: [
        BoghratLogo(),
        _buildTitle(context),
        Spacer(),
        _buildPhoneForm(context),
        SizedBox(height: 20),
        _buildPasswordForm(context),
        Spacer(),
        _buildForgotPasswordButton(context),
        Spacer(),
        _buildLoginButton(context),
        SizedBox(height: 10),
        _buildHaveNoAccountButton(context),
        SizedBox(height: 30),
      ],
    );
  }

  _buildTitle(context) {
    return CustomTitle(
        titleText: DemoLocalizations.of(context)!.trans("LoginText"));
  }

  _buildPhoneForm(context) {
    return CustomPhoneForm(
      icon: FontAwesomeIcons.phone,
      text: DemoLocalizations.of(context)!.trans("Phone"),
      controller: loginController.phoneController.value,
      screenType: "Login",
    );
  }

  _buildPasswordForm(context) {
    return CustomPasswordForm(
      icon: FontAwesomeIcons.key,
      text: DemoLocalizations.of(context)!.trans("Password"),
      controller: loginController.passwordController.value,
      screenType: "Login",
    );
  }

  _buildForgotPasswordButton(context) {
    return TextButton(
      onPressed: () {
        loginController.forgotPassword(context);
      },
      child: Text(
        DemoLocalizations.of(context)!.trans("ForgotPassword"),
        style: Theme.of(context).textTheme.headline2,
      ),
    );
  }

  _buildLoginButton(context) {
    return Obx(() {
      return MaterialButton(
        // onPressed: () {
        //   loginController.login(context);
        // },
        onPressed: loginController.isValid.value
            ? () {
                loginController.login(context);
              }
            : null,
        disabledColor: config.Colors().accentColor(1).withOpacity(0.5),
        color: config.Colors().accentColor(1),
        minWidth: MediaQuery.of(context).size.width,
        shape: RoundedRectangleBorder(
          borderRadius: BorderRadius.circular(10.0),
        ),
        child: (loginController.isLoading.value)
            ? CircularProgressIndicator(
                strokeWidth: 2,
                color: config.Colors().primaryColorLight(1),
              )
            : Text(DemoLocalizations.of(context)!.trans("Login"),
                style: Theme.of(context).textTheme.subtitle1),
        height: 55.0,
      );
    });
  }

  _buildHaveNoAccountButton(context) {
    return MaterialButton(
      onPressed: () {
        // Get.to(() => RegisterScreen(), transition: Transition.downToUp);
        Get.back();
      },
      color: config.Colors().primaryColor(1),
      minWidth: MediaQuery.of(context).size.width,
      shape: RoundedRectangleBorder(
        borderRadius: BorderRadius.circular(10.0),
      ),
      child: Text(DemoLocalizations.of(context)!.trans("HaveNoAccount"),
          style: Theme.of(context).textTheme.subtitle2),
      height: 55.0,
    );
  }
}

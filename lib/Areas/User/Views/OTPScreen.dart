import 'package:boghrat_flutter/Areas/User/Controllers/OTPController.dart';
import 'package:boghrat_flutter/Areas/User/Models/RegisterModel.dart';
import 'package:boghrat_flutter/Widgets/CustomBackButton.dart';
import 'package:flutter/material.dart';
import 'package:boghrat_flutter/Localization/Localization.dart';
import 'package:flutter/services.dart';
import 'package:get/get.dart';
import 'package:lottie/lottie.dart';
import 'package:pin_code_fields/pin_code_fields.dart';
import 'package:persian_number_utility/persian_number_utility.dart';
import 'package:boghrat_flutter/Config/AppConfig.dart' as config;

class OTPScreen extends StatelessWidget {
  final RegisterDS registerDS;
  final String screenType;
  OTPScreen({Key? key, required this.registerDS, required this.screenType})
      : super(key: key);

  @override
  Widget build(BuildContext context) {
    final otpController = Get.put(OTPController(registerDS));

    return Scaffold(
      backgroundColor: Colors.red.shade50,
      body: Row(
        mainAxisAlignment: MainAxisAlignment.center,
        children: [
          Container(
            color: config.Colors().primaryColorLight(1),
            width: MediaQuery.of(context).size.width,
            constraints: BoxConstraints(maxWidth: 500),
            child: Column(
              mainAxisAlignment: MainAxisAlignment.spaceBetween,
              children: [
                CustomBackButton(),
                _buildLogo(context),
                _buildTitle(context),
                SizedBox(height: 25),
                _buildStaredPhoneTextSpan(context),
                SizedBox(height: 20),
                _buildPincode(context, otpController),
                _buildClearButton(context, otpController),
                SizedBox(height: 40),
                _buildSubmitButton(context, otpController),
                SizedBox(height: 20),
              ],
            ),
          )
        ],
      ),
    );
  }

  _buildLogo(BuildContext context) {
    return Container(
      height: MediaQuery.of(context).size.height / 3.2,
      child: Lottie.asset("assets/images/otp.json"),
    );
  }

  _buildTitle(BuildContext context) {
    return Text(
      this.screenType == "Register"
          ? DemoLocalizations.of(context)!.trans("Register")
          : DemoLocalizations.of(context)!.trans("ChangePassword"),
      textAlign: TextAlign.center,
      style: Theme.of(context).textTheme.headline4,
    );
  }

  _buildStaredPhoneTextSpan(BuildContext context) {
    return Padding(
      padding: const EdgeInsets.symmetric(horizontal: 30.0),
      child: RichText(
        textAlign: TextAlign.center,
        text: TextSpan(
          children: [
            TextSpan(
              text: "کد ارسالی به شماره ",
              style: Theme.of(context).textTheme.headline2,
            ),
            TextSpan(
                text: this
                    .registerDS
                    .phoneNumber
                    .toString()
                    .substring(7, 11)
                    .toPersianDigit(),
                style: Theme.of(context).textTheme.headline6),
            TextSpan(
                text: " *** ", style: Theme.of(context).textTheme.headline6),
            TextSpan(
                text: this
                    .registerDS
                    .phoneNumber
                    .toString()
                    .substring(0, 4)
                    .toPersianDigit(),
                style: Theme.of(context).textTheme.headline6),
            TextSpan(
              text: " را وارد کنید",
              style: Theme.of(context).textTheme.headline2,
            )
          ],
        ),
      ),
    );
  }

  _buildPincode(BuildContext context, OTPController otpController) {
    return Container(
      padding: const EdgeInsets.symmetric(horizontal: 45),
      child: Directionality(
        textDirection: TextDirection.ltr,
        child: PinCodeTextField(
          keyboardType: TextInputType.number,
          length: 4,
          autoFocus: true,
          enabled: true,
          obscureText: false,
          autoDisposeControllers: false,
          animationType: AnimationType.fade,
          animationDuration: Duration(milliseconds: 300),
          enableActiveFill: true,
          mainAxisAlignment: MainAxisAlignment.spaceEvenly,
          controller: otpController.codeController.value,
          pinTheme: PinTheme(
            shape: PinCodeFieldShape.box,
            activeColor: config.Colors().accentColor(1),
            selectedColor: config.Colors().accentColor(1),
            activeFillColor: config.Colors().accentColor(0.3),
            inactiveColor: config.Colors().accentColor(0.5),
            inactiveFillColor: Colors.white70,
            selectedFillColor: config.Colors().accentColor(0.5),
            borderRadius: BorderRadius.circular(10.0),
            fieldHeight: 55,
            fieldWidth: 55,
            disabledColor: config.Colors().primaryColorLight(1),
          ),
          inputFormatters: [
            // PhoneInputFormatter(),
            // FilteringTextInputFormatter.allow(RegExp(r'[\u06F0-\u06F90-9]')),
          ],
          onChanged: (value) {
            print(value);
            otpController.checkIsValid();
          },
          onCompleted: (value) {
            if (otpController.isValid.value) {
              otpController.verifyCode(context);
            }
          },
          beforeTextPaste: (text) {
            return false;
          },
          appContext: context,
        ),
      ),
    );
  }

  _buildClearButton(BuildContext context, OTPController otpController) {
    return Container(
      padding: const EdgeInsets.symmetric(horizontal: 30),
      child: Row(
        mainAxisAlignment: MainAxisAlignment.end,
        children: <Widget>[
          TextButton(
            onPressed: () {
              otpController.codeController.value.clear();
            },
            child: Text(
              DemoLocalizations.of(context)!.trans("Clear"),
              style: Theme.of(context).textTheme.headline6,
            ),
          ),
        ],
      ),
    );
  }

  _buildSubmitButton(BuildContext context, OTPController otpController) {
    return Obx(() {
      return Container(
        padding: EdgeInsets.symmetric(horizontal: 30.0),
        child: MaterialButton(
          onPressed: otpController.resend.value
              ? () {
                  otpController.resendCode(context);
                }
              : null,
          disabledColor: config.Colors().accentColor(1).withOpacity(0.7),
          color: config.Colors().accentColor(1),
          minWidth: MediaQuery.of(context).size.width,
          shape: RoundedRectangleBorder(
            borderRadius: BorderRadius.circular(10.0),
          ),
          child: (otpController.isLoading.value)
              ? CircularProgressIndicator(
                  strokeWidth: 2,
                  color: config.Colors().primaryColorLight(1),
                )
              : otpController.resend.value
                  ? Text(DemoLocalizations.of(context)!.trans("ResendCode"),
                      style: Theme.of(context).textTheme.subtitle1)
                  : Text(
                      otpController.counter.value.toString().toPersianDigit(),
                      style: TextStyle(
                          color: config.Colors().primaryColorLight(0.9)),
                    ),
          height: 55.0,
        ),
      );
    });
  }
}

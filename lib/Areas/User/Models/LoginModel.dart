import 'dart:convert';

LoginModel loginModelFromJson(String? str) {
  if (str == null) {
    return LoginModel.fromJson(json.decode("{}"));
  }
  return LoginModel.fromJson(json.decode(str));
}

String loginModelToJson(LoginModel data) => json.encode(data.toJson());

class LoginModel {
  LoginModel({
    required this.accessToken,
    required this.tokenType,
    required this.expiresIn,
    required this.issued,
    required this.expires,
    required this.error,
  });

  String? accessToken;
  String? tokenType;
  int? expiresIn;
  String? issued;
  String? expires;
  String? error;

  factory LoginModel.fromJson(Map<String, dynamic> json) => LoginModel(
        accessToken: json["access_token"] == null ? null : json["access_token"],
        tokenType: json["token_type"] == null ? null : json["token_type"],
        expiresIn: json["expires_in"] == null ? null : json["expires_in"],
        issued: json[".issued"] == null ? null : json[".issued"],
        expires: json[".expires"] == null ? null : json[".expires"],
        error: json["error"] == null ? null : json["error"],
      );

  Map<String, dynamic> toJson() => {
        "access_token": accessToken == null ? null : accessToken,
        "token_type": tokenType == null ? null : tokenType,
        "expires_in": expiresIn == null ? null : expiresIn,
        ".issued": issued == null ? null : issued,
        ".expires": expires == null ? null : expires,
      };
}

String loginDSToJson(LoginDS data) => json.encode(data.toJson());

class LoginDS {
  LoginDS({
    grantType,
    userName,
    password,
  });

  String? grantType;
  String? userName;
  String? password;

  set setGrantType(String type) {
    grantType = type;
  }

  String? get getGrantType {
    return grantType;
  }

  set setUsername(String name) {
    userName = name;
  }

  String? get getUsername {
    return userName;
  }

  set setPassword(String name) {
    password = name;
  }

  String? get getPassword {
    return password;
  }

  Map<String, dynamic> toJson() => {
        "userName": userName == null ? null : userName,
        "password": password == null ? null : password,
        "grant_type": grantType == null ? null : grantType,
      };
}

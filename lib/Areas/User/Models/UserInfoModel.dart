import 'dart:convert';

UserInfoModel userInfoModelFromJson(String? str) {
  if (str == null) {
    return UserInfoModel.fromJson(json.decode("{}"));
  }
  return UserInfoModel.fromJson(json.decode(str));
}

String userInfoModelToJson(UserInfoModel data) => json.encode(data.toJson());

class UserInfoModel {
  UserInfoModel({
    required this.id,
    required this.userName,
    required this.firstName,
    required this.lastName,
    required this.fullName,
    required this.email,
    required this.phoneNumber,
    required this.userType,
    required this.genderId,
    required this.isDemo,
    required this.profileImageId,
  });

  String? id;
  String? userName;
  String? firstName;
  String? lastName;
  String? fullName;
  String? email;
  String? phoneNumber;
  int? userType;
  int? genderId;
  bool? isDemo;
  String? profileImageId;

  factory UserInfoModel.fromJson(Map<String, dynamic> json) => UserInfoModel(
        id: json["id"] == null ? null : json["id"],
        userName: json["userName"] == null ? null : json["userName"],
        firstName: json["firstName"] == null ? null : json["firstName"],
        lastName: json["lastName"] == null ? null : json["lastName"],
        fullName: json["fullName"] == null ? null : json["fullName"],
        email: json["email"] == null ? null : json["email"],
        phoneNumber: json["phoneNumber"] == null ? null : json["phoneNumber"],
        userType: json["userType"] == null ? null : json["userType"],
        genderId: json["genderId"] == null ? null : json["genderId"],
        isDemo: json["isDemo"] == null ? null : json["isDemo"],
        profileImageId:
            json["profileImageId"] == null ? null : json["profileImageId"],
      );

  Map<String, dynamic> toJson() => {
        "id": id == null ? null : id,
        "userName": userName == null ? null : userName,
        "firstName": firstName == null ? null : firstName,
        "lastName": lastName == null ? null : lastName,
        "fullName": fullName == null ? null : fullName,
        "email": email == null ? null : email,
        "phoneNumber": phoneNumber == null ? null : phoneNumber,
        "userType": userType == null ? null : userType,
        "genderId": genderId == null ? null : genderId,
        "isDemo": isDemo == null ? null : isDemo,
        "profileImageId": profileImageId == null ? null : profileImageId,
      };
}

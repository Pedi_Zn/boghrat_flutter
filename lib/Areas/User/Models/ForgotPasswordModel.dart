import 'dart:convert';

ForgotPasswordModel forgotPasswordModelFromJson(String? str) {
  if (str == null) {
    return ForgotPasswordModel.fromJson(json.decode("{}"));
  }
  return ForgotPasswordModel.fromJson(json.decode(str));
}

String forgotPasswordModelToJson(ForgotPasswordModel data) =>
    json.encode(data.toJson());

class ForgotPasswordModel {
  ForgotPasswordModel({
    required this.resultFlag,
    required this.resultCode,
    required this.result,
    required this.errorMessages,
    required this.systemErrorMessages,
    required this.messages,
  });

  bool? resultFlag;
  double? resultCode;
  dynamic result;
  List<dynamic>? errorMessages;
  List<dynamic>? systemErrorMessages;
  List<dynamic>? messages;

  factory ForgotPasswordModel.fromJson(Map<String, dynamic> json) =>
      ForgotPasswordModel(
        resultFlag: json["resultFlag"] == null ? null : json["resultFlag"],
        resultCode: json["resultCode"] == null ? null : json["resultCode"],
        result: json["result"],
        errorMessages: json["errorMessages"] == null
            ? null
            : List<dynamic>.from(json["errorMessages"].map((x) => x)),
        systemErrorMessages: json["systemErrorMessages"] == null
            ? null
            : List<dynamic>.from(json["systemErrorMessages"].map((x) => x)),
        messages: json["messages"] == null
            ? null
            : List<dynamic>.from(json["messages"].map((x) => x)),
      );

  Map<String, dynamic> toJson() => {
        "resultFlag": resultFlag == null ? null : resultFlag,
        "resultCode": resultCode == null ? null : resultCode,
        "result": result,
        "errorMessages": errorMessages == null
            ? null
            : List<dynamic>.from(errorMessages!.map((x) => x)),
        "systemErrorMessages": systemErrorMessages == null
            ? null
            : List<dynamic>.from(systemErrorMessages!.map((x) => x)),
        "messages": messages == null
            ? null
            : List<dynamic>.from(messages!.map((x) => x)),
      };
}

class ForgotPasswordDS {
  ForgotPasswordDS({
    phoneCode,
    phoneNumber,
    newPassword,
    confirmPassword,
  });

  String? phoneCode;
  String? phoneNumber;
  String? newPassword;
  String? confirmPassword;

  set setPhoneCode(String code) {
    phoneCode = code;
  }

  String? get getPhoneCode {
    return phoneCode;
  }

  set setPhoneNumber(String phone) {
    phoneNumber = phone;
  }

  String? get getPhoneNumber {
    return phoneNumber;
  }

  set setNewPassword(String password) {
    newPassword = password;
  }

  String? get getNewPassword {
    return newPassword;
  }

  set setConfirmPassword(String password) {
    confirmPassword = password;
  }

  String? get getConfirmPassword {
    return confirmPassword;
  }

  Map<String, dynamic> toJson() => {
        "phoneNumber": phoneNumber == null ? null : phoneNumber,
        "newPassword": newPassword == null ? null : newPassword,
        "confirmPassword": confirmPassword == null ? null : confirmPassword,
        "phoneCode": phoneCode == null ? null : phoneCode,
      };
}

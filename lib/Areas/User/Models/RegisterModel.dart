import 'dart:convert';

import 'package:boghrat_flutter/Areas/User/Models/UserInfoModel.dart';

RegisterModel registerModelFromJson(String? str) {
  if (str == null) {
    return RegisterModel.fromJson(json.decode("{}"));
  }
  return RegisterModel.fromJson(json.decode(str));
}

String registerModelToJson(RegisterModel data) => json.encode(data.toJson());

class RegisterModel {
  RegisterModel({
    required this.userInfo,
    required this.requireEmailVerification,
    required this.requirePhoneNumberVerification,
    required this.resultFlag,
    required this.resultCode,
    required this.result,
    required this.errorMessages,
    required this.systemErrorMessages,
    required this.messages,
  });

  UserInfoModel? userInfo;
  bool? requireEmailVerification;
  bool? requirePhoneNumberVerification;
  bool? resultFlag;
  double? resultCode;
  dynamic result;
  List<dynamic>? errorMessages;
  List<dynamic>? systemErrorMessages;
  List<dynamic>? messages;

  factory RegisterModel.fromJson(Map<String, dynamic> json) => RegisterModel(
        userInfo: json["userInfo"] == null
            ? null
            : UserInfoModel.fromJson(json["userInfo"]),
        requireEmailVerification: json["requireEmailVerification"] == null
            ? null
            : json["requireEmailVerification"],
        requirePhoneNumberVerification:
            json["requirePhoneNumberVerification"] == null
                ? null
                : json["requirePhoneNumberVerification"],
        resultFlag: json["resultFlag"] == null ? null : json["resultFlag"],
        resultCode: json["resultCode"] == null ? null : json["resultCode"],
        result: json["result"],
        errorMessages: json["errorMessages"] == null
            ? null
            : List<dynamic>.from(json["errorMessages"].map((x) => x)),
        systemErrorMessages: json["systemErrorMessages"] == null
            ? null
            : List<dynamic>.from(json["systemErrorMessages"].map((x) => x)),
        messages: json["messages"] == null
            ? null
            : List<dynamic>.from(json["messages"].map((x) => x)),
      );

  Map<String, dynamic> toJson() => {
        "userInfo": userInfo == null ? null : userInfo!.toJson(),
        "requireEmailVerification":
            requireEmailVerification == null ? null : requireEmailVerification,
        "requirePhoneNumberVerification": requirePhoneNumberVerification == null
            ? null
            : requirePhoneNumberVerification,
        "resultFlag": resultFlag == null ? null : resultFlag,
        "resultCode": resultCode == null ? null : resultCode,
        "result": result,
        "errorMessages": errorMessages == null
            ? null
            : List<dynamic>.from(errorMessages!.map((x) => x)),
        "systemErrorMessages": systemErrorMessages == null
            ? null
            : List<dynamic>.from(systemErrorMessages!.map((x) => x)),
        "messages": messages == null
            ? null
            : List<dynamic>.from(messages!.map((x) => x)),
      };
}

class RegisterDS {
  RegisterDS({
    phoneNumber,
    nationalCode,
  });

  String? phoneNumber;
  String? nationalCode;

  set setPhoneNumber(String phone) {
    phoneNumber = phone;
  }

  String? get getPhoneNumber {
    return phoneNumber;
  }

  set setNationalCode(String code) {
    nationalCode = code;
  }

  String? get getNationalCode {
    return nationalCode;
  }

  Map<String, dynamic> toJson() => {
        "phoneNumber": phoneNumber == null ? null : phoneNumber,
        "nationalCode": nationalCode == null ? null : nationalCode,
      };
}

class RegisterV2DS {
  RegisterV2DS({
    phoneNumber,
    nationalCode,
    firstName,
    lastName,
    genderId,
    nationalCodeIsValid,
    typeId,
  });

  String? phoneNumber;
  String? nationalCode;
  String? firstName;
  String? lastName;
  String? genderId;
  String? nationalCodeIsValid;
  // Country? country;
  String? typeId;

  set setNationalCodeIsValid(String code) {
    nationalCodeIsValid = code;
  }

  set setTypeId(String id) {
    typeId = id;
  }

  set setPhoneNumber(String phone) {
    phoneNumber = phone;
  }

  String? get getPhoneNumber {
    return phoneNumber;
  }

  set setNationalCode(String code) {
    nationalCode = code;
  }

  String? get getNationalCode {
    return nationalCode;
  }

  set setFirstName(String name) {
    firstName = name;
  }

  String? get getFirstName {
    return firstName;
  }

  set setLastName(String name) {
    lastName = name;
  }

  String? get getLastName {
    return lastName;
  }

  set setGenderId(String id) {
    genderId = id;
  }

  String? get getGenderId {
    return genderId;
  }

  Map<String, dynamic> toJson() => {
        "phoneNumber": phoneNumber == null ? null : phoneNumber,
        "nationalCode": nationalCode == null ? null : nationalCode,
        "firstName": firstName == null ? null : firstName,
        "lastName": lastName == null ? null : lastName,
        "genderId": genderId == null ? null : genderId,
        "confirmPassword": nationalCode == null ? null : nationalCode,
        "password": nationalCode == null ? null : nationalCode,
        "nationalCodeIsValid":
            nationalCodeIsValid == null ? null : nationalCodeIsValid,
        // "country": country!.toJson(),
        "typeId": typeId == null ? null : typeId,
      };
}

class Country {
  Country({
    required this.id,
  });

  int id;

  factory Country.fromJson(Map<String, dynamic> json) => Country(
        id: json["id"] == null ? null : json["id"],
      );

  Map<String, dynamic> toJson() => {
        "id": id == null ? 1 : 1,
      };
}

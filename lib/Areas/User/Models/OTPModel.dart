import 'dart:convert';

import 'package:boghrat_flutter/Areas/User/Models/UserInfoModel.dart';

OTPModel otpModelFromJson(String? str) {
  if (str == null) {
    return OTPModel.fromJson(json.decode("{}"));
  }
  return OTPModel.fromJson(json.decode(str));
}

String otpModelToJson(OTPModel data) => json.encode(data.toJson());

class OTPModel {
  OTPModel({
    required this.userInfo,
    required this.requireEmailVerification,
    required this.requirePhoneNumberVerification,
    required this.resultFlag,
    required this.resultCode,
    required this.result,
    required this.errorMessages,
    required this.systemErrorMessages,
    required this.messages,
  });

  UserInfoModel? userInfo;
  bool? requireEmailVerification;
  bool? requirePhoneNumberVerification;
  bool? resultFlag;
  double? resultCode;
  dynamic result;
  List<dynamic>? errorMessages;
  List<dynamic>? systemErrorMessages;
  List<dynamic>? messages;

  factory OTPModel.fromJson(Map<String, dynamic> json) => OTPModel(
        userInfo: json["userInfo"] == null
            ? null
            : UserInfoModel.fromJson(json["userInfo"]),
        requireEmailVerification: json["requireEmailVerification"] == null
            ? null
            : json["requireEmailVerification"],
        requirePhoneNumberVerification:
            json["requirePhoneNumberVerification"] == null
                ? null
                : json["requirePhoneNumberVerification"],
        resultFlag: json["resultFlag"] == null ? null : json["resultFlag"],
        resultCode: json["resultCode"] == null ? null : json["resultCode"],
        result: json["result"],
        errorMessages: json["errorMessages"] == null
            ? null
            : List<dynamic>.from(json["errorMessages"].map((x) => x)),
        systemErrorMessages: json["systemErrorMessages"] == null
            ? null
            : List<dynamic>.from(json["systemErrorMessages"].map((x) => x)),
        messages: json["messages"] == null
            ? null
            : List<dynamic>.from(json["messages"].map((x) => x)),
      );

  Map<String, dynamic> toJson() => {
        "userInfo": userInfo == null ? null : userInfo!.toJson(),
        "requireEmailVerification":
            requireEmailVerification == null ? null : requireEmailVerification,
        "requirePhoneNumberVerification": requirePhoneNumberVerification == null
            ? null
            : requirePhoneNumberVerification,
        "resultFlag": resultFlag == null ? null : resultFlag,
        "resultCode": resultCode == null ? null : resultCode,
        "result": result,
        "errorMessages": errorMessages == null
            ? null
            : List<dynamic>.from(errorMessages!.map((x) => x)),
        "systemErrorMessages": systemErrorMessages == null
            ? null
            : List<dynamic>.from(systemErrorMessages!.map((x) => x)),
        "messages": messages == null
            ? null
            : List<dynamic>.from(messages!.map((x) => x)),
      };
}

String otpDSToJson(OTPDS data) => json.encode(data.toJson());

class OTPDS {
  OTPDS({
    phoneCode,
    phoneNumber,
    password,
  });

  String? phoneCode;
  String? phoneNumber;
  String? password;

  set setPhoneCode(String code) {
    phoneCode = code;
  }

  String? get getPhoneCode {
    return phoneCode;
  }

  set setPhoneNumber(String phone) {
    phoneNumber = phone;
  }

  String? get getPhoneNumber {
    return phoneNumber;
  }

  set setPassword(String name) {
    password = name;
  }

  String? get getPassword {
    return password;
  }

  Map<String, dynamic> toJson() => {
        "phoneNumber": phoneNumber == null ? null : phoneNumber,
        "password": password == null ? null : password,
        "phoneCode": phoneCode == null ? null : phoneCode,
      };
}

String sendCodeDSToJson(SendCodeDS data) => json.encode(data.toJson());

class SendCodeDS {
  SendCodeDS({
    phoneNumber,
  });

  String? phoneNumber;

  set setPhoneNumber(String phone) {
    phoneNumber = phone;
  }

  String? get getPhoneNumber {
    return phoneNumber;
  }

  Map<String, dynamic> toJson() => {
        "phoneNumber": phoneNumber == null ? null : phoneNumber,
      };
}

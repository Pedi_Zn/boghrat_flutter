import 'package:boghrat_flutter/Areas/User/Models/OTPModel.dart';
import 'package:boghrat_flutter/Areas/User/Models/RegisterModel.dart';
import 'package:boghrat_flutter/Areas/User/Views/OTPScreen.dart';
import 'package:boghrat_flutter/Areas/User/Views/RegisterV2Screen.dart';
import 'package:boghrat_flutter/Config/AppConfig.dart';
import 'package:boghrat_flutter/Config/BaseController.dart';
import 'package:boghrat_flutter/Services/User/RegisterService.dart';
import 'package:flutter/cupertino.dart';
import 'package:get/get.dart';
import 'package:persian_number_utility/persian_number_utility.dart';

class RegisterController extends BaseController {
  RegisterService registerService = new RegisterService();

  late RegisterDS registerDS = new RegisterDS();
  late SendCodeDS sendCodeDS = new SendCodeDS();

  var phoneController = TextEditingController().obs;
  var nationalCodeController = TextEditingController().obs;

  @override
  void onInit() {
    super.onInit();
    phoneController.value.text = "";
    nationalCodeController.value.text = "";
  }

  @override
  void onClose() {
    super.onClose();
  }

  void getModel() {
    registerDS.setPhoneNumber =
        phoneController.value.text.replaceAll(" ", "").toEnglishDigit();
    registerDS.setNationalCode =
        nationalCodeController.value.text.replaceAll(" ", "").toEnglishDigit();
  }

  void getSendCodeModel() {
    sendCodeDS.setPhoneNumber =
        phoneController.value.text.replaceAll(" ", "").toEnglishDigit();
  }

  void checkIsValid() {
    getModel();
    isValid(registerService.checkIsValid(registerDS).flag);
  }

  Future register(context) async {
    getModel();
    isLoading(true);
    try {
      var response = await registerService.register(registerDS);
      if (response.flag) {
        getModel();
        Get.to(
            () => OTPScreen(
                  registerDS: registerDS,
                  screenType: "Register",
                ),
            transition: Transition.upToDown);
      } else if (response.errors.length < 1) {
        Get.to(
            () => RegisterV2Screen(
                  registerDS: registerDS,
                ),
            transition: Transition.upToDown);
      } else {
        Tools().showDialog(context, response.errors);
      }
    } finally {
      isLoading(false);
    }
  }
}

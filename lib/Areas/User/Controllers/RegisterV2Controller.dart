import 'package:boghrat_flutter/Areas/User/Models/OTPModel.dart';
import 'package:boghrat_flutter/Areas/User/Models/RegisterModel.dart';
import 'package:boghrat_flutter/Areas/User/Views/OTPScreen.dart';
import 'package:boghrat_flutter/Config/AppConfig.dart';
import 'package:boghrat_flutter/Config/BaseController.dart';
import 'package:boghrat_flutter/Services/User/RegisterV2Service.dart';
import 'package:flutter/cupertino.dart';
import 'package:get/get.dart';
import 'package:persian_number_utility/persian_number_utility.dart';

class RegisterV2Controller extends BaseController {
  RegisterV2Service registerV2Service = new RegisterV2Service();

  late RegisterV2DS registerV2DS = new RegisterV2DS();
  late SendCodeDS sendCodeDS = new SendCodeDS();

  var firstNameController = TextEditingController().obs;
  var lastNameController = TextEditingController().obs;
  var genderController = TextEditingController().obs;

  var registerDS;

  RegisterV2Controller(this.registerDS);

  @override
  void onInit() {
    super.onInit();
    genderController.value.text = "";
    firstNameController.value.text = "";
    lastNameController.value.text = "";
  }

  @override
  void onClose() {
    super.onClose();
  }

  void getModel() {
    registerV2DS.setFirstName =
        firstNameController.value.text.replaceAll(" ", "").toEnglishDigit();
    registerV2DS.setLastName =
        lastNameController.value.text.replaceAll(" ", "").toEnglishDigit();
    registerV2DS.setTypeId = "6";
    registerV2DS.setNationalCodeIsValid = "true";
    registerV2DS.setGenderId =
        genderController.value.text.replaceAll(" ", "").toEnglishDigit();
    registerV2DS.setNationalCode =
        registerDS.nationalCode.toString().replaceAll(" ", "").toEnglishDigit();
    registerV2DS.setPhoneNumber =
        registerDS.phoneNumber.toString().replaceAll(" ", "").toEnglishDigit();
  }

  void checkIsValid() {
    getModel();
    // print(registerV2DS.toJson());
    isValid(registerV2Service.checkIsValid(registerV2DS).flag);
  }

  Future registerV2(context) async {
    getModel();
    isLoading(true);
    try {
      var response = await registerV2Service.registerV2(registerV2DS);
      print(response.flag);
      if (response.flag) {
        getModel();
        Get.to(
            () => OTPScreen(
                  registerDS: registerDS,
                  screenType: "Register",
                ),
            transition: Transition.upToDown);
      } else {
        Tools().showDialog(context, response.errors);
      }
    } finally {
      isLoading(false);
    }
  }
}

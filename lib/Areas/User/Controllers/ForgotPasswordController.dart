import 'package:boghrat_flutter/Areas/User/Models/ForgotPasswordModel.dart';
import 'package:boghrat_flutter/Config/AppConfig.dart';
import 'package:boghrat_flutter/Config/BaseController.dart';
import 'package:boghrat_flutter/Services/User/ForgotPasswordService.dart';
import 'package:flutter/cupertino.dart';
import 'package:get/get.dart';
import 'package:persian_number_utility/persian_number_utility.dart';

class ForgotPasswordController extends BaseController {
  ForgotPasswordService forgotPasswordService = new ForgotPasswordService();

  late ForgotPasswordDS forgotPasswordDS = new ForgotPasswordDS();

  var codeController = TextEditingController().obs;
  var passwordController = TextEditingController().obs;
  var confirmPasswordController = TextEditingController().obs;

  var phoneNumber;

  ForgotPasswordController(String this.phoneNumber);

  @override
  void onInit() {
    super.onInit();
    codeController.value.text = "";
    passwordController.value.text = "";
    confirmPasswordController.value.text = "";
  }

  @override
  void onClose() {
    super.onClose();
  }

  void getModel() {
    forgotPasswordDS.setPhoneNumber =
        phoneNumber.toString().replaceAll(" ", "").toEnglishDigit();
    forgotPasswordDS.setPhoneCode =
        codeController.value.text.replaceAll(" ", "").toEnglishDigit();
    forgotPasswordDS.setNewPassword = passwordController.value.text;
    forgotPasswordDS.setConfirmPassword = confirmPasswordController.value.text;
  }

  void checkIsValid() {
    getModel();
    isValid(forgotPasswordService.checkIsValid(forgotPasswordDS).flag);
  }

  Future forgotPassword(context) async {
    getModel();
    isLoading(true);
    try {
      var response =
          await forgotPasswordService.forgotPassword(forgotPasswordDS);
      if (response.flag) {
        Tools().showDialog(context, ["baba barikalla"]);
      } else {
        Tools().showDialog(context, response.errors);
      }
    } finally {
      isLoading(false);
    }
  }
}

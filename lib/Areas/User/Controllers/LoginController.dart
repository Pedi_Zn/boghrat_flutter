import 'package:boghrat_flutter/Areas/TabBar/Views/TabBarScreen.dart';
import 'package:boghrat_flutter/Areas/User/Models/LoginModel.dart';
import 'package:boghrat_flutter/Areas/User/Models/OTPModel.dart';
import 'package:boghrat_flutter/Areas/User/Views/ForgotPasswordScreen.dart';
import 'package:boghrat_flutter/Config/AppConfig.dart';
import 'package:boghrat_flutter/Config/BaseController.dart';
import 'package:boghrat_flutter/Localization/Localization.dart';
import 'package:boghrat_flutter/Services/User/LoginService.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:get_storage/get_storage.dart';
import 'package:persian_number_utility/persian_number_utility.dart';

class LoginController extends BaseController {
  LoginService loginService = new LoginService();

  final box = GetStorage();

  late LoginDS loginDS = new LoginDS();
  late SendCodeDS sendCodeDS = new SendCodeDS();

  var phoneController = TextEditingController().obs;
  var passwordController = TextEditingController().obs;

  @override
  void onInit() {
    super.onInit();
    phoneController.value.text = "";
    passwordController.value.text = "";
  }

  @override
  void onClose() {
    super.onClose();
  }

  void getModel() {
    loginDS.setUsername =
        phoneController.value.text.replaceAll(" ", "").toEnglishDigit();
    loginDS.setPassword = passwordController.value.text;
    loginDS.setGrantType = "password";
  }

  void getSendCodeModel() {
    sendCodeDS.setPhoneNumber =
        phoneController.value.text.replaceAll(" ", "").toEnglishDigit();
  }

  void checkIsValid() {
    getModel();
    isValid(loginService.checkIsValid(loginDS).flag);
  }

  Future login(context) async {
    getModel();
    isLoading(true);
    try {
      var response = await loginService.loginRequest(loginDS);
      if (response.flag) {
        box.write("Token",
            response.result.tokenType + " " + response.result.accessToken);
        await getUserInfo(context);
      } else {
        Tools().showDialog(
            context, [DemoLocalizations.of(context)!.trans("LoginError")]);
      }
    } finally {
      isLoading(false);
    }
  }

  Future getUserInfo(context) async {
    var response = await loginService.getUserInfo(box.read("Token"));
    if (response.flag) {
      box.write("UserInfo", response.result);
      Tools().showDialog(context, [
        DemoLocalizations.of(context)!.trans("Welcome") +
            " " +
            response.result.fullName
      ]);
      print(response.result.fullName);
      Get.to(TabBarScreen());
    }
  }

  Future forgotPassword(context) async {
    getSendCodeModel();
    isLoading(true);
    try {
      if (loginService.validatePhone(sendCodeDS.phoneNumber).flag) {
        var response = await loginService.sendVerificationCode(sendCodeDS);
        if (response.flag) {
          getModel();
          Get.to(
              () =>
                  ForgotPasswordScreen(phoneNumber: phoneController.value.text),
              transition: Transition.upToDown);
        } else {
          Tools().showDialog(context, response.errors);
        }
      } else {
        Tools().showDialog(
            context, [DemoLocalizations.of(context)!.trans("EmptyPhone")]);
      }
    } finally {
      isLoading(false);
    }
  }
}

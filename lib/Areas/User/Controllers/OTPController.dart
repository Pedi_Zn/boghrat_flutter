import 'dart:async';

import 'package:boghrat_flutter/Areas/User/Models/OTPModel.dart';
import 'package:boghrat_flutter/Areas/User/Models/RegisterModel.dart';
import 'package:boghrat_flutter/Config/AppConfig.dart';
import 'package:boghrat_flutter/Config/BaseController.dart';
import 'package:boghrat_flutter/Services/User/OTPService.dart';
import 'package:flutter/cupertino.dart';
import 'package:get/get.dart';
import 'package:persian_number_utility/persian_number_utility.dart';

class OTPController extends BaseController {
  OTPService otpService = new OTPService();

  late OTPDS otpDS = new OTPDS();
  late SendCodeDS sendCodeDS = new SendCodeDS();

  var resend = false.obs;
  var codeController = TextEditingController().obs;
  var counter = 180.obs;
  var registerDS;

  OTPController(RegisterDS this.registerDS);

  @override
  void onInit() {
    super.onInit();
    startTimer();
    codeController.value.text = "".toPersianDigit();
  }

  @override
  void onClose() {
    super.onClose();
  }

  void startTimer() {
    Timer timer;
    timer = Timer.periodic(
      Duration(seconds: 1),
      (timer) {
        if (counter > 0) {
          counter--;
        } else {
          resend(true);
          timer.cancel();
        }
      },
    );
  }

  void getModel() {
    otpDS.setPhoneNumber =
        registerDS.phoneNumber.toString().replaceAll(" ", "").toEnglishDigit();
    otpDS.setPassword =
        registerDS.nationalCode.toString().replaceAll(" ", "").toEnglishDigit();
    otpDS.setPhoneCode =
        codeController.value.text.replaceAll(" ", "").toEnglishDigit();
  }

  void getSendCodeModel() {
    sendCodeDS.setPhoneNumber =
        registerDS.phoneNumber.toString().replaceAll(" ", "").toEnglishDigit();
  }

  void checkIsValid() {
    getModel();
    isValid(otpService.checkIsValid(otpDS).flag);
  }

  void resendCode(context) async {
    getSendCodeModel();
    counter(180);
    resend(false);
    isLoading(true);
    try {
      var response = await otpService.resendCode(sendCodeDS);
      if (response.flag) {
        startTimer();
      } else {
        Tools().showDialog(context, response.errors);
      }
    } finally {
      isLoading(false);
    }
  }

  Future verifyCode(context) async {
    getSendCodeModel();
    isLoading(true);
    try {
      var response = await otpService.verifyCode(otpDS);
      if (response.flag) {
        Tools().showDialog(context, ["baba barikalla"]);
      } else {
        Tools().showDialog(context, response.errors);
      }
    } finally {
      isLoading(false);
    }
  }
}

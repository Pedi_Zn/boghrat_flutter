import 'package:boghrat_flutter/Areas/Doctors/Views/DoctorsScreen.dart';
import 'package:boghrat_flutter/Areas/TabBar/Views/TabBarScreen.dart';
import 'package:boghrat_flutter/Areas/User/Views/RegisterScreen.dart';
import 'package:boghrat_flutter/Localization/LocalizationDelegate.dart';
import 'package:boghrat_flutter/Config/AppConfig.dart' as config;
import 'package:boghrat_flutter/test_widget.dart';
import 'package:flutter/material.dart';
import 'package:flutter_localizations/flutter_localizations.dart';
import 'package:get/get_navigation/src/root/get_material_app.dart';
import 'package:get_storage/get_storage.dart';

void main() async {
  await GetStorage.init();
  runApp(MyApp());
}

class MyApp extends StatelessWidget {
  // This widget is the root of your application.
  @override
  Widget build(BuildContext context) {
    final localizationDel = new DemoLocalizationsDelegate();
    var localeFa = new Locale('fa', 'FA');
    var localeEn = new Locale('en', 'EN');

    return GetMaterialApp(
      title: 'Flutter Demo',
      debugShowCheckedModeBanner: false,
      supportedLocales: [localeFa, localeEn],
      locale: localeFa,
      localizationsDelegates: [
        localizationDel,
        GlobalCupertinoLocalizations.delegate,
        GlobalMaterialLocalizations.delegate,
        GlobalWidgetsLocalizations.delegate
      ],
      theme: ThemeData(
        fontFamily: 'IranSans',
        primarySwatch:
            config.Colors().createMaterialColor(config.Colors().accentColor(1)),
        textTheme: TextTheme(
          subtitle1: TextStyle(
              fontSize: 17.0,
              color: config.Colors().primaryColorLight(1),
              fontWeight: FontWeight.normal),
          subtitle2: TextStyle(
              fontSize: 15.0,
              color: config.Colors().accentColor(1),
              fontWeight: FontWeight.normal),
          bodyText1: TextStyle(
              fontSize: 12.0,
              color: config.Colors().brownColor(1),
              fontWeight: FontWeight.normal),
          headline1:
              TextStyle(fontSize: 17.0, color: config.Colors().blackColor(0.6)),
          headline2:
              TextStyle(fontSize: 14.0, color: config.Colors().blackColor(0.6)),
          headline3: TextStyle(
              fontSize: 17.0,
              color: config.Colors().primaryColorDark(1),
              fontWeight: FontWeight.bold),
          headline4: TextStyle(
            fontSize: 22.0,
            color: config.Colors().blackColor(1),
            fontWeight: FontWeight.bold,
          ),
          headline5: TextStyle(
            fontSize: 17.0,
            color: config.Colors().blackColor(0.7),
            fontWeight: FontWeight.bold,
          ),
          headline6: TextStyle(
            fontSize: 14.0,
            color: config.Colors().blackColor(1),
            fontWeight: FontWeight.bold,
          ),
        ),
      ),
      home: DoctorsScreen(),
    );
  }
}

/*
 *  1 : Informational responses (100–199)
 *  2 : Successful responses (200–299)
 *  3 : Redirects (300–399)
 *  4 : Client errors (400–499)
 *  5 : Server errors (500–599)
 */

class RequestException implements Exception {
  final String? message;
  final String? prefix;
  final String? url;

  RequestException([this.message, this.prefix, this.url]);

  String toString() {
    return "$prefix$message";
  }
}

class BadRequestException extends RequestException {
  BadRequestException([String? message, String? url])
      : super(message, 'Bad Request', url);
}

class UnAuthorizedException extends RequestException {
  UnAuthorizedException([String? message, String? url])
      : super(message, 'UnAuthorized request', url);
}

class PaymentRequiredException extends RequestException {
  PaymentRequiredException([String? message, String? url])
      : super(message, 'Payment Required for', url);
}

class ForbiddenException extends RequestException {
  ForbiddenException([String? message, String? url])
      : super(message, 'Forbidden request', url);
}

class NotFoundException extends RequestException {
  NotFoundException([String? message, String? url])
      : super(message, 'NotFound request', url);
}

class ConflictException extends RequestException {
  ConflictException([String? message, String? url])
      : super(message, 'Conflict request', url);
}

class InternalServerError extends RequestException {
  InternalServerError([String? message, String? url])
      : super(message, 'Internal Server Error from', url);
}

class ServiceUnavailableException extends RequestException {
  ServiceUnavailableException([String? message, String? url])
      : super(message, 'Service Unavailable from', url);
}

class FetchDataException extends RequestException {
  FetchDataException([String? message, String? url])
      : super(message, 'Unable to process', url);
}

class ApiNotRespondingException extends RequestException {
  ApiNotRespondingException([String? message, String? url])
      : super(message, 'Api not responded in time', url);
}

import 'package:boghrat_flutter/Apis/Search/SearchApis.dart';
import 'package:boghrat_flutter/Config/CRUDOperationDS.dart';

class SearchFilterService {
  SearchApis searchApis = new SearchApis();

  Future<CRUDOperationDS> advanceSearch(searchFilterDS) async {
    var crud = new CRUDOperationDS();
    var response = await searchApis.getDoctors(searchFilterDS.toJson());
    crud.result = response;

    return crud;
  }

  Future<CRUDOperationDS> getProvinces() async {
    var crud = new CRUDOperationDS();
    var response = await searchApis.getProvinces();
    crud.result = response;

    return crud;
  }

  Future<CRUDOperationDS> getExpertisesWithQuery(query) async {
    var crud = new CRUDOperationDS();
    var response = await searchApis.getExpertisesWithQuery(query);
    crud.result = response;

    return crud;
  }

  Future<CRUDOperationDS> getDoctorsWithQuert(query) async {
    var crud = new CRUDOperationDS();
    var response = await searchApis.getDoctorsWithQuert(query, "");
    crud.result = response;

    return crud;
  }
}

import 'package:boghrat_flutter/Apis/Search/SearchApis.dart';
import 'package:boghrat_flutter/Config/CRUDOperationDS.dart';
import 'package:persian_number_utility/persian_number_utility.dart';

class SearchService {
  SearchApis searchApis = new SearchApis();

  CRUDOperationDS checkDoctorsImage(doctorInfos) {
    var crud = new CRUDOperationDS();
    final String imageBaseUrl =
        "https://boghrat.com/api/document/Thumbnail/LoadThumb/";
    if (doctorInfos.img != null) {
      if (doctorInfos.img.length > 0) {
        crud.result = imageBaseUrl + doctorInfos.img;
      } else {
        crud.result = "https://boghrat.com/content/images/doc-ph-male.png";
      }
    } else {
      crud.addNewErrorMessage(
          "Id : " + doctorInfos.id.toString() + " Null Image");
    }

    return crud;
  }

  CRUDOperationDS checkCity(doctorInfos) {
    var crud = new CRUDOperationDS();

    if (doctorInfos.cities == null) {
      crud.addNewErrorMessage(
          "Id : " + doctorInfos.id.toString() + " Null Cities");
    }

    return crud;
  }

  CRUDOperationDS checkCityText(doctorInfos) {
    var crud = new CRUDOperationDS();

    if (checkCity(doctorInfos).flag) {
      if (doctorInfos.cities.length > 0) {
        crud.result = doctorInfos.cities[0].title;
      } else {
        crud.result = "تهران";
      }
    } else {
      crud.flag = checkCity(doctorInfos).flag;
      crud.errors = checkCity(doctorInfos).errors;
    }

    return crud;
  }

  CRUDOperationDS checkVisitType(doctorInfos) {
    var crud = new CRUDOperationDS();
    if (doctorInfos.visitTypes == null) {
      crud.addNewErrorMessage(
          "Id : " + doctorInfos.id.toString() + " Null Visit Types");
    }

    return crud;
  }

  CRUDOperationDS checkSpecialties(doctorInfos) {
    var crud = new CRUDOperationDS();

    if (doctorInfos.specialties == null) {
      crud.addNewErrorMessage(
          "Id : " + doctorInfos.id.toString() + " Null Specialties");
    }

    return crud;
  }

  CRUDOperationDS checkSpecialtiesText(doctorInfos) {
    var crud = new CRUDOperationDS();

    if (checkSpecialties(doctorInfos).flag) {
      if (doctorInfos.specialties.length > 0) {
        crud.result =
            doctorInfos.specialties[0].key + doctorInfos.specialties[0].value;
      } else {
        crud.result = "تخصصی ثبت نشده";
      }
    } else {
      crud.flag = checkSpecialties(doctorInfos).flag;
      crud.errors = checkSpecialties(doctorInfos).errors;
    }

    return crud;
  }

  CRUDOperationDS checkButton(doctorInfos) {
    var crud = new CRUDOperationDS();

    if (checkVisitType(doctorInfos).flag) {
      if (doctorInfos.visitTypes.contains(3) &&
          doctorInfos.visitTypes.contains(4)) {
        crud.result = false;
      } else {
        crud.result = true;
      }
    } else {
      crud.flag = checkVisitType(doctorInfos).flag;
      crud.errors = checkVisitType(doctorInfos).errors;
    }

    return crud;
  }

  CRUDOperationDS doctorCodeStyle(doctorInfos) {
    var crud = new CRUDOperationDS();

    if (doctorInfos.doctorCode != null) {
      if (doctorInfos.doctorCode.toString().contains("-")) {
        crud.result = doctorInfos.doctorCode.toString().split("-")[0] +
            "-" +
            doctorInfos.doctorCode.toString().split("-")[1].toPersianDigit();
      } else {
        crud.result = doctorInfos.doctorCode.toString().toPersianDigit();
      }
    } else {
      crud.addNewErrorMessage(
          "Id : " + doctorInfos.id.toString() + " Null DoctorCode");
    }

    return crud;
  }

  Future<CRUDOperationDS> getDoctors(searchDS) async {
    var crud = new CRUDOperationDS();

    var getDoctorsResponse = await searchApis.getDoctors(searchDS.toJson());
    crud.result = getDoctorsResponse;

    return crud;
  }
}

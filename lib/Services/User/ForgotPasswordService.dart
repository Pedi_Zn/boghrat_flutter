import 'package:boghrat_flutter/Apis/User/UserApis.dart';
import 'package:boghrat_flutter/Config/CRUDOperationDS.dart';

class ForgotPasswordService {
  UserApis userApis = new UserApis();

  CRUDOperationDS checkIsValid(forgotPasswordDS) {
    var codeCrud = validateCode(forgotPasswordDS.phoneCode);
    var passwordCrud = validatePassword(
        forgotPasswordDS.newPassword, forgotPasswordDS.confirmPassword);
    var crudResult = new CRUDOperationDS();

    crudResult.addErrorMessageRange(codeCrud);
    crudResult.addErrorMessageRange(passwordCrud);

    return crudResult;
  }

  CRUDOperationDS validateCode(code) {
    var crud = new CRUDOperationDS();
    if (code.length != 4) {
      crud.addNewErrorMessage("Phone Length is less than 4");
    }
    return crud;
  }

  CRUDOperationDS validatePassword(password, confirmPassword) {
    var crud = new CRUDOperationDS();
    if (password != confirmPassword) {
      crud.addNewErrorMessage("Password and Confirm Password does not match");
    }
    return crud;
  }

  Future<CRUDOperationDS> forgotPassword(forgotPasswordDS) async {
    var crud = new CRUDOperationDS();
    var forgotPasswordResponse =
        await userApis.forgotPassword(forgotPasswordDS.toJson());
    if (forgotPasswordResponse.errorMessages!.length > 0) {
      crud.addErrors(forgotPasswordResponse.errorMessages!);
    }

    return crud;
  }
}

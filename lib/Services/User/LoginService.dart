import 'dart:convert';

import 'package:boghrat_flutter/Config/CRUDOperationDS.dart';
import 'package:boghrat_flutter/Apis/User/UserApis.dart';

class LoginService {
  UserApis userApis = new UserApis();

  CRUDOperationDS checkIsValid(loginDS) {
    var phoneCrud = validatePhone(loginDS.userName);
    var passwordCrud = validatePassword(loginDS.password);
    var crudResult = new CRUDOperationDS();

    crudResult.addErrorMessageRange(phoneCrud);
    crudResult.addErrorMessageRange(passwordCrud);

    return crudResult;
  }

  CRUDOperationDS validatePhone(phone) {
    var crud = new CRUDOperationDS();
    if (phone.length != 11) {
      crud.addNewErrorMessage("Phone Length is less than 11");
    }
    return crud;
  }

  CRUDOperationDS validatePassword(password) {
    var crud = new CRUDOperationDS();
    if (password.length < 5) {
      crud.addNewErrorMessage("Password Length is less than 5");
    }
    return crud;
  }

  Future<CRUDOperationDS> loginRequest(loginDS) async {
    var crud = new CRUDOperationDS();
    var loginResponse = await userApis.login(loginDS.toJson());
    if (loginResponse.error != null) {
      crud.addNewErrorMessage(loginResponse.error.toString());
    }
    crud.result = loginResponse;

    return crud;
  }

  Future<CRUDOperationDS> getUserInfo(token) async {
    var crud = new CRUDOperationDS();
    var loginResponse = await userApis.getUserInfo(token);
    crud.result = loginResponse;

    return crud;
  }

  Future<CRUDOperationDS> sendVerificationCode(sendCodeDS) async {
    var crud = new CRUDOperationDS();
    var response = await userApis.sendVerificationCode(sendCodeDS.toJson());
    if (response.errorMessages!.length > 0) {
      crud.addErrors(response.errorMessages!);
    }
    crud.result = response;

    return crud;
  }
}

import 'package:boghrat_flutter/Apis/User/UserApis.dart';
import 'package:boghrat_flutter/Config/CRUDOperationDS.dart';

class RegisterService {
  UserApis userApis = new UserApis();

  CRUDOperationDS checkIsValid(registerDS) {
    var phoneCrud = validatePhone(registerDS.phoneNumber);
    var nationalCodeCrud = validateNationalCode(registerDS.nationalCode);
    var crudResult = new CRUDOperationDS();

    crudResult.addErrorMessageRange(phoneCrud);
    crudResult.addErrorMessageRange(nationalCodeCrud);

    return crudResult;
  }

  CRUDOperationDS validatePhone(phone) {
    var crud = new CRUDOperationDS();
    if (phone.length != 11) {
      crud.addNewErrorMessage("Phone Length is less than 11");
    }
    return crud;
  }

  CRUDOperationDS validateNationalCode(nationalCode) {
    var crud = new CRUDOperationDS();
    if (nationalCode.length != 10) {
      crud.addNewErrorMessage("National Code Length is less than 10");
    }
    return crud;
  }

  Future<CRUDOperationDS> register(registerDS) async {
    var crud = new CRUDOperationDS();
    var response = await userApis.register(registerDS.toJson());
    if (response.errorMessages!.length > 0 &&
        response.errorMessages![0] == "506") {
      crud.addErrors([]);
      crud.flag = false;
    } else if (response.errorMessages!.length > 0) {
      crud.addErrors(response.errorMessages!);
    }
    print(crud.errors.toString());

    return crud;
  }

  Future<CRUDOperationDS> registerV2(registerV2DS) async {
    var crud = new CRUDOperationDS();
    var response = await userApis.registerV2(registerV2DS.toJson());
    if (response.errorMessages!.length > 0) {
      crud.addErrors(response.errorMessages!);
    }

    return crud;
  }
}

import 'dart:async';

import 'package:boghrat_flutter/Apis/User/UserApis.dart';
import 'package:boghrat_flutter/Config/CRUDOperationDS.dart';

class OTPService {
  UserApis userApis = new UserApis();

  CRUDOperationDS checkIsValid(otpDS) {
    var codeCrud = validateCode(otpDS.phoneCode);
    var crudResult = new CRUDOperationDS();

    crudResult.addErrorMessageRange(codeCrud);

    return crudResult;
  }

  CRUDOperationDS validateCode(phoneCode) {
    var crud = new CRUDOperationDS();
    if (phoneCode.length != 4) {
      crud.addNewErrorMessage("Phone Code Length is less than 4");
    }
    return crud;
  }

  Future<CRUDOperationDS> resendCode(sendCodeDS) async {
    var crud = new CRUDOperationDS();
    var response = await userApis.resendVerificationCode(sendCodeDS.toJson());
    if (response.errorMessages!.length > 0) {
      crud.addErrors(response.errorMessages!);
    }
    crud.result = response;

    return crud;
  }

  Future<CRUDOperationDS> verifyCode(otpDS) async {
    var crud = new CRUDOperationDS();
    var sendVerificationCodeResponse =
        await userApis.phoneVerification(otpDS.toJson());
    if (sendVerificationCodeResponse.errorMessages!.length > 0) {
      crud.addErrors(sendVerificationCodeResponse.errorMessages!);
    }
    crud.result = sendVerificationCodeResponse;

    return crud;
  }

  // Future<CRUDOperationDS> registerUser(token) async {
  //   var crud = new CRUDOperationDS();
  //   var registerResponse = await userApis.registerUser(token);
  //   crud.result = registerResponse;

  //   return crud;
  // }
}

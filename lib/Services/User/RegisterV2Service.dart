import 'dart:convert';

import 'package:boghrat_flutter/Apis/User/UserApis.dart';
import 'package:boghrat_flutter/Config/CRUDOperationDS.dart';

class RegisterV2Service {
  UserApis userApis = new UserApis();

  CRUDOperationDS checkIsValid(registerV2DS) {
    var genderCrud = validateGender(registerV2DS.genderId);
    var firstNameCrud = validateFirstName(registerV2DS.firstName);
    var lastNameCrud = validateLastName(registerV2DS.lastName);
    var crudResult = new CRUDOperationDS();

    crudResult.addErrorMessageRange(genderCrud);
    crudResult.addErrorMessageRange(firstNameCrud);
    crudResult.addErrorMessageRange(lastNameCrud);

    return crudResult;
  }

  CRUDOperationDS validateGender(gender) {
    var crud = new CRUDOperationDS();
    if (gender == "") {
      crud.addNewErrorMessage("Gender is Empty");
    }
    return crud;
  }

  CRUDOperationDS validateFirstName(firstName) {
    var crud = new CRUDOperationDS();
    if (firstName.length < 3) {
      crud.addNewErrorMessage("FirstName Length is less than 2");
    }
    return crud;
  }

  CRUDOperationDS validateLastName(lastName) {
    var crud = new CRUDOperationDS();
    if (lastName.length < 3) {
      crud.addNewErrorMessage("LastName Code Length is less than 2");
    }
    return crud;
  }

  Future<CRUDOperationDS> registerV2(registerV2DS) async {
    print("++++ " + registerV2DS.toJson().toString());
    dynamic temp = {
      "nationalCodeIsValid": true,
      "firstName": "یی",
      "lastName": "یی",
      "phoneNumber": "09122222222",
      "nationalCode": "0015700437",
      "confirmPassword": "0015700437",
      "password": "0015700437",
      "genderId": "2",
      "typeId": 6,
      "country": {"id": 1},
    };
    var crud = new CRUDOperationDS();
    var response = await userApis.registerV2(registerV2DS.toJson());
    if (response.errorMessages!.length > 0) {
      crud.addErrors(response.errorMessages!);
    }

    return crud;
  }
}

import 'package:flash/flash.dart';
import 'package:flutter/material.dart';

class AppConfig {
  late BuildContext _context;
  late double _height;
  late double _width;
  late double _heightPadding;
  late double _widthPadding;
  late double _salam;

  AppConfig(_context) {
    this._context = _context;
    MediaQueryData _queryData = MediaQuery.of(this._context);
    _height = _queryData.size.height / 100.0;
    _width = _queryData.size.width / 100.0;
    _heightPadding = (_queryData.padding.top + _queryData.padding.bottom) / 100;
    _widthPadding =
        (_queryData.padding.left + _queryData.padding.right) / 100.0;
  }

  double appHeight(double v) {
    return _height * v;
  }

  double appWidth(double v) {
    return _width * v;
  }

  double appVerticalPadding(double v) {
    return _heightPadding * v;
  }

  double appHorizontalPadding(double v) {
    return _widthPadding * v;
  }
}

class Colors {
  Color _primaryColor = fromHex("#ffd99d");
  Color _primaryColorDark = fromHex("#c3a127");
  Color _primaryColorLight = fromHex("#ffffff");
  Color _accentColor = fromHex("#711033");

  Color _infoColor = fromHex('#1cbbb4');
  Color _successColor = fromHex('#00b300');
  Color _warningColor = fromHex('#f7941d');
  Color _dangerColor = fromHex('#ed1c24');

  Color _blackColor = fromHex("#000000");
  Color _greenColor = fromHex('#4caf50');
  Color _cyanColor = fromHex('#00bcd4');
  Color _orangeColor = fromHex('#C35300');
  Color _brownColor = fromHex('#8e837a');
  Color _blueColor = fromHex("#167f9e");
  Color _lightBlueColor = fromHex("#2196F3");
  Color _redColor = fromHex("#E91E63");

  static Color fromHex(String hexString) {
    final buffer = StringBuffer();
    if (hexString.length == 6 || hexString.length == 7) buffer.write('ff');
    buffer.write(hexString.replaceFirst('#', ''));
    return Color(int.parse(buffer.toString(), radix: 16));
  }

  MaterialColor createMaterialColor(Color color) {
    List strengths = <double>[.05];
    final swatch = <int, Color>{};
    final int r = color.red, g = color.green, b = color.blue;

    for (int i = 1; i < 10; i++) {
      strengths.add(0.1 * i);
    }
    strengths.forEach((strength) {
      final double ds = 0.5 - strength;
      swatch[(strength * 1000).round()] = Color.fromRGBO(
        r + ((ds < 0 ? r : (255 - r)) * ds).round(),
        g + ((ds < 0 ? g : (255 - g)) * ds).round(),
        b + ((ds < 0 ? b : (255 - b)) * ds).round(),
        1,
      );
    });
    return MaterialColor(color.value, swatch);
  }

  Color blackColor(double opacity) {
    return this._blackColor.withOpacity(opacity);
  }

  Color greenColor(double opacity) {
    return this._greenColor.withOpacity(opacity);
  }

  Color cyanColor(double opacity) {
    return this._cyanColor.withOpacity(opacity);
  }

  Color brownColor(double opacity) {
    return this._brownColor.withOpacity(opacity);
  }

  Color blueColor(double opacity) {
    return this._blueColor.withOpacity(opacity);
  }

  Color lightBlueColor(double opacity) {
    return this._lightBlueColor.withOpacity(opacity);
  }

  Color redColor(double opacity) {
    return this._redColor.withOpacity(opacity);
  }

  Color infoColor(double opacity) {
    return this._infoColor.withOpacity(opacity);
  }

  Color successColor(double opacity) {
    return this._successColor.withOpacity(opacity);
  }

  Color warningColor(double opacity) {
    return this._warningColor.withOpacity(opacity);
  }

  Color dangerColor(double opacity) {
    return this._dangerColor.withOpacity(opacity);
  }

  Color primaryColor(double opacity) {
    return this._primaryColor.withOpacity(opacity);
  }

  Color primaryColorLight(double opacity) {
    return this._primaryColorLight.withOpacity(opacity);
  }

  Color primaryColorDark(double opacity) {
    return this._primaryColorDark.withOpacity(opacity);
  }

  Color accentColor(double opacity) {
    return this._accentColor.withOpacity(opacity);
  }
}

class Tools {
  void showDialog(BuildContext context, List<dynamic> text) {
    showFlash(
      context: context,
      duration: Duration(milliseconds: 2500),
      builder: (context, controller) {
        return Flash(
          controller: controller,
          barrierDismissible: false,
          behavior: FlashBehavior.floating,
          position: FlashPosition.top,
          margin: EdgeInsets.fromLTRB(45.0, 20.0, 45.0, 20.0),
          borderRadius: BorderRadius.circular(10.0),
          boxShadows: kElevationToShadow[4],
          backgroundColor: Theme.of(context).accentColor,
          horizontalDismissDirection: HorizontalDismissDirection.horizontal,
          child: Wrap(
            alignment: WrapAlignment.start,
            children: [
              Container(
                padding: EdgeInsets.fromLTRB(10, 10, 10, 10),
                child: Text(
                  errorText(text),
                  textAlign: TextAlign.center,
                  style: TextStyle(
                    color: Colors().primaryColorLight(1),
                    fontSize: 14.0,
                  ),
                ),
              ),
            ],
          ),
        );
      },
    );
  }

  String errorText(textList) {
    var temp = "";
    for (var text in textList) {
      temp += text + " ";
    }
    return temp;
  }
}

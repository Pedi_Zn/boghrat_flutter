import 'dart:async';
import 'dart:convert';
import 'dart:io';

import 'package:boghrat_flutter/Services/RequestException.dart';
import 'package:http/http.dart' as http;

class BaseClient {
  static const int TIME_OUT_DURATION = 20;

  static Map<String, String> headers = {};

  void setHeader(String key, val) {
    headers[key] = val;
  }

  //GET
  Future<dynamic> get(String url) async {
    var uri = Uri.parse(url);
    try {
      var response = await http
          .get(uri, headers: headers)
          .timeout(Duration(seconds: TIME_OUT_DURATION));
      return _processResponse(response);
    } on SocketException {
      throw FetchDataException('No Internet connection', uri.toString());
    } on TimeoutException {
      throw ApiNotRespondingException(
          'API not responded in time', uri.toString());
    }
  }

  //POST
  Future<dynamic> post(String url, dynamic payload) async {
    var uri = Uri.parse(url);
    try {
      var response = await http
          // .post(uri, body: jsonEncode(payload), headers: headers)
          .post(uri, body: payload, headers: headers)
          .timeout(Duration(seconds: TIME_OUT_DURATION));
      return _processResponse(response);
    } on SocketException {
      throw FetchDataException('No Internet connection', uri.toString());
    } on TimeoutException {
      throw ApiNotRespondingException(
          'API not responded in time', uri.toString());
    }
  }

  //PUT
  Future<dynamic> put(String baseUrl, String api, dynamic payload) async {
    var uri = Uri.parse(baseUrl + api);
    try {
      var response = await http
          .put(uri, body: payload, headers: headers)
          .timeout(Duration(seconds: TIME_OUT_DURATION));
      return _processResponse(response);
    } on SocketException {
      throw FetchDataException('No Internet connection', uri.toString());
    } on TimeoutException {
      throw ApiNotRespondingException(
          'API not responded in time', uri.toString());
    }
  }

  void handleError(error) {
    if (error is BadRequestException) {
      var message = error.message;
      print(message);
    } else if (error is FetchDataException) {
      var message = error.message;
      print(message);
    } else if (error is ApiNotRespondingException) {
      print("API not responded in time");
    }
  }

  dynamic _processResponse(http.Response response) {
    switch (response.statusCode) {
      case 200:
        var responseJson = utf8.decode(response.bodyBytes);
        return responseJson;
      case 400:
        throw BadRequestException(
            utf8.decode(response.bodyBytes), response.request!.url.toString());
      case 401:
        throw UnAuthorizedException(
            utf8.decode(response.bodyBytes), response.request!.url.toString());
      case 402:
        throw PaymentRequiredException(
            utf8.decode(response.bodyBytes), response.request!.url.toString());
      case 403:
        throw ForbiddenException(
            utf8.decode(response.bodyBytes), response.request!.url.toString());
      case 404:
        throw NotFoundException(
            utf8.decode(response.bodyBytes), response.request!.url.toString());
      case 500:
        throw InternalServerError(
            utf8.decode(response.bodyBytes), response.request!.url.toString());
      case 503:
        throw ServiceUnavailableException(
            utf8.decode(response.bodyBytes), response.request!.url.toString());
      default:
        throw FetchDataException(
            'Error occured with code : ${response.statusCode}',
            response.request!.url.toString());
    }
  }
}

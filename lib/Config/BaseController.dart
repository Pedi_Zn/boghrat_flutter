import 'package:get/get.dart';
export 'package:get/get.dart';

class BaseController extends GetxController {
  var isLoading = false.obs;
  var isValid = false.obs;

  // set setLoading(bool value) => isLoading(value);
  // get getLoading => isLoading.value;

  @override
  void onInit() {
    super.onInit();
  }

  @override
  void onClose() {
    super.onClose();
  }

  @override
  void onReady() {
    super.onReady();
  }
}

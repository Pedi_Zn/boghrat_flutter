class CRUDOperationDS {
  CRUDOperationDS({
    errors,
    result,
  });
  bool flag = true;
  List<dynamic> errors = [];
  dynamic result = {};

  addNewErrorMessage(String errorMessage) {
    errors.add(errorMessage);
    flag = false;
  }

  addErrors(List<dynamic> errorList) {
    for (var err in errorList) {
      this.addNewErrorMessage(err);
    }
  }

  addErrorMessageRange(CRUDOperationDS crudOperationDS) {
    for (var err in crudOperationDS.errors) {
      this.addNewErrorMessage(err);
    }
  }
}

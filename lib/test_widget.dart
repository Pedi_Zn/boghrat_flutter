import 'package:carousel_slider/carousel_slider.dart';
import 'package:flutter/material.dart';

final List<String> imgList = [
  'https://boghrat.com/api/document/Thumbnail/LoadThumb/0353a112-12d8-467f-b43b-8f6b2fa130a0.jpeg',
  'https://boghrat.com/api/document/Thumbnail/LoadThumb/55ddc1a5-d995-43f4-989c-9dd0bc31441d.jpeg',
  'https://boghrat.com/api/document/Thumbnail/LoadThumb/1cd21c68-121b-45fc-b7cd-ee375a50904b.jpeg',
  'https://boghrat.com/api/document/Thumbnail/LoadThumb/0353a112-12d8-467f-b43b-8f6b2fa130a0.jpeg',
  'https://boghrat.com/api/document/Thumbnail/LoadThumb/55ddc1a5-d995-43f4-989c-9dd0bc31441d.jpeg',
  'https://boghrat.com/api/document/Thumbnail/LoadThumb/1cd21c68-121b-45fc-b7cd-ee375a50904b.jpeg',
];

final themeMode = ValueNotifier(1);

final List<Widget> imageSliders = imgList
    .map(
      (item) => Container(
        child: Container(
          margin: EdgeInsets.all(5.0),
          child: ClipRRect(
            borderRadius: BorderRadius.all(Radius.circular(15.0)),
            child: Stack(
              children: <Widget>[
                Image.network(
                  item,
                  fit: BoxFit.fill,
                  width: 1000.0,
                  height: 160,
                ),
              ],
            ),
          ),
        ),
      ),
    )
    .toList();

class CarouselWithIndicator extends StatefulWidget {
  @override
  State<StatefulWidget> createState() {
    return _CarouselWithIndicatorState();
  }
}

class _CarouselWithIndicatorState extends State<CarouselWithIndicator> {
  int _current = 0;
  final CarouselController _controller = CarouselController();

  @override
  Widget build(BuildContext context) {
    return Container(
      width: MediaQuery.of(context).size.width,
      child: Column(
        mainAxisSize: MainAxisSize.min,
        children: [
          CarouselSlider(
            items: imageSliders,
            carouselController: _controller,
            options: CarouselOptions(
              autoPlay: true,
              enlargeCenterPage: true,
              aspectRatio: 2.5,
              viewportFraction: 0.6,
              onPageChanged: (index, reason) {
                setState(
                  () {
                    _current = index;
                  },
                );
              },
            ),
          ),
          SizedBox(height: 15),
          Row(
            mainAxisAlignment: MainAxisAlignment.center,
            mainAxisSize: MainAxisSize.min,
            children: imgList.asMap().entries.map(
              (entry) {
                return GestureDetector(
                  onTap: () => _controller.animateToPage(entry.key),
                  child: Container(
                    width: 12.0,
                    height: 12.0,
                    margin:
                        EdgeInsets.symmetric(vertical: 8.0, horizontal: 4.0),
                    decoration: BoxDecoration(
                        shape: BoxShape.circle,
                        color: (Theme.of(context).brightness == Brightness.dark
                                ? Colors.white
                                : Colors.black)
                            .withOpacity(_current == entry.key ? 0.9 : 0.4)),
                  ),
                );
              },
            ).toList(),
          ),
        ],
      ),
    );
  }
}








// import 'package:flutter/cupertino.dart';
// import 'package:flutter/material.dart';
// import 'package:flutter/rendering.dart';
// import 'package:persian_datetime_picker/persian_datetime_picker.dart';

// final ThemeData androidTheme = new ThemeData(
//   fontFamily: 'Dana',
// );

// void main() => runApp(new MyApp());

// class MyApp extends StatelessWidget {
//   // This widget is the root of your application.
//   @override
//   Widget build(BuildContext context) {
//     return new MaterialApp(
//       title: 'Flutter Demo',
//       theme: androidTheme,
//       home: new MyHomePage(title: 'دیت تایم پیکر فارسی'),
//     );
//   }
// }

// class MyHomePage extends StatefulWidget {
//   MyHomePage({Key key, this.title}) : super(key: key);

//   final String title;

//   @override
//   _MyHomePageState createState() => new _MyHomePageState();
// }

// class _MyHomePageState extends State<MyHomePage> {
//   String label;

//   String selectedDate = Jalali.now().toJalaliDateTime();

//   @override
//   void initState() {
//     super.initState();
//     label = 'انتخاب تاریخ زمان';
//   }

//   @override
//   Widget build(BuildContext context) {
//     return new Directionality(
//       textDirection: TextDirection.rtl,
//       child: Scaffold(
//         backgroundColor: Colors.white,
//         appBar: new AppBar(
//           title: new Text(
//             widget.title,
//             textAlign: TextAlign.center,
//             style: TextStyle(color: Colors.black),
//           ),
//           centerTitle: true,
//           backgroundColor: Colors.transparent,
//           elevation: 0,
//         ),
//         body: Container(
//           decoration: BoxDecoration(
//             gradient: LinearGradient(
//               begin: Alignment.topCenter,
//               colors: [Colors.white, Color(0xffE4F5F9)],
//             ),
//           ),
//           child: SingleChildScrollView(
//             padding: EdgeInsets.symmetric(horizontal: 10),
//             physics: BouncingScrollPhysics(),
//             child: Row(
//               crossAxisAlignment: CrossAxisAlignment.start,
//               children: [
//                 Expanded(
//                   child: Column(
//                     children: [
//                       imageButton(
//                         onTap: () async {
//                           Jalali? picked = await showPersianDatePicker(
//                             context: context,
//                             initialDate: Jalali.now(),
//                             firstDate: Jalali(1385, 8),
//                             lastDate: Jalali(1450, 9),
//                           );
//                           if (picked != null && picked != selectedDate) {
//                             setState(() {
//                               label = picked.toJalaliDateTime();
//                             });
//                           }
//                         },
//                         image: "08",
//                       ),
//                       imageButton(
//                         onTap: () async {
//                           Jalali? pickedDate =
//                               await showModalBottomSheet<Jalali>(
//                             context: context,
//                             builder: (context) {
//                               Jalali tempPickedDate;
//                               return Container(
//                                 height: 250,
//                                 child: Column(
//                                   children: <Widget>[
//                                     Container(
//                                       child: Row(
//                                         mainAxisAlignment:
//                                             MainAxisAlignment.spaceBetween,
//                                         children: <Widget>[
//                                           CupertinoButton(
//                                             child: Text(
//                                               'لغو',
//                                               style: TextStyle(
//                                                 fontFamily: 'Dana',
//                                               ),
//                                             ),
//                                             onPressed: () {
//                                               Navigator.of(context).pop();
//                                             },
//                                           ),
//                                           CupertinoButton(
//                                             child: Text(
//                                               'تایید',
//                                               style: TextStyle(
//                                                 fontFamily: 'Dana',
//                                               ),
//                                             ),
//                                             onPressed: () {
//                                               Navigator.of(context).pop(
//                                                   tempPickedDate ??
//                                                       Jalali.now());
//                                             },
//                                           ),
//                                         ],
//                                       ),
//                                     ),
//                                     Divider(
//                                       height: 0,
//                                       thickness: 1,
//                                     ),
//                                     Expanded(
//                                       child: Container(
//                                         child: CupertinoTheme(
//                                           data: CupertinoThemeData(
//                                             textTheme: CupertinoTextThemeData(
//                                               dateTimePickerTextStyle:
//                                                   TextStyle(fontFamily: "Dana"),
//                                             ),
//                                           ),
//                                           child: PCupertinoDatePicker(
//                                             mode: PCupertinoDatePickerMode
//                                                 .dateAndTime,
//                                             onDateTimeChanged:
//                                                 (Jalali dateTime) {
//                                               tempPickedDate = dateTime;
//                                             },
//                                           ),
//                                         ),
//                                       ),
//                                     ),
//                                   ],
//                                 ),
//                               );
//                             },
//                           );

//                           if (pickedDate != null) {
//                             setState(() {
//                               label = "${pickedDate.toDateTime()}";
//                             });
//                           }
//                         },
//                         image: "07",
//                       ),
//                       imageButton(
//                         onTap: () async {
//                           var picked = await showPersianTimePicker(
//                             context: context,
//                             initialTime: TimeOfDay.now(),
//                             initialEntryMode: PTimePickerEntryMode.input,
//                             builder: (BuildContext context, Widget child) {
//                               return Directionality(
//                                 textDirection: TextDirection.rtl,
//                                 child: child,
//                               );
//                             },
//                           );
//                           setState(() {
//                             if (picked != null)
//                               label = picked.persianFormat(context);
//                           });
//                         },
//                         image: "09",
//                       ),
//                       imageButton(
//                         onTap: () async {
//                           Jalali? pickedDate =
//                               await showModalBottomSheet<Jalali>(
//                             context: context,
//                             builder: (context) {
//                               Jalali tempPickedDate;
//                               return Container(
//                                 height: 250,
//                                 child: Column(
//                                   children: <Widget>[
//                                     Container(
//                                       child: Row(
//                                         mainAxisAlignment:
//                                             MainAxisAlignment.spaceBetween,
//                                         children: <Widget>[
//                                           CupertinoButton(
//                                             child: Text(
//                                               'لغو',
//                                               style: TextStyle(
//                                                 fontFamily: 'Dana',
//                                               ),
//                                             ),
//                                             onPressed: () {
//                                               Navigator.of(context).pop();
//                                             },
//                                           ),
//                                           CupertinoButton(
//                                             child: Text(
//                                               'تایید',
//                                               style: TextStyle(
//                                                 fontFamily: 'Dana',
//                                               ),
//                                             ),
//                                             onPressed: () {
//                                               print(tempPickedDate);

//                                               Navigator.of(context)
//                                                   .pop(tempPickedDate);
//                                             },
//                                           ),
//                                         ],
//                                       ),
//                                     ),
//                                     Divider(
//                                       height: 0,
//                                       thickness: 1,
//                                     ),
//                                     Expanded(
//                                       child: Container(
//                                         child: CupertinoTheme(
//                                           data: CupertinoThemeData(
//                                             textTheme: CupertinoTextThemeData(
//                                               dateTimePickerTextStyle:
//                                                   TextStyle(fontFamily: "Dana"),
//                                             ),
//                                           ),
//                                           child: PCupertinoDatePicker(
//                                             mode: PCupertinoDatePickerMode.time,
//                                             onDateTimeChanged:
//                                                 (Jalali dateTime) {
//                                               tempPickedDate = dateTime;
//                                             },
//                                           ),
//                                         ),
//                                       ),
//                                     ),
//                                   ],
//                                 ),
//                               );
//                             },
//                           );

//                           if (pickedDate != null) {
//                             setState(() {
//                               label = "${pickedDate.toJalaliDateTime()}";
//                             });
//                           }
//                         },
//                         image: "05",
//                       ),
//                     ],
//                   ),
//                 ),
//                 Expanded(
//                   child: Column(
//                     children: [
//                       imageButton(
//                         onTap: () async {
//                           var picked = await showPersianDateRangePicker(
//                             context: context,
//                             initialDateRange: JalaliRange(
//                               start: Jalali(1400, 1, 2),
//                               end: Jalali(1400, 1, 10),
//                             ),
//                             firstDate: Jalali(1385, 8),
//                             lastDate: Jalali(1450, 9),
//                           );
//                           setState(() {
//                             label =
//                                 "${picked?.start?.toJalaliDateTime() ?? ""} ${picked?.end?.toJalaliDateTime() ?? ""}";
//                           });
//                         },
//                         image: "03",
//                       ),
//                       imageButton(
//                         onTap: () async {
//                           var picked = await showPersianTimePicker(
//                             context: context,
//                             initialTime: TimeOfDay.now(),
//                             builder: (BuildContext context, Widget child) {
//                               return Directionality(
//                                 textDirection: TextDirection.rtl,
//                                 child: child,
//                               );
//                             },
//                           );
//                           setState(() {
//                             if (picked != null)
//                               label = picked.persianFormat(context);
//                           });
//                         },
//                         image: "04",
//                       ),
//                       imageButton(
//                         onTap: () async {
//                           var picked = await showPersianDateRangePicker(
//                             context: context,
//                             initialEntryMode: PDatePickerEntryMode.input,
//                             initialDateRange: JalaliRange(
//                               start: Jalali(1400, 1, 2),
//                               end: Jalali(1400, 1, 10),
//                             ),
//                             firstDate: Jalali(1385, 8),
//                             lastDate: Jalali(1450, 9),
//                           );
//                           setState(() {
//                             label =
//                                 "${picked?.start?.toJalaliDateTime() ?? ""} ${picked?.end?.toJalaliDateTime() ?? ""}";
//                           });
//                         },
//                         image: "06",
//                       ),
//                     ],
//                   ),
//                 ),
//               ],
//             ),
//           ),
//         ),
//         bottomNavigationBar: Container(
//           height: 70,
//           width: double.infinity,
//           padding: EdgeInsets.all(10),
//           decoration: BoxDecoration(boxShadow: [
//             BoxShadow(
//               blurRadius: 3,
//               spreadRadius: 0,
//               offset: Offset(0, 4),
//               color: Color(0xff000000).withOpacity(0.3),
//             ),
//           ], color: Colors.white),
//           child: Center(
//             child: Text(
//               label,
//               style: Theme.of(context)
//                   .textTheme
//                   .headline5
//                   .copyWith(color: Colors.black),
//               textAlign: TextAlign.center,
//             ),
//           ),
//         ),
//       ),
//     );
//   }

//   Widget imageButton({
//     Function onTap,
//     String image,
//   }) {
//     return ScaleGesture(
//       onTap: onTap,
//       child: Container(
//         margin: EdgeInsets.all(10),
//         decoration: BoxDecoration(
//             color: Colors.white,
//             boxShadow: [
//               BoxShadow(
//                 blurRadius: 3,
//                 spreadRadius: 0,
//                 offset: Offset(0, 4),
//                 color: Color(0xff000000).withOpacity(0.3),
//               ),
//             ],
//             borderRadius: BorderRadius.all(Radius.circular(10))),
//         child: Image.asset(
//           'assets/images/$image.png',
//           fit: BoxFit.fitWidth,
//         ),
//       ),
//     );
//   }
// }

// class ScaleGesture extends StatefulWidget {
//   final Widget child;
//   final double scale;
//   final VoidCallback onTap;

//   ScaleGesture({
//     this.child,
//     this.scale = 1.1,
//     this.onTap,
//   });
//   @override
//   _ScaleGestureState createState() => _ScaleGestureState();
// }

// class _ScaleGestureState extends State<ScaleGesture> {
//   double scale;

//   @override
//   void initState() {
//     scale = 1;
//     super.initState();
//   }

//   @override
//   Widget build(BuildContext context) {
//     return GestureDetector(
//       onTapDown: (detail) {
//         setState(() {
//           scale = widget.scale;
//         });
//       },
//       onTapCancel: () {
//         setState(() {
//           scale = 1;
//         });
//       },
//       onTapUp: (datail) {
//         setState(() {
//           scale = 1;
//         });
//         widget?.onTap();
//       },
//       child: Transform.scale(
//         scale: scale,
//         child: widget.child,
//       ),
//     );
//   }
// }
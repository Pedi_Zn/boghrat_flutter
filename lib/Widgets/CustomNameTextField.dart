import 'package:boghrat_flutter/Areas/User/Controllers/RegisterController.dart';
import 'package:boghrat_flutter/Areas/User/Controllers/RegisterV2Controller.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:get/get.dart';
import 'package:persian_number_utility/persian_number_utility.dart';
import 'package:boghrat_flutter/Config/AppConfig.dart' as config;

class CustomNameTextField extends StatefulWidget {
  final String screenType;
  final TextEditingController controller;

  const CustomNameTextField({
    Key? key,
    required this.controller,
    required this.screenType,
  }) : super(key: key);

  @override
  _CustomNameTextFieldState createState() => _CustomNameTextFieldState();
}

class _CustomNameTextFieldState extends State<CustomNameTextField> {
  @override
  void dispose() {
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return Container(
      height: 25,
      child: TextFormField(
        inputFormatters: [],
        onChanged: (s) {
          chooseValidator();
        },
        obscureText: false,
        textDirection: TextDirection.ltr,
        controller: widget.controller,
        cursorColor: config.Colors().accentColor(1),
        autofocus: false,
        keyboardType: TextInputType.text,
        style: Theme.of(context).textTheme.subtitle2,
        decoration: InputDecoration(
          counterText: "",
          hintText: "",
          hintTextDirection: TextDirection.ltr,
          hintStyle: TextStyle(
            fontSize: 14,
          ),
        ),
      ),
    );
  }

  void chooseValidator() {
    final registerController = Get.put(RegisterController());
    final registerV2Controller =
        Get.put(RegisterV2Controller(registerController.registerDS));

    switch (widget.screenType) {
      case "Register":
        registerV2Controller.checkIsValid();
        break;
    }
  }
}

import 'package:boghrat_flutter/Areas/User/Controllers/LoginController.dart';
import 'package:boghrat_flutter/Areas/User/Controllers/RegisterController.dart';
import 'package:boghrat_flutter/Widgets/PhoneInputFormater.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:get/get.dart';
import 'package:persian_number_utility/persian_number_utility.dart';
import 'package:boghrat_flutter/Config/AppConfig.dart' as config;

class CustomPhoneTextField extends StatefulWidget {
  final String screenType;
  final TextEditingController controller;

  const CustomPhoneTextField({
    Key? key,
    required this.controller,
    required this.screenType,
  }) : super(key: key);

  @override
  _CustomPhoneTextFieldState createState() => _CustomPhoneTextFieldState();
}

class _CustomPhoneTextFieldState extends State<CustomPhoneTextField> {
  final registerController = Get.put(RegisterController());
  final loginController = Get.put(LoginController());

  @override
  void dispose() {
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return Container(
      height: 30,
      child: TextFormField(
        inputFormatters: [
          FilteringTextInputFormatter.allow(RegExp(r'[\u06F0-\u06F90-9]')),
          PhoneInputFormatter(),
        ],
        onChanged: (s) {
          chooseValidator();
        },
        obscureText: false,
        textDirection: TextDirection.ltr,
        controller: widget.controller,
        cursorColor: config.Colors().accentColor(1),
        maxLength: 13,
        autofocus: false,
        keyboardType: TextInputType.number,
        style: Theme.of(context).textTheme.subtitle2,
        decoration: InputDecoration(
          counterText: "",
          hintText: "0912 111 1111".toPersianDigit(),
          hintTextDirection: TextDirection.ltr,
          hintStyle: TextStyle(
            fontSize: 14,
          ),
        ),
      ),
    );
  }

  void chooseValidator() {
    switch (widget.screenType) {
      case "Register":
        this.registerController.checkIsValid();
        break;
      case "Login":
        this.loginController.checkIsValid();
        break;
    }
  }
}

import 'package:flutter/material.dart';
import 'package:flutter_svg/flutter_svg.dart';

class BoghratLogo extends StatefulWidget {
  const BoghratLogo({Key? key}) : super(key: key);

  @override
  _BoghratLogoState createState() => _BoghratLogoState();
}

class _BoghratLogoState extends State<BoghratLogo> {
  @override
  void dispose() {
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return Container(
      height: 200,
      child: Stack(
        children: [
          Center(
            child: SvgPicture.asset(
              "assets/images/blob.svg",
              height: 180,
            ),
          ),
          Center(
            child: SvgPicture.asset(
              "assets/images/boghrat-logo.svg",
              height: 140,
            ),
          ),
        ],
      ),
    );
  }
}

import 'package:boghrat_flutter/Widgets/CustomPhoneTextField.dart';
import 'package:boghrat_flutter/Widgets/CustomTitleRow.dart';
import 'package:flutter/material.dart';

class CustomPhoneForm extends StatefulWidget {
  final IconData icon;
  final String text;
  final String screenType;
  final TextEditingController controller;

  CustomPhoneForm({
    Key? key,
    required this.icon,
    required this.text,
    required this.controller,
    required this.screenType,
  }) : super(key: key);

  @override
  _CustomPhoneFormState createState() => _CustomPhoneFormState();
}

class _CustomPhoneFormState extends State<CustomPhoneForm> {
  @override
  Widget build(BuildContext context) {
    return Column(
      crossAxisAlignment: CrossAxisAlignment.start,
      children: [
        CustomTitleRow(
          icon: widget.icon,
          text: widget.text,
          space: 10,
          textStyle: Theme.of(context).textTheme.subtitle2!,
        ),
        CustomPhoneTextField(
          controller: widget.controller,
          screenType: widget.screenType,
        ),
      ],
    );
  }
}

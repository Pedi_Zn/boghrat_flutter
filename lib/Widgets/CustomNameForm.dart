import 'package:boghrat_flutter/Widgets/CustomNameTextField.dart';
import 'package:boghrat_flutter/Widgets/CustomTitleRow.dart';
import 'package:flutter/material.dart';

class CustomNameForm extends StatefulWidget {
  final IconData icon;
  final String text;
  final String screenType;
  final TextEditingController controller;

  CustomNameForm({
    Key? key,
    required this.icon,
    required this.text,
    required this.controller,
    required this.screenType,
  }) : super(key: key);

  @override
  _CustomNameFormState createState() => _CustomNameFormState();
}

class _CustomNameFormState extends State<CustomNameForm> {
  @override
  Widget build(BuildContext context) {
    return Column(
      crossAxisAlignment: CrossAxisAlignment.start,
      children: [
        CustomTitleRow(
          icon: widget.icon,
          text: widget.text,
          space: 10,
          textStyle: Theme.of(context).textTheme.subtitle2!,
        ),
        CustomNameTextField(
          controller: widget.controller,
          screenType: widget.screenType,
        ),
      ],
    );
  }
}

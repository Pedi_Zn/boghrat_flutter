import 'package:boghrat_flutter/Areas/SearchTab/Controllers/SearchFilterController.dart';
import 'package:boghrat_flutter/Areas/SearchTab/Views/SearchFilterButton.dart';
import 'package:boghrat_flutter/Localization/Localization.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:material_floating_search_bar/material_floating_search_bar.dart';
import 'package:boghrat_flutter/Config/AppConfig.dart' as config;

class CustomSearch extends StatelessWidget {
  final searchFilterController = Get.put(SearchFilterController());

  @override
  Widget build(BuildContext context) {
    final isPortrait =
        MediaQuery.of(context).orientation == Orientation.portrait;

    return Obx(() {
      return FloatingSearchBar(
        margins: EdgeInsets.fromLTRB(14, 20, 14, 14),
        elevation: 0.7,
        controller: searchFilterController.searchQuery.value,
        backgroundColor: Colors.grey.shade200,
        accentColor: config.Colors().blackColor(1),
        queryStyle: TextStyle(color: config.Colors().blackColor(1)),
        hint: DemoLocalizations.of(context)!.trans("Search"),
        hintStyle: TextStyle(fontSize: 14, color: Colors.grey.shade700),
        scrollPadding: const EdgeInsets.only(bottom: 10),
        transitionDuration: const Duration(milliseconds: 800),
        transitionCurve: Curves.easeInOut,
        physics: const BouncingScrollPhysics(),
        axisAlignment: isPortrait ? 0.0 : 0.0,
        openAxisAlignment: 0.0,
        borderRadius: BorderRadius.circular(10.0),
        width: isPortrait ? 500 : 550,
        debounceDelay: const Duration(milliseconds: 500),
        onQueryChanged: (query) {
          searchFilterController.fillSearchResult();
        },
        transition: CircularFloatingSearchBarTransition(),
        actions: [
          FloatingSearchBarAction.searchToClear(
            showIfClosed: false,
          ),
          FloatingSearchBarAction(
            showIfOpened: true,
            showIfClosed: false,
            child: Container(
              padding: EdgeInsets.only(top: 7, bottom: 7),
              child: VerticalDivider(
                color: Colors.grey.shade400,
              ),
            ),
          ),
          SearchFilterButton(),
        ],
        builder: (context, transition) {
          return Obx(
            () => ClipRRect(
              borderRadius: BorderRadius.circular(10),
              child: Material(
                child: Column(
                  mainAxisSize: MainAxisSize.min,
                  children: [
                    ListView.builder(
                      shrinkWrap: true,
                      physics: NeverScrollableScrollPhysics(),
                      itemCount: _checkItemCount(searchFilterController),
                      itemBuilder: (BuildContext context, int index) {
                        return !searchFilterController.isLoading.value
                            ? _checkIndex(
                                index,
                                searchFilterController.searchQuery.value.query,
                                searchFilterController,
                              )
                            : SizedBox(
                                height: 80,
                                width: 40,
                                child: Center(
                                  child: CircularProgressIndicator(),
                                ),
                              );
                      },
                    ),
                  ],
                ),
              ),
            ),
          );
        },
      );
    });
  }
}

_checkItemCount(searchFilterController) {
  if (searchFilterController.searchQuery.value.query == "") {
    return 0;
  }
  if (searchFilterController.isLoading.value) {
    return 1;
  } else if (searchFilterController.doctorsList.length > 0 &&
      searchFilterController.expertisesList.length > 0) {
    print("TOTAL : ");
    print(searchFilterController.doctorsList.length +
        searchFilterController.expertisesList.length +
        2);
    return searchFilterController.doctorsList.length +
        searchFilterController.expertisesList.length +
        2;
  } else {
    return searchFilterController.doctorsList.length +
        searchFilterController.expertisesList.length +
        3;
  }
}

_checkIndex(index, query, searchFilterController) {
  var list = searchFilterController.list.value;

  List<String>? searchResult = [];

  for (var i = 0; i < list[0].length; i++) {
    searchResult.add("+" + list[0].keys.elementAt(i));
    for (var j = 0; j < list[0][list[0].keys.elementAt(i)].length; j++) {
      searchResult.add(list[0][list[0].keys.elementAt(i)][j]);
    }
  }

  if (searchResult[index].startsWith("+")) {
    return _buildSearchTitle(searchResult[index].substring(1));
  } else {
    return _buildSearchSubTitle(searchResult[index], query);
  }
}

_buildSearchTitle(title) {
  return ListTile(
    contentPadding: EdgeInsets.all(10),
    title: Text(
      title,
      style: TextStyle(fontSize: 15.0, color: Colors.grey.shade600),
    ),
    onTap: () {},
  );
}

_buildSearchSubTitle(subTitle, query) {
  return Column(
    children: [
      ListTile(
        contentPadding: EdgeInsets.only(left: 30, right: 30),
        title: RichText(
          text: new TextSpan(
            style: new TextStyle(fontFamily: 'IranSans'),
            children: (subTitle.contains(query))
                ? <TextSpan>[
                    new TextSpan(
                      text: subTitle.split(query)[0],
                      style: TextStyle(
                        fontSize: 13,
                        color: config.Colors().blackColor(1),
                      ),
                    ),
                    new TextSpan(
                      text: query,
                      style: TextStyle(
                        fontSize: 13,
                        color: config.Colors().primaryColorDark(1),
                      ),
                    ),
                    new TextSpan(
                      text: subTitle.split(query)[1],
                      style: TextStyle(
                        fontSize: 13.0,
                        color: config.Colors().blackColor(1),
                      ),
                    ),
                  ]
                : [
                    new TextSpan(
                      text: subTitle,
                      style: TextStyle(
                        fontSize: 13,
                        color: config.Colors().blackColor(1),
                      ),
                    ),
                  ],
          ),
        ),
        onTap: () {},
      ),
      Container(
        color: Colors.grey.shade300,
        height: 1,
        margin: EdgeInsets.symmetric(horizontal: 20),
      ),
    ],
  );
}

// _buildOldSearch(context) {
//   return Container(
//     height: 50.0,
//     margin: EdgeInsets.all(20.0),
//     child: TextField(
//       autofocus: false,
//       maxLines: 1,
//       style: TextStyle(fontSize: 14.0, color: Colors.black),
//       decoration: InputDecoration(
//         hintText: DemoLocalizations.of(context)!.trans("Search"),
//         hintStyle: TextStyle(fontSize: 15),
//         contentPadding: EdgeInsets.fromLTRB(10.0, 16.0, 10.0, 0.0),
//         border: OutlineInputBorder(borderRadius: BorderRadius.circular(10.0)),
//         filled: true,
//         fillColor: Colors.grey.shade200,
//         hoverColor: Colors.grey.shade200,
//         focusedBorder: OutlineInputBorder(
//           borderSide: BorderSide(color: config.Colors().primaryColorLight(1)),
//           borderRadius: BorderRadius.circular(10.0),
//         ),
//         enabledBorder: UnderlineInputBorder(
//           borderSide: BorderSide(color: config.Colors().primaryColorLight(1)),
//           borderRadius: BorderRadius.circular(10.0),
//         ),
//         suffixIcon: Row(
//           mainAxisAlignment: MainAxisAlignment.spaceBetween,
//           mainAxisSize: MainAxisSize.min,
//           children: <Widget>[
//             Icon(
//               FontAwesomeIcons.times,
//               size: 20,
//               color: Colors.black12,
//             ),
//             Container(
//               width: 1,
//               margin: EdgeInsets.fromLTRB(15, 10, 15, 10),
//               color: Colors.black26,
//             ),
//             IconButton(
//               icon: Icon(
//                 FontAwesomeIcons.slidersH,
//                 size: 20,
//                 color: Colors.black,
//               ),
//               onPressed: () {
//                 Container(
//                   margin: EdgeInsets.all(20),
//                   padding: EdgeInsets.all(30),
//                   child: _buildBottomSheet(context),
//                 );
//               },
//             ),
//             SizedBox(width: 15),
//           ],
//         ),
//       ),
//     ),
//   );
// }

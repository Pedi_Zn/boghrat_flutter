import 'package:flutter/material.dart';
import 'package:boghrat_flutter/Config/AppConfig.dart' as config;

class CustomVisitTypeRow extends StatefulWidget {
  final String text;
  var value;

  CustomVisitTypeRow({
    Key? key,
    required this.text,
    required this.value,
  }) : super(key: key);

  @override
  _CustomVisitTypeRowState createState() => _CustomVisitTypeRowState();
}

class _CustomVisitTypeRowState extends State<CustomVisitTypeRow> {
  @override
  void dispose() {
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    Color getColor(Set<MaterialState> states) {
      const Set<MaterialState> interactiveStates = <MaterialState>{
        MaterialState.pressed,
        MaterialState.hovered,
        MaterialState.focused,
      };
      if (states.any(interactiveStates.contains)) {
        return config.Colors().primaryColorDark(1);
      }
      return config.Colors().primaryColorDark(1);
    }

    return Container(
      height: 30,
      child: Row(
        mainAxisSize: MainAxisSize.min,
        children: [
          Checkbox(
            value: widget.value.value,
            onChanged: (s) {
              setState(() {
                widget.value(!widget.value.value);
              });
            },
            checkColor: config.Colors().primaryColorLight(1),
            fillColor: MaterialStateProperty.resolveWith(getColor),
          ),
          Container(
            margin: EdgeInsets.only(left: 5),
            child: Text(
              widget.text,
              style: TextStyle(fontSize: 13),
            ),
          )
        ],
      ),
    );
  }
}

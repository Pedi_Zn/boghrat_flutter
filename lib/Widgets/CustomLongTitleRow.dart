import 'package:flutter/material.dart';
import 'package:font_awesome_flutter/font_awesome_flutter.dart';
import 'package:boghrat_flutter/Config/AppConfig.dart' as config;

class CustomLongTitleRow extends StatefulWidget {
  final IconData icon;
  final String text;
  final String secondText;
  final double space;
  final TextStyle textStyle;

  const CustomLongTitleRow({
    Key? key,
    required this.icon,
    required this.text,
    required this.secondText,
    required this.space,
    required this.textStyle,
  }) : super(key: key);

  @override
  _CustomTitleRowState createState() => _CustomTitleRowState();
}

class _CustomTitleRowState extends State<CustomLongTitleRow> {
  @override
  void dispose() {
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return Row(
      mainAxisAlignment: MainAxisAlignment.start,
      mainAxisSize: MainAxisSize.min,
      children: [
        FaIcon(
          widget.icon,
          size: 20.0,
          color: config.Colors().greenColor(1),
        ),
        Container(
          margin: EdgeInsets.symmetric(horizontal: widget.space),
          child: Text(
            widget.text,
            style: Theme.of(context).textTheme.headline6!,
          ),
        ),
        Container(
          margin: EdgeInsets.symmetric(horizontal: widget.space),
          child: Text(
            widget.secondText,
            style: widget.textStyle,
          ),
        ),
      ],
    );
  }
}

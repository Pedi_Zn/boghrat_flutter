import 'package:boghrat_flutter/Localization/Localization.dart';
import 'package:flutter/material.dart';
import 'package:boghrat_flutter/Config/AppConfig.dart' as config;

class BoghratBackground extends StatefulWidget {
  final List<Widget> children;
  BoghratBackground({Key? key, required this.children}) : super(key: key);

  @override
  _BoghratBackgroundState createState() => _BoghratBackgroundState();
}

class _BoghratBackgroundState extends State<BoghratBackground> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
        backgroundColor: config.Colors().primaryColor(1),
        // resizeToAvoidBottomInset: false,
        body: SingleChildScrollView(
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.center,
            children: <Widget>[
              Center(
                child: Container(
                  height: config.AppConfig(context).appHeight(100) -
                      config.AppConfig(context).appVerticalPadding(100),
                  margin:
                      EdgeInsets.symmetric(horizontal: 30.0, vertical: 30.0),
                  padding: EdgeInsets.symmetric(horizontal: 30.0),
                  constraints: BoxConstraints(maxWidth: 600),
                  decoration: BoxDecoration(
                    color: config.Colors().primaryColorLight(1),
                    boxShadow: [
                      BoxShadow(
                        color: Colors.grey,
                        offset: Offset(0.0, 1.0),
                        blurRadius: 10.0,
                      ),
                    ],
                    borderRadius: BorderRadius.only(
                      topRight: Radius.circular(150.0),
                      topLeft: Radius.circular(150.0),
                      bottomRight: Radius.circular(10.0),
                      bottomLeft: Radius.circular(10.0),
                    ),
                  ),
                  child: Column(children: widget.children),
                ),
              ),
            ],
          ),
        ));
    // return Scaffold(
    //   resizeToAvoidBottomInset: false,
    //   backgroundColor: config.Colors().primaryColor(1),
    //   body: Center(
    //     child: Container(
    //       margin: EdgeInsets.symmetric(horizontal: 30.0, vertical: 30.0),
    //       padding: EdgeInsets.symmetric(horizontal: 30.0),
    //       constraints: BoxConstraints(maxWidth: 600),
    //       decoration: BoxDecoration(
    //         color: config.Colors().primaryColorLight(1),
    //         boxShadow: [
    //           BoxShadow(
    //             color: Colors.grey,
    //             offset: Offset(0.0, 1.0),
    //             blurRadius: 10.0,
    //           ),
    //         ],
    //         borderRadius: BorderRadius.only(
    //           topRight: Radius.circular(150.0),
    //           topLeft: Radius.circular(150.0),
    //           bottomRight: Radius.circular(10.0),
    //           bottomLeft: Radius.circular(10.0),
    //         ),
    //       ),
    //       child: Column(children: widget.children),
    //     ),
    //   ),
    // );
  }
}

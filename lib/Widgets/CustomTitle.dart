import 'package:flutter/material.dart';

class CustomTitle extends StatefulWidget {
  final String titleText;
  CustomTitle({Key? key, required this.titleText}) : super(key: key);

  @override
  _CustomTitleState createState() => _CustomTitleState();
}

class _CustomTitleState extends State<CustomTitle> {
  @override
  Widget build(BuildContext context) {
    return Text(
      widget.titleText,
      style: Theme.of(context).textTheme.headline1,
    );
  }
}

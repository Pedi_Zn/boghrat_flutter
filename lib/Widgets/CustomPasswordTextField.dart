import 'package:boghrat_flutter/Areas/User/Controllers/ForgotPasswordController.dart';
import 'package:boghrat_flutter/Areas/User/Controllers/LoginController.dart';
import 'package:boghrat_flutter/Areas/User/Controllers/RegisterController.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:get/get.dart';
import 'package:boghrat_flutter/Config/AppConfig.dart' as config;

class CustomPasswordTextField extends StatefulWidget {
  final String screenType;
  final TextEditingController controller;

  const CustomPasswordTextField({
    Key? key,
    required this.controller,
    required this.screenType,
  }) : super(key: key);

  @override
  _CustomPasswordTextFieldState createState() =>
      _CustomPasswordTextFieldState();
}

class _CustomPasswordTextFieldState extends State<CustomPasswordTextField> {
  @override
  void dispose() {
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return Container(
      height: 30,
      child: TextFormField(
        onChanged: (s) {
          chooseValidator();
        },
        obscureText: true,
        textDirection: TextDirection.ltr,
        controller: widget.controller,
        cursorColor: config.Colors().accentColor(1),
        maxLength: 100,
        autofocus: false,
        keyboardType: TextInputType.number,
        style: Theme.of(context).textTheme.subtitle2,
        decoration: InputDecoration(
          counterText: "",
          hintText: "\u2022\u2022\u2022\u2022\u2022\u2022\u2022\u2022",
          hintTextDirection: TextDirection.ltr,
          hintStyle: TextStyle(
            fontSize: 14,
          ),
        ),
      ),
    );
  }

  void chooseValidator() {
    final registerController = Get.put(RegisterController());
    final loginController = Get.put(LoginController());
    final forgotPasswordController = Get.put(ForgotPasswordController(
        registerController.phoneController.value.text));

    switch (widget.screenType) {
      case "Register":
        registerController.checkIsValid();
        break;
      case "Login":
        loginController.checkIsValid();
        break;
      case "Forgot":
        forgotPasswordController.checkIsValid();
        break;
    }
  }
}

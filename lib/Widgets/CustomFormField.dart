import 'package:boghrat_flutter/Widgets/CustomPasswordTextField.dart';
import 'package:boghrat_flutter/Widgets/CustomTitleRow.dart';
import 'package:flutter/material.dart';

class CustomPasswordForm extends StatefulWidget {
  final IconData icon;
  final String text;
  final String screenType;
  final TextEditingController controller;

  CustomPasswordForm({
    Key? key,
    required this.icon,
    required this.text,
    required this.controller,
    required this.screenType,
  }) : super(key: key);

  @override
  _CustomPasswordFormState createState() => _CustomPasswordFormState();
}

class _CustomPasswordFormState extends State<CustomPasswordForm> {
  @override
  Widget build(BuildContext context) {
    return Column(
      crossAxisAlignment: CrossAxisAlignment.start,
      children: [
        CustomTitleRow(
          icon: widget.icon,
          text: widget.text,
          space: 10,
          textStyle: Theme.of(context).textTheme.subtitle2!,
        ),
        CustomPasswordTextField(
          controller: widget.controller,
          screenType: widget.screenType,
        ),
      ],
    );
  }
}

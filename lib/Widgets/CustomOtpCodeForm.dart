import 'package:boghrat_flutter/Widgets/CustomOtpCodeTextField.dart';
import 'package:boghrat_flutter/Widgets/CustomTitleRow.dart';
import 'package:flutter/material.dart';

class CustomOtpCodeForm extends StatefulWidget {
  final IconData icon;
  final String text;
  final String screenType;
  final TextEditingController controller;

  CustomOtpCodeForm({
    Key? key,
    required this.icon,
    required this.text,
    required this.controller,
    required this.screenType,
  }) : super(key: key);

  @override
  _CustomOtpCodeFormState createState() => _CustomOtpCodeFormState();
}

class _CustomOtpCodeFormState extends State<CustomOtpCodeForm> {
  @override
  Widget build(BuildContext context) {
    return Column(
      crossAxisAlignment: CrossAxisAlignment.start,
      children: [
        CustomTitleRow(
          icon: widget.icon,
          text: widget.text,
          space: 10,
          textStyle: Theme.of(context).textTheme.subtitle2!,
        ),
        CustomOtpCodeTextField(
          controller: widget.controller,
          screenType: widget.screenType,
        ),
      ],
    );
  }
}

import 'package:flutter/material.dart';
import 'package:boghrat_flutter/Config/AppConfig.dart' as config;

class CustomBox extends StatefulWidget {
  final String text;
  final double height;
  final Widget widget;

  const CustomBox({
    Key? key,
    required this.text,
    required this.height,
    required this.widget,
  }) : super(key: key);

  @override
  _CustomBoxState createState() => _CustomBoxState();
}

class _CustomBoxState extends State<CustomBox> {
  @override
  Widget build(BuildContext context) {
    return Stack(
      children: [
        Wrap(
          children: [
            Container(
              margin: EdgeInsets.fromLTRB(20, 32, 25, 0),
              constraints: BoxConstraints(minWidth: 360, maxWidth: 360),
              height: widget.height,
              child: CustomPaint(painter: MyPainter(), child: widget.widget),
            ),
          ],
        ),
        Positioned(
          top: 10,
          right: 15,
          child: Row(
            children: [
              Container(
                color: config.Colors().primaryColorLight(1),
                padding: EdgeInsets.fromLTRB(15, 10, 25, 5),
                child: Text(
                  widget.text,
                  style: TextStyle(
                    fontWeight: FontWeight.bold,
                    color: config.Colors().primaryColorDark(1),
                    backgroundColor: config.Colors().primaryColorLight(1),
                  ),
                ),
              ),
              Container(
                margin: EdgeInsets.only(top: 5),
                height: 8,
                width: 8,
                decoration: BoxDecoration(
                  color: config.Colors().primaryColorDark(1),
                  shape: BoxShape.circle,
                ),
              ),
            ],
          ),
        ),
      ],
    );
  }
}

class MyPainter extends CustomPainter {
  @override
  void paint(Canvas canvas, Size size) {
    double w = size.width;
    double h = size.height;
    double r = 15;

    Paint blackPaint = Paint()
      ..color = config.Colors().primaryColorDark(1)
      ..style = PaintingStyle.stroke
      ..strokeWidth = 1;

    RRect fullRect = RRect.fromRectAndRadius(
      Rect.fromCenter(center: Offset(w / 2, h / 2), width: w, height: h),
      Radius.circular(r),
    );

    canvas.drawRRect(fullRect, blackPaint);
  }

  @override
  bool shouldRepaint(CustomPainter oldDelegate) {
    return true;
  }
}

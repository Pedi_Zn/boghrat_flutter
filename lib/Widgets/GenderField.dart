import 'package:flutter/material.dart';

class GenderField extends StatelessWidget {
  final List<String> genderList;
  final TextEditingController controller;

  GenderField(this.genderList, this.controller);

  @override
  Widget build(BuildContext context) {
    String? select;
    Map<int, String> mappedGender = genderList.asMap();

    return StatefulBuilder(
      builder: (_, StateSetter setState) => Row(
        mainAxisAlignment: MainAxisAlignment.end,
        children: [
          ...mappedGender.entries.map(
            (MapEntry<int, String> mapEntry) => Row(
              mainAxisAlignment: MainAxisAlignment.start,
              mainAxisSize: MainAxisSize.min,
              children: [
                Radio(
                    activeColor: Theme.of(context).primaryColor,
                    groupValue: select,
                    value: genderList[mapEntry.key],
                    onChanged: (value) {
                      setState(() {
                        controller.text = setGenderId(value.toString());
                        select = value.toString();
                      });
                      setState(() => select = value as String?);
                    }),
                Text(mapEntry.value)
              ],
            ),
          ),
        ],
      ),
    );
  }

  String setGenderId(gender) {
    if (gender == "زن") {
      return "1";
    } else
      return "2";
  }
}

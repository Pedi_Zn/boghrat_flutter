import 'package:flutter/material.dart';
import 'package:boghrat_flutter/Config/AppConfig.dart' as config;

class CustomLittleButton extends StatefulWidget {
  final IconData icon;
  final Color color;
  final GestureTapCallback onPressed;
  final String text;
  final bool? disabled;

  CustomLittleButton({
    Key? key,
    required this.icon,
    required this.color,
    required this.onPressed,
    required this.text,
    this.disabled = false,
  }) : super(key: key);

  @override
  _CustomLittleButtonState createState() => _CustomLittleButtonState();
}

class _CustomLittleButtonState extends State<CustomLittleButton> {
  @override
  Widget build(BuildContext context) {
    return Container(
      height: 40,
      child: MaterialButton(
        onPressed: widget.disabled! ? null : widget.onPressed,
        disabledColor: Colors.grey.shade400,
        padding: EdgeInsets.zero,
        color: widget.color,
        shape: RoundedRectangleBorder(
          borderRadius: BorderRadius.circular(10.0),
        ),
        child: Row(
          mainAxisAlignment: MainAxisAlignment.center,
          children: [
            Icon(
              widget.icon,
              color: config.Colors().primaryColorLight(1),
            ),
            SizedBox(width: 5),
            Text(
              widget.text,
              style: TextStyle(
                  color: config.Colors().primaryColorLight(1), fontSize: 15),
            )
          ],
        ),
      ),
    );
  }
}

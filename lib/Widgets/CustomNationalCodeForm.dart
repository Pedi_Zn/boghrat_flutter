import 'package:boghrat_flutter/Widgets/CustomNationalCodeTextField.dart';
import 'package:boghrat_flutter/Widgets/CustomTitleRow.dart';
import 'package:flutter/material.dart';

class CustomNationalCodeForm extends StatefulWidget {
  final IconData icon;
  final String text;
  final String screenType;
  final TextEditingController controller;

  CustomNationalCodeForm({
    Key? key,
    required this.icon,
    required this.text,
    required this.controller,
    required this.screenType,
  }) : super(key: key);

  @override
  _CustomNationalCodeFormState createState() => _CustomNationalCodeFormState();
}

class _CustomNationalCodeFormState extends State<CustomNationalCodeForm> {
  @override
  Widget build(BuildContext context) {
    return Column(
      crossAxisAlignment: CrossAxisAlignment.start,
      children: [
        CustomTitleRow(
          icon: widget.icon,
          text: widget.text,
          space: 10,
          textStyle: Theme.of(context).textTheme.subtitle2!,
        ),
        CustomNationalCodeTextField(
          controller: widget.controller,
          screenType: widget.screenType,
        ),
      ],
    );
  }
}

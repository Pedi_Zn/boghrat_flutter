import 'package:boghrat_flutter/Areas/User/Controllers/OTPController.dart';
import 'package:flutter_test/flutter_test.dart';
import 'package:get/get.dart';

void main() {
  final oTPController = Get.put(OTPController());
  group('OTP From', () {
    test('Empty Code Test', () {
      oTPController.codeController.value.text = "";
      oTPController.checkIsValid();
      var result = oTPController.isValid.value;
      expect(result, false);
    });

    test('Non-Valid Code Test', () {
      oTPController.codeController.value.text = "000";
      oTPController.checkIsValid();
      var result = oTPController.isValid.value;
      expect(result, false);
    });

    test('Valid Code Test', () {
      oTPController.codeController.value.text = "1234";
      oTPController.checkIsValid();
      var result = oTPController.isValid.value;
      expect(result, true);
    });
  });
}

import 'package:boghrat_flutter/Areas/User/Controllers/ForgotPasswordController.dart';
import 'package:flutter_test/flutter_test.dart';
import 'package:get/get.dart';

void main() {
  final forgotPasswordController = Get.put(ForgotPasswordController());
  group('ForgotPassword From', () {
    test('Empty ConfirmPassword & Password Test', () {
      forgotPasswordController.passwordController.value.text = "";
      forgotPasswordController.confirmPasswordController.value.text = "";
      forgotPasswordController.checkIsValid();
      var result = forgotPasswordController.isValid.value;
      expect(result, false);
    });

    test('Empty ConfirmPassword Non-Valid Password Test', () {
      forgotPasswordController.passwordController.value.text = "1234";
      forgotPasswordController.confirmPasswordController.value.text = "";
      forgotPasswordController.checkIsValid();
      var result = forgotPasswordController.isValid.value;
      expect(result, false);
    });

    test('Empty ConfirmPassword Valid Password Test', () {
      forgotPasswordController.passwordController.value.text = "123456";
      forgotPasswordController.confirmPasswordController.value.text = "";
      forgotPasswordController.checkIsValid();
      var result = forgotPasswordController.isValid.value;
      expect(result, false);
    });

    test('Empty Password Non-Valid ConfirmPassword Test', () {
      forgotPasswordController.passwordController.value.text = "";
      forgotPasswordController.confirmPasswordController.value.text = "123";
      forgotPasswordController.checkIsValid();
      var result = forgotPasswordController.isValid.value;
      expect(result, false);
    });

    test('Empty Password Valid ConfirmPassword Test', () {
      forgotPasswordController.passwordController.value.text = "";
      forgotPasswordController.confirmPasswordController.value.text = "123456";
      forgotPasswordController.checkIsValid();
      var result = forgotPasswordController.isValid.value;
      expect(result, false);
    });

    test('Valid Password Non-Valid ConfirmPassword Test', () {
      forgotPasswordController.passwordController.value.text = "123456";
      forgotPasswordController.confirmPasswordController.value.text = "123";
      forgotPasswordController.checkIsValid();
      var result = forgotPasswordController.isValid.value;
      expect(result, false);
    });

    test('Valid ConfirmPassword Non-Valid Password Test', () {
      forgotPasswordController.passwordController.value.text = "123";
      forgotPasswordController.confirmPasswordController.value.text = "123456";
      forgotPasswordController.checkIsValid();
      var result = forgotPasswordController.isValid.value;
      expect(result, false);
    });

    test('Non-Valid Password & ConfirmPassword Test', () {
      forgotPasswordController.passwordController.value.text = "1234";
      forgotPasswordController.confirmPasswordController.value.text = "1234";
      forgotPasswordController.checkIsValid();
      var result = forgotPasswordController.isValid.value;
      expect(result, false);
    });

    test('Non-Equal Valid Password & ConfirmPassword Test', () {
      forgotPasswordController.passwordController.value.text = "1234567";
      forgotPasswordController.confirmPasswordController.value.text = "123465";
      forgotPasswordController.checkIsValid();
      var result = forgotPasswordController.isValid.value;
      expect(result, false);
    });

    test('Equal Valid Password & ConfirmPassword Test', () {
      forgotPasswordController.passwordController.value.text = "123456";
      forgotPasswordController.confirmPasswordController.value.text = "123456";
      forgotPasswordController.checkIsValid();
      var result = forgotPasswordController.isValid.value;
      expect(result, true);
    });
  });
}

import 'package:boghrat_flutter/Areas/User/Controllers/LoginController.dart';
import 'package:flutter_test/flutter_test.dart';
import 'package:get/get.dart';

void main() {
  final loginController = Get.put(LoginController());
  group('Login From', () {
    test('Empty Password & Phone Test', () {
      loginController.phoneController.value.text = "";
      loginController.passwordController.value.text = "";
      loginController.checkIsValid();
      var result = loginController.isValid.value;
      expect(result, false);
    });

    test('Empty Password Non-Valid Phone Test', () {
      loginController.phoneController.value.text = "0991 203 76";
      loginController.passwordController.value.text = "";
      loginController.checkIsValid();
      var result = loginController.isValid.value;
      expect(result, false);
    });

    test('Empty Password Valid Phone Test', () {
      loginController.phoneController.value.text = "0991 203 7674";
      loginController.passwordController.value.text = "";
      loginController.checkIsValid();
      var result = loginController.isValid.value;
      expect(result, false);
    });

    test('Empty Phone Non-Valid Password Test', () {
      loginController.phoneController.value.text = "";
      loginController.passwordController.value.text = "123";
      loginController.checkIsValid();
      var result = loginController.isValid.value;
      expect(result, false);
    });

    test('Empty Phone Valid Password Test', () {
      loginController.phoneController.value.text = "";
      loginController.passwordController.value.text = "123456";
      loginController.checkIsValid();
      var result = loginController.isValid.value;
      expect(result, false);
    });

    test('Valid Phone Non-Valid Password Test', () {
      loginController.phoneController.value.text = "0991 203 7674";
      loginController.passwordController.value.text = "123";
      loginController.checkIsValid();
      var result = loginController.isValid.value;
      expect(result, false);
    });

    test('Valid Password Non-Valid Phone Test', () {
      loginController.phoneController.value.text = "0991 203 76";
      loginController.passwordController.value.text = "123456";
      loginController.checkIsValid();
      var result = loginController.isValid.value;
      expect(result, false);
    });

    test('Non-Valid Phone & Password Test', () {
      loginController.phoneController.value.text = "0991 203 76";
      loginController.passwordController.value.text = "123";
      loginController.checkIsValid();
      var result = loginController.isValid.value;
      expect(result, false);
    });

    test('Valid Phone & Password Test', () {
      loginController.phoneController.value.text = "0991 203 7674";
      loginController.passwordController.value.text = "12345";
      loginController.checkIsValid();
      var result = loginController.isValid.value;
      expect(result, true);
    });
  });
}

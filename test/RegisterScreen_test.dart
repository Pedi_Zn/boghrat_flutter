import 'package:boghrat_flutter/Areas/User/Controllers/RegisterController.dart';
import 'package:flutter_test/flutter_test.dart';
import 'package:get/get.dart';

void main() {
  final registerController = Get.put(RegisterController());
  group('Register From', () {
    test('Empty NationalCode & Phone Test', () {
      registerController.phoneController.value.text = "";
      registerController.nationalCodeController.value.text = "";
      registerController.checkIsValid();
      var result = registerController.isValid.value;
      expect(result, false);
    });

    test('Empty NationalCode Non-Valid Phone Test', () {
      registerController.phoneController.value.text = "0991 203 76";
      registerController.nationalCodeController.value.text = "";
      registerController.checkIsValid();
      var result = registerController.isValid.value;
      expect(result, false);
    });

    test('Empty NationalCode Valid Phone Test', () {
      registerController.phoneController.value.text = "0991 203 7674";
      registerController.nationalCodeController.value.text = "";
      registerController.checkIsValid();
      var result = registerController.isValid.value;
      expect(result, false);
    });

    test('Empty Phone Non-Valid NationalCode Test', () {
      registerController.phoneController.value.text = "";
      registerController.nationalCodeController.value.text = "123";
      registerController.checkIsValid();
      var result = registerController.isValid.value;
      expect(result, false);
    });

    test('Empty Phone Valid NationalCode Test', () {
      registerController.phoneController.value.text = "";
      registerController.nationalCodeController.value.text = "002 1114501";
      registerController.checkIsValid();
      var result = registerController.isValid.value;
      expect(result, false);
    });

    test('Valid Phone Non-Valid NationalCode Test', () {
      registerController.phoneController.value.text = "0991 203 7674";
      registerController.nationalCodeController.value.text = "123";
      registerController.checkIsValid();
      var result = registerController.isValid.value;
      expect(result, false);
    });

    test('Valid NationalCode Non-Valid Phone Test', () {
      registerController.phoneController.value.text = "0991 203 76";
      registerController.nationalCodeController.value.text = "002 1114501";
      registerController.checkIsValid();
      var result = registerController.isValid.value;
      expect(result, false);
    });

    test('Non-Valid Phone & NationalCode Test', () {
      registerController.phoneController.value.text = "0991 203 76";
      registerController.nationalCodeController.value.text = "123";
      registerController.checkIsValid();
      var result = registerController.isValid.value;
      expect(result, false);
    });

    test('Valid Phone & NationalCode Test', () {
      registerController.phoneController.value.text = "0991 203 7674";
      registerController.nationalCodeController.value.text = "002 1114501";
      registerController.checkIsValid();
      var result = registerController.isValid.value;
      expect(result, true);
    });
  });
}
